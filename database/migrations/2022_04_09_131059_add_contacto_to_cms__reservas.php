<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContactoToCmsReservas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_reservas', function (Blueprint $table) {
            $table->string('medio_contacto')->nullable()->default(null)->after('observaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_reservas', function (Blueprint $table) {
            $table->dropColumn('medio_contacto');
        });
    }
}
