<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pagos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_pagos', function (Blueprint $table) {
            
            $table->increments('id');

            $table->integer('id_venta')->unsigned();
            $table->foreign('id_venta')->references('id')->on('cms_ventas');

            $table->string('folio');
            $table->float('cantidad');
            $table->string('usuario');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_pagos');
    }
}
