<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsPromocionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_promociones', function (Blueprint $table) {
            $table->increments('id');

            $table->string('titulo');
            $table->string('slug_url')->unique();
            $table->string('precio');

            $table->text('descripcion');
            $table->text('contenido');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->string('imagen');
            $table->string('thumb');    

                 // nuevos campos
            $table->integer('precioTercer');
            $table->integer('menorPrecio1');
            $table->integer('menorPrecio2');
            $table->integer('menorPrecio3');
            $table->string('menorEdad1');
            $table->string('menorEdad2');
            $table->string('menorEdad3');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_promociones');
    }
}
