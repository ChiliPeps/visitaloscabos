<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_reservas', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('cms_users');

            $table->integer('habitaciones');
            $table->string('nombre');
            $table->string('telefono');
            $table->string('correo');            
            $table->string('hotel');
            $table->string('tarifa')->nullable();
            $table->string('fecha_entrada');
            $table->string('fecha_salida');
            // nuevos campos
            $table->date('activa')->nullable();
            $table->date('confirmacion')->nullable();
            $table->date('limite_pago_cliente')->nullable();
            $table->date('limite_pago_hotel')->nullable();
            $table->string('folio')->nullable();
            $table->string('clave')->nullable();
            $table->date('hotel_pagado')->nullable();
            $table->date('reserva_pagada')->nullable();
            $table->integer('precio');
            $table->string('color')->nullable()->default(null);
            $table->text('observaciones')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('cms_habitaciones', function (Blueprint $table){
            $table->increments('id');
            
            $table->integer('id_reserva')->unsigned();
            $table->foreign('id_reserva')->references('id')->on('cms_reservas');
            $table->string('nombre');
            $table->integer('adultos');
            $table->integer('menores')->nullable();
            $table->integer('edad1')->nullable()->default(null);
            $table->integer('edad2')->nullable()->default(null);
            $table->integer('edad3')->nullable()->default(null);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_habitaciones');
        Schema::dropIfExists('cms_reservas');
        
    }
}
