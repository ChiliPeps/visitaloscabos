<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ventas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_ventas', function (Blueprint $table) {
            
            $table->increments('id');

            // $table->integer('id_user')->nullable()->unsigned();
            // $table->foreign('id_user')->references('id')->on('cms_users');

            $table->string('tipo');
            $table->string('nombre');
            $table->string('telefono');
            $table->string('correo');            
            $table->string('hotel');
            $table->string('fecha_entrada');
            $table->string('fecha_salida');
            // nuevos campos
            $table->date('confirmacion')->nullable();
            $table->date('limite_pago_cliente')->nullable();
            $table->date('limite_pago_hotel')->nullable();
            $table->string('folio')->nullable();
            $table->string('clave')->nullable();

            $table->date('pagado')->nullable();
            // 
            $table->boolean('tercerAdulto');
            $table->string('menores');
            $table->integer('edad1')->nullable();
            $table->integer('edad2')->nullable();
            $table->integer('precio');
           
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_ventas');
    }
}
