<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPapeletaToCMSReservas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_reservas', function (Blueprint $table) {
            $table->date('papeleta_enviada')->nullable()->default(null)->after('medio_contacto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_reservas', function (Blueprint $table) {
            $table->dropColumn('papeleta_enviada');
        });
    }
}
