<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsDestinosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_destinos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('personas');
            $table->string('precio');
            $table->string('incluye');
            // $table->string('vigencia');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');

            $table->string('imagen');
            $table->string('thumb');

            $table->text('contenido');
            $table->text('descripcion');
            $table->string('youtubeUrl');

            $table->string('tipo');          
            $table->string('duracion');      
            $table->text('observaciones');  

            $table->integer('hits')->nullable();

            $table->boolean('publish');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_destinos');
    }
}
