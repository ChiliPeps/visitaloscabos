<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdVendedorToCmsReservas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_reservas', function (Blueprint $table) {
            $table->integer('id_vendedor')->unsigned()->nullable()->default(null)->after('id_user');
            $table->foreign('id_vendedor')->references('id')->on('cms_vendedores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_reservas', function (Blueprint $table) {
            $table->dropColumn('id_vendedor');
        });
    }
}
