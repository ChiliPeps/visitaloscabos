<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Images extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('cms_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->text('ruta');

            $table->integer('id_destino')->unsigned()->nullable();
            $table->foreign('id_destino')->references('id')->on('cms_destinos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('cms_images');
    }
}
