<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CMSPagoReserva extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_pagosReserva', function (Blueprint $table) {
            
            $table->increments('id');

            $table->integer('id_reserva')->unsigned();
            $table->foreign('id_reserva')->references('id')->on('cms_reservas');

            $table->string('folio');
            $table->string('forma_pago')->nullable();
            $table->string('identificador')->nullable();
            $table->float('cantidad');
            $table->string('usuario');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_pagosReserva');
    }
}
