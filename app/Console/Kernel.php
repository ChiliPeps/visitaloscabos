<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Models\CMS\CMSReserva;
use App\Debtor;
use Carbon\Carbon;
use DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\AddUserCMS::class,
        Commands\AddModuleCMS::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $now = Carbon::now();
            $debtors = CMSReserva::whereBetween('limite_pago_hotel', [$now->toDateString(), $now->addDay()->toDateString()])->get();
            foreach($debtors as $data){
                Debtor::withTrashed()->firstOrCreate([
                    'id_reserva' => $data->id,
                ]);
            }
        })->name('debtorSchedule')->withoutOverlapping()->dailyAt('00:01')->timezone('America/Mazatlan');
        // })->name('debtorSchedule')->withoutOverlapping()->everyMinute()->timezone('America/Mazatlan');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
