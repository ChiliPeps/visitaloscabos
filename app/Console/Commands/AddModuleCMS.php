<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Artisan;
use File;

class AddModuleCMS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:createmodule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Module for CMS.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('*********************************');
        $this->info('Add new module to CMS');
        $this->info('*********************************');

        $templates = ['Clean Template', 'Categories Template', 'Articles Template', 'Blog Template', 'Calendar Template', 'I want more stuff!'];
        $data['template'] = $this->choice('Choose one template module form the list:', $templates, 0);

        $option = array_search($data['template'], $templates);

        switch($option) {
            case 0: $this->cleanTemplate(); break;
            case 1: $this->info('Soon...'); break;
            case 2: $this->info('Soon...'); break;
            case 3: $this->info('Soon...'); break;
            case 4: $this->info('Soon...'); break;
            case 5: $this->info('Help Me! If you want more stuff! Kisses Bye!'); break;
        }
    }

    protected function cleanTemplate()
    {
        $valid_name = false;
        while (!$valid_name) {
            $module_names = $this->nameQuestions();
            $valid_name = $this->confirm('Singluar Name = "'.$module_names['singular'].'" and Plural Name = "'
            .$module_names['plural'].'". Is this correct? [y|N]');
        }

        // App Directory
        $app_directory = app_path()."/Templates/clean_mod";

        // Views
        // index.blade.php
        $file_name = "index.blade.php"; $final_name = "index.blade.php";
        $sourceDir = $app_directory."/views";
        $destinationDir = base_path()."/resources/views/cms/".strtolower($module_names['plural']);
        $this->copyFiles($module_names, $file_name, $final_name, $sourceDir, $destinationDir);

        // inputs.blade.php
        $file_name = "inputs.blade.php"; $final_name = "inputs.blade.php";
        $sourceDir = $app_directory."/views/partials";
        $destinationDir = base_path()."/resources/views/cms/".strtolower($module_names['plural'])."/partials";
        $this->copyFiles($module_names, $file_name, $final_name, $sourceDir, $destinationDir);

        // script.blade.php
        $file_name = "scripts.blade.php"; $final_name = "scripts.blade.php";
        $sourceDir = $app_directory."/views/partials";
        $destinationDir = base_path()."/resources/views/cms/".strtolower($module_names['plural'])."/partials";
        $this->copyFiles($module_names, $file_name, $final_name, $sourceDir, $destinationDir);

        // controller.php
        $file_name = "controller.php"; $final_name = ucfirst($module_names['plural'])."Controller.php";
        $sourceDir = $app_directory."/controller";
        $destinationDir = base_path()."/app/Http/Controllers/Cms";
        $this->copyFiles($module_names, $file_name, $final_name, $sourceDir, $destinationDir);

        // migration.php
        $file_name = "migration.php";
        $final_name = Carbon::now()->format('Y_m_d_his')."_create_cms_".lcfirst($module_names['plural'])."_table.php";
        $sourceDir = $app_directory."/migration";
        $destinationDir = base_path()."/database/migrations";
        $this->copyFiles($module_names, $file_name, $final_name, $sourceDir, $destinationDir);

        // model.php
        $file_name = "model.php"; $final_name = "CMS".ucfirst($module_names['singular']).".php";
        $sourceDir = $app_directory."/model";
        $destinationDir = base_path()."/app/Models/CMS";
        $this->copyFiles($module_names, $file_name, $final_name, $sourceDir, $destinationDir);

        // request.php
        $file_name = "request.php"; $final_name = "CMS".ucfirst($module_names['plural'])."Request.php";
        $sourceDir = $app_directory."/request";
        $destinationDir = base_path()."/app/Http/Requests/CMS";
        $this->copyFiles($module_names, $file_name, $final_name, $sourceDir, $destinationDir);

        // route.php
        $file_name = "route.php"; $final_name = "CMS".ucfirst($module_names['plural']).".php";
        $sourceDir = $app_directory."/routes";
        $destinationDir = base_path()."/routes/cms";
        $this->copyFiles($module_names, $file_name, $final_name, $sourceDir, $destinationDir);

        // Lateral Side Menu
        $cms_menu_file = base_path().'/resources/views/cms/inc/items_menu_lateral.blade.php';
        $route_alias = 'admin.'.strtolower($module_names['plural']).'.index';
        $make_link_function = "{!! CMSHelpers::makeLinkForSidebarMenu('".$route_alias."', '".ucfirst($module_names['plural'])."', 'fa fa-file-o') !!}\r";
        $new_menu = File::append($cms_menu_file, $make_link_function);
        $this->info('Added new module link to Lateral Side Menu.');

        $this->info('Finished!');
    }

    protected function nameQuestions()
    {
        $singular = $this->ask('What is the module name in singular? (Example: Article)');
        $plural = $this->ask('What is the module name in plural? (Example: Articles)');
        $singular = ucfirst(strtolower(trim($singular)));
        $plural = ucfirst(strtolower(trim($plural)));
        return ['singular' => $singular, 'plural' => $plural];
    }

     /**
     * File copy Function.
     * ucfirst($plural)      -> C1XlcpX (First Capital Plural Name)
     * strtolower($plural)   -> C2XpX   (All Lowercase Plural Name)
     * ucfirst($singular)    -> C3XlcsX (First Capital Singular Name)
     * strtolower($singular) -> C4XsX   (All Lowercase Singular Name)
     *
     * @var array
     * @var string
     * @var string
     * @var string
     * @var string
     */
    protected function copyFiles($module_names, $file_name, $final_name, $sourceDir, $destinationDir)
    {
        $singular = $module_names['singular'];
        $plural   = $module_names['plural'];

        if(!File::isDirectory($destinationDir)) {
            $result = File::makeDirectory($destinationDir, 0775, true);
        }

        $original_file = $sourceDir."/".$file_name;

        $contents = File::get($original_file);
        $contents = str_replace("C1XlcpX", ucfirst($plural), $contents);
        $contents = str_replace("C2XpX", strtolower($plural), $contents);
        $contents = str_replace("C3XlcsX", ucfirst($singular), $contents);
        $contents = str_replace("C4XsX", strtolower($singular), $contents);

        $copied_file = $destinationDir."/".$final_name;
        
        File::put($copied_file, $contents);

        $this->comment('File '.$final_name.' created at: ');
        $this->info($copied_file);
    }
}
