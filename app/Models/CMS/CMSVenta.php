<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class CMSVenta extends Model
{
  
    protected $table = 'cms_ventas';


    protected $fillable = ['id','tipo', 'telefono', 'nombre','correo','hotel', 'fecha_entrada', 'fecha_salida', 'tercerAdulto', 'menores', 'edad1', 'edad2', 'precio', 'confirmacion', 'limite_pago_cliente', 'limite_pago_hotel', 'clave', 'pagado'];


    public function detalle()
    {
        return $this->hasMany('App\Models\CMS\CMSPago', 'id_venta');
    }

}