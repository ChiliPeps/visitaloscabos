<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class CMSPromocion extends Model
{
  
    protected $table = 'cms_promociones';


    protected $fillable = ['titulo', 'slug_url', 'imagen', 'thumb', 'descripcion', 'contenido','precio', 'precioTercer' ,'fecha_inicio', 'fecha_fin', 'precioTercer', 'menorPrecio1' ,'menorPrecio2', 'menorPrecio3', 'menorEdad1', 'menorEdad2', 'menorEdad3'];
}