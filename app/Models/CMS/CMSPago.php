<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class CMSPago extends Model
{
  
    protected $table = 'cms_pagos';


    protected $fillable = ['id_venta', 'folio', 'cantidad', 'usuario'];


    public function venta()
    {
        return $this->belongsTo('App\Models\CMS\CMSVenta', 'id_venta');
    }
}