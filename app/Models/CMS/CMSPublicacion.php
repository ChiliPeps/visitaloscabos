<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CMSPublicacion extends Model
{
    use SoftDeletes;

    protected $table = 'cms_publicaciones';

    protected $fillable = ['titulo', 'slugurl', 'descripcion', 'contenido', 'imagen', 'autor_foto', 'thumb', 'publish', 'hits'];
}
