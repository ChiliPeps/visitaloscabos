<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class CMSPagosReserva extends Model
{
    protected $table = 'cms_pagosReserva';


    protected $fillable = ['id_reserva', 'folio', 'cantidad', 'usuario', 'forma_pago', 'identificador'];


    public function reserva()
    {
        return $this->belongsTo('App\Models\CMS\CMSReserva', 'id_reserva');
    }
}
