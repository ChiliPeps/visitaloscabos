<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CMSReserva extends Model
{
     use SoftDeletes;

     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_reservas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_user', 'id_vendedor', 'habitaciones', 'nombre', 'telefono', 'correo','hotel','limite_pago_cliente', 'limite_pago_hotel' ,'precio', 'fecha_entrada','fecha_salida' , 'confirmacion', 'observaciones','color', 'hotel_pagado', 'reserva_pagada', 'tarifa', 'clave', 'medio_contacto', 'papeleta_enviada'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];


     public function habitacionInfo()
     {
        return $this->hasMany('App\Models\CMS\CMSHabitacion', 'id_reserva');
     }

    public function detalle()
    {
        return $this->hasMany('App\Models\CMS\CMSPagosReserva', 'id_reserva');
    }
}
