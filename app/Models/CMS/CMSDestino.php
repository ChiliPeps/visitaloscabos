<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class CMSDestino extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_destinos';

    protected $fillable = ['nombre', 'personas', 'precio', 'incluye', 'fecha_inicio', 'fecha_fin', 'imagen', 'thumb', 'contenido','descripcion' ,'youtubeUrl', 'publish', 'hits', 'tipo', 'duracion', 'observaciones'];
}
