<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class CMSImages extends Model
{
  
    protected $table = 'cms_images';


    protected $fillable = ['id', 'nombre', 'ruta', 'id_destino'];
}