<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class CMSHabitacion extends Model
{
    protected $table = 'cms_habitaciones';

    protected $fillable = ['id_reserva', 'nombre', 'adultos', 'menores', 'edad1', 'edad2', 'edad3' ];

    public function reserva()
    {
        return $this->belongsTo('App\Models\CMS\CMSReserva', 'id_reserva');
    }
}
