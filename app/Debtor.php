<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Debtor extends Model
{
    use SoftDeletes;

    protected $table = 'debtors';

    protected $fillable = ['id_reserva', 'send_messages'];

     protected $dates = ['deleted_at'];

    public function reserva()
    {
        return $this->belongsTo('App\Models\CMS\CMSReserva', 'id_reserva');
    }
}
