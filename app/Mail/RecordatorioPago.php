<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

class RecordatorioPago extends Mailable
{
    use Queueable, SerializesModels;

    public $request;
    public $info;
    public $fecha;
    public $hora;

    public $greeting = "Recordatorio de reservacion";
    public $introLines = [];
    public $outroLines = [];

    public $asunto;
    public $fechaLimite;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($info)
    {
        // dd($info);

        $this->info = $info;
        $this->asunto = 'Recordatorio VisitaLosCabosMX';
         // Tiempo Server
        // setlocale(LC_TIME, config('app.locale'));
        setlocale(LC_TIME, 'es_MX.utf8');

        $this->fecha = utf8_encode(Carbon::now()->formatLocalized('%A %d, %B %Y'));
        $this->hora  = Carbon::now()->toTimeString();
        $this->fechaLimite = Carbon::parse($info->limite_pago_cliente)->formatLocalized('%A %d, %B %Y');

        $this->introLines[] = "";

        $this->outroLines[] = "Mensaje generado automaticamente el <b>: ".$this->fecha."</b> a las <b>".$this->hora."</b> hora del servidor.";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.recordatorioPago');
    }
}
