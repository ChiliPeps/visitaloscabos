<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

class ReservaCorreo extends Mailable
{
    use Queueable, SerializesModels;

    public $request;
    public $fecha;
    public $hora;

    public $greeting = "Hola!";
    public $introLines = [];
    public $outroLines = [];



    public $asunto;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
        $this->asunto = "Nueva Reserva del Formulario de Contacto";

        // Tiempo Server
        setlocale(LC_TIME, config('app.locale'));
        $this->fecha = utf8_encode(Carbon::now()->formatLocalized('%A %d %B %Y'));
        $this->hora  = Carbon::now()->toTimeString();

        $nombre = $request->input('nombre');

        $this->introLines[] = "La persona con el nombre de <b>".$nombre."</b> ha mandado un mensaje desde la Aplicación Web en la fecha del <b>".$this->fecha."</b> a las <b>".$this->hora."</b> hora del servidor.";
        $this->outroLines[] = ""; 

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->asunto)->view('email.reserva');
    }
}
