<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

class pagoCorreo extends Mailable
{
    use Queueable, SerializesModels;

    public $request;
    public $venta;
    public $fecha;
    public $hora;

    public $greeting = "Hola!";
    public $introLines = [];
    public $outroLines = [];



    public $asunto;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($venta)
    {
        $this->venta = $venta;
        // $this->request = $request;
        $this->asunto = "Pago de reservacion";

        // Tiempo Server
        setlocale(LC_TIME, config('app.locale'));
        $this->fecha = utf8_encode(Carbon::now()->formatLocalized('%A %d %B %Y'));
        $this->hora  = Carbon::now()->toTimeString();

        $tipo          = $venta->tipo;
        $hotel         = $venta->hotel;
        $nombre        = $venta->nombre;
        $telefono      = $venta->telefono;
        $correo        = $venta->correo;
        $fecha_entrada = $venta->fecha_entrada;
        $fecha_salida  = $venta->fecha_salida;
        $tercerAdulto  = $venta->tercerAdulto;
        $menores       = $venta->menores;
        $edad1         = $venta->edad1;
        $edad2         = $venta->edad2;

        // dd($venta);
        $this->introLines[] = "La persona con el nombre <b>".$nombre."</b>hizo un </b>".$tipo."</b> desde la Aplicación Web en la fecha del <b>".$this->fecha."</b> a las <b>".$this->hora."</b> hora del servidor.";
        $this->outroLines[] = ""; 

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.pago');
    }
}
