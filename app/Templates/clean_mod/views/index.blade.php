@extends('cms.master')

@section('content')
    <section class="content-header" id="app">
        <h1><i class="fa fa-file"></i> C1XlcpX </h1>
    </section>
@endsection

@push('scripts')
    @include('cms.C2XpX.partials.scripts')
@endpush