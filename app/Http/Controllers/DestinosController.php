<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CMS\CMSDestino;
use App\Models\CMS\CMSImages;

class DestinosController extends Controller
{
    public function getDestino($id_destino)
    {
       // get Publicacion principal
        $w = ['id' => $id_destino, 'publish' => 1];
        $destino = CMSDestino::where($w)->first();
        if($destino == null) { abort(404); }
        // Incrementar Hits
        CMSDestino::where('id', $destino->id)->increment('hits');

        $images = CMSImages::where('id_destino', $destino->id)->get();

        return view('pages.destino', compact('destino', 'images'));

    }
}
