<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CMS\CMSDestino;


class InicioController extends Controller
{
    public function getHost(Request $request) {
        dd($request->url());
    }

    public function getHostWithBlog(Request $request) {
        $full_url = $request->fullUrl();
        $domain = parse_url($full_url);
        dd($domain['host']);
    }

    public function index()
    {   
        $w = ['publish' => 1];
        $destinos = CMSDestino::take(2)->orderBy('created_at', 'desc')->get();
        // $principal = CMSPublicacion::with('autor')->latest('created_at')->where($w)->first();
        // $masLeidos = CMSPublicacion::with('autor')->where($w)->orderBy('hits', 'desc')->take(3)->get();



        return view('pages.inicio', compact('destinos'));
    }
}
