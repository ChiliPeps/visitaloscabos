<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CMS\CMSVendedor;

use Auth;

class VendedoresController extends Controller
{
    public function __construct() 
    {
        $this->middleware('CMSAuthenticate');
    }

    public function index()
    {
        abort_unless(Auth::guard('cms')->user()->isAdmin(), 404);
        return view('cms.vendedores.index');
    }

    public function getVendedores()
    {
        $results = CMSVendedor::orderBy('created_at', 'desc')->paginate(20);
        return response()->json($results);
    }

    public function setVendedor(Request $request){
        $vendedor = CMSVendedor::create([
            'nombre'    => $request->nombre,
            'active'    => $request->active,
        ]);

        return response()->json(['success' => true, 'vendedor_id' => $vendedor->id]);
    }

    public function updateVendedor(Request $request){
        $vendedor = CMSVendedor::findOrFail($request->id);
        // Update
        $vendedor->fill([
            'nombre'    => $request->nombre,
            'active'    => $request->active,
        ]);
        $vendedor->save();
    }

    public function active(Request $request)
    {
        $vendedor = CMSVendedor::findOrFail($request->id);
        if ($vendedor->active == 0) { $vendedor->active = 1; } 
        else { $vendedor->active = 0; }
        $vendedor->save();
        return response()->json(['success' => true, 'active' => $vendedor->active]);
    }

    public function delete(Request $request)
    {
        $vendedor = CMSVendedor::findOrFail($request->id);
        $vendedor->delete();
    }
}
