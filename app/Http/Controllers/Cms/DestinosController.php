<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CMS\CMSDestino;
use App\Models\CMS\CMSImages;

use App\Http\Requests\CMS\CMSDestinosRequest;

use Image;
use Storage;
use DB;

class DestinosController extends Controller
{
    public function __construct() 
    {
        $this->middleware('CMSAuthenticate');
    }

    public function index()
    {
        return view('cms.destinos.index');
    }

    public function getDestinos()
    {
    	$results = CMSDestino::orderBy('created_at', 'desc')->paginate(20);
    	return response()->json($results);
    }


    public function setDestino(CMSDestinosRequest $request)
    {
    	$destino = CMSDestino::create([
    		'nombre' 		=> $request->nombre,
    		'personas'      => $request->personas,
    		'slugurl'       => $request->slugurl,
    		'precio'        => $request->precio,
    		'incluye'       => $request->incluye,
    		'fecha_inicio'  => $request->fecha_inicio,
    		'fecha_fin'     => $request->fecha_fin,
    		'contenido'     => $request->contenido,
            'descripcion'   => $request->descripcion,
    		'youtubeUrl'    => $request->youtubeUrl,
    		
            'tipo'          => $request->tipo,
            'duracion'      => $request->duracion,
            'observaciones' => $request->observaciones,

            'imagen'        => 'subiendo...',
    		'thumb' 		=> 'subiendo...',
    		'hits'			=> 0,
    		'publish' 		=> 1,
    	]);

    	// Upload File
        if($request->hasFile('imagen')) {
            $paths = $this->uploadImage($request->file('imagen'), $destino->id);
            $destino->imagen = $paths['imagen'];
            $destino->thumb  = $paths['thumb'];
            $destino->save();
        }

        return response()->json(['success' => true, 'destino_id' => $destino->id]);

    }

    public function updateDestino(CMSDestinosRequest $request)
    {
    	$destino = CMSDestino::findOrFail($request->id);
        // Update
        $destino->fill([
            'nombre' 		=> $request->nombre,
    		'personas'      => $request->personas,
    		'slugurl'       => $request->slugurl,
    		'precio'        => $request->precio,
    		'incluye'       => $request->incluye,
    		'fecha_inicio'  => $request->fecha_inicio,
    		'fecha_fin'     => $request->fecha_fin,
    		'contenido'     => $request->contenido,
            'descripcion'   => $request->descripcion,
            'youtubeUrl'    => $request->youtubeUrl,

            'tipo'          => $request->tipo,
            'duracion'      => $request->duracion,
            'observaciones' => $request->observaciones,

    		'publish' 		=> 1,
        ]);
        $destino->save();

        // Upload New Image
        if($request->hasFile('imagen')) {
            Storage::delete($destino->imagen);
            Storage::delete($destino->thumb);
            $paths = $this->uploadImage($request->file('imagen'), $destino->id);
            $destino->imagen = $paths['imagen'];
            $destino->thumb  = $paths['thumb'];
            $destino->save();
        }

        // Update Tags
        // $this->tagManager("update", $destino->id);

        return response()->json(['success' => true, 'destino_id' => $destino->id]);
    }

    public function deleteDestino(Request $request)
    {
        $destino = CMSDestino::findOrFail($request->id);
        $id_destino = $destino->id; // Guardar Id
        Storage::delete($destino->imagen);
        Storage::delete($destino->thumb);
        $destino->delete();
        return response()->json(['success' => true, 'id_promocion' => $id_destino]);
    }

	public function publish(Request $request)
    {
    	$destino = CMSDestino::findOrFail($request->id);
        if ($destino->publish == 0) { $destino->publish = 1; } 
        else { $destino->publish = 0; }
        $destino->save();
        return response()->json(['success' => true, 'publish' => $destino->publish]);
    }    

    protected function uploadImage($file, $destino_id)
    {       
        // Directories
        $directory_name = "media-manager";
        $directory      = $directory_name."/destinos/".$destino_id;

        // Save Imagen
        $file_name = $destino_id."_".time().".".$file->getClientOriginalExtension();
        $path = $file->storeAs($directory, $file_name, 'public');

        // Thumb ---->
        $img = Image::make($file->getRealPath());
        // $img->resize(186, 125); // Antigua Resolución
        $img->resize(200, 120);
        $file_name_thumb = $destino_id."_".time()."_thumb.".$file->getClientOriginalExtension();
        $path_thumb = $directory."/".$file_name_thumb;
        $img->save($path_thumb);

        return ['imagen' => $path, 'thumb' => $path_thumb];
    }

    public function tinymceImages(Request $request)
    {
        // Directory
        $directory_name = "media-manager";
        $directory      = $directory_name."/tinymce_images/";
        $file           = $request->file('file');

        // Save Imagen
        $file_name = uniqid('img_').".".$file->getClientOriginalExtension();
        $path = $file->storeAs($directory, $file_name, 'public');
        return response()->json(url($path));
    }

    protected function checkNull($value) {
        if ($value == "null") { return ""; }
        else { return $value; }
    }

    public function comprobarUrl(Request $request)
    {
        $url =  trim($request->input('url'));

        $comprobar=filter_var($url, FILTER_VALIDATE_URL);

        if(!$comprobar) 
        {
            return 0;
        }        
        $dirimg="";
        $tags = $this->get_youtube($url);

        if($tags===false)
        {
            return 0;
        }

        // dd($tags, $url);
        $titulo = $tags['title'];
        $url_string = parse_url($url, PHP_URL_QUERY);
        parse_str($url_string, $args);
        $id = isset($args['v']) ? $args['v'] : false;
        if($id==false||$titulo==""||strlen($id)!=11)
        {
            return 0;
        }
        
        //$dirimg="https://img.youtube.com/vi/".$id."/sddefault.jpg";
        $dirimg="https://i3.ytimg.com/vi/".$id."/hqdefault.jpg";

        return response()->json([
            'tags'=>$tags,
            'titulo' => $titulo,
            'urlimg' => $dirimg,
        ]);
    }

    public function get_youtube($url){

        $youtube = "https://www.youtube.com/oembed?url=". $url ."&format=json";

        $curl = curl_init($youtube);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $return = curl_exec($curl);
        curl_close($curl);
        return json_decode($return, true);

    }


    public function aloneUploadFile(Request $request)
    {   
        /*http_response_code(500);
        dd($request->all());*/
        $id = $request->input('idDestinoSelect');

        if(!$request->hasFile('file')) { http_response_code(422); }

        $file = $request->file('file');
        $nombre = $file->getClientOriginalName(); 

        $archivo = CMSIMages::create([
        // $archivo = CMSArchivo::create([
            'nombre'    => $nombre,
            'ruta'      => '',
            'id_destino' =>$id
        ]);

        //Directories
        $directory_name = "media-manager";
        $directory      = $directory_name."/cms_archivos";

        //Filename
        $file_name = $archivo->id."_.".$file->getClientOriginalExtension();

        //Store the file
        $path = $file->storeAs($directory, $file_name, 'public');

        $archivo->ruta = $path;
        $archivo->save();

        return response()->json($archivo);
    }

    public function deleteManyArchivos(Request $request)
    {
        $ids = $request->input('ids');
        $rutas = CMSImages::select('ruta')->whereIn('id', $ids)->get()->toArray();
        $rutas_to_delete = array_map(function ($item) { return $item["ruta"]; }, $rutas);
        Storage::delete($rutas_to_delete);
        DB::table('cms_images')->whereIn('id', $ids)->delete();
        return response()->json($ids);
    }

    public function getArchivos(Request $request)
    {
        $idCarpeta = $request->input('id');

        // if($request->has('tipo') && $request->has('busqueda')) {

        //     $tipo = $request->input('tipo');
        //     $busqueda = $request->input('busqueda');

        //     $results = CMSImages::where($tipo, 'LIKE', '%'.$busqueda.'%')
        //     ->orderBy('created_at', 'desc')->paginate(20);
        // } else {
            $results = CMSImages::orderBy('created_at', 'desc')->where('id_destino', $idCarpeta)->paginate(20);
        // }

        return response()->json($results);
    }
}
