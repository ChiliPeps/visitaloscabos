<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\CMS\CMSPago;
use Auth;

class PagosController extends Controller
{
    //
    public function getPagos(Request $request)
    {
        $id = $request->id;
    	$results = CMSPago::with('venta')->where('id_venta', $id)->orderBy('created_at', 'desc')->get();
        return response()->json($results);
    }

    public function registrarPago(Request $request)
    {
    	$pago = CMSPago::create([
            'folio'         => $request->folio,
    		'cantidad' 		=> $request->cantidad,
    		'id_venta'      => $request->id_venta,
    		'usuario'       => Auth::guard('cms')->user()->name,
    	]);

        return response()->json(['success' => true, 'pago_id' => $pago->id]);
    }

    public function updatePago(Request $request)
    {
        $pago = CMSPago::findOrFail($request->id);
        // Update
        $pago->fill([
            'cantidad' 		=> $request->cantidad,   
            'folio'         => $request->folio,
        ]);
        $pago->save();

        return response()->json(['success' => true, 'pago_id' => $pago->id]);
    }

    public function deletePago(Request $request)
    {
        $pago = CMSPago::findOrFail($request->id);
        //Storage::delete($archivo->ruta); // Softdelete Activated
        //Storage::delete($archivo->ruta); // Softdelete Activated
        $id_pago = $pago->id; // Guardar Id
        $pago->delete();
        return response()->json(['success' => true, 'id_pago' => $id_pago]);
    }

    public function getSuma(Request $request)
    {
        $id = $request->id;

        $pagos = CMSPago::where('id_venta', $id)->sum('cantidad');
         // dd($pagos);

        // foreach ($pagos as $pago) {
        //     $total += $pago->cantidad;
        // }

        return $pagos;
    }
}
