<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\CMS\CMSPagosReserva as CMSPago;
use App\Models\CMS\CMSReserva;

use Carbon\Carbon;
use Auth;

class PagosReservasController extends Controller
{
    //
    public function getPagos(Request $request)
    {

        $id = $request->id;        

        if($request->has('tipo') && $request->has('busqueda') && $request->input('busqueda') != '') {

            $tipo = $request->input('tipo');
            $busqueda = $request->input('busqueda');

            $results = CMSPago::with('reserva')
            ->where([
                ['id_reserva', '=', $id],
                [$tipo, 'LIKE', $busqueda.'%'],
            ])
            ->orderBy('identificador','desc')
            ->orderBy('created_at', 'desc')->get();
        }
        else{
            
    	   $results = CMSPago::with('reserva')->where('id_reserva', $id)
           ->orderBy('identificador','desc')
           ->orderBy('created_at', 'desc')->get();
        }

        $suma = $results->sum('cantidad');

        return response()->json(["results" => $results, "suma" => $suma]);
    }

    public function registrarPago(Request $request)
    {   
        // dd($request->identificador);
    	$pago = CMSPago::create([
            'folio'         => $request->folio,
    		'cantidad' 		=> $request->cantidad,
    		'id_reserva'    => $request->id_reserva,
    		'usuario'       => Auth::guard('cms')->user()->name,
            'forma_pago'    => $request->forma_pago,
            'identificador' => $request->identificador,
    	]);


        $total = CMSReserva::findOrFail($request->id_reserva)->precio;
        $pagos = CMSPago::where('id_reserva', $request->id_reserva)->sum('cantidad');
        // dd($request->id_reserva, $reserva, $pagos);
        if ($pagos == $total) {
            // dd("entro");
            $r = CMSReserva::findOrFail($request->id_reserva);
            $r->fill(['reserva_pagada' => Carbon::now()->toDateString()]);
            $r->save();
        }

        return response()->json(['success' => true, 'pago_id' => $pago->id]);
    }

    public function updatePago(Request $request)
    {
        // dd($request);
        $pago = CMSPago::findOrFail($request->id);
        // Update
        $pago->fill([
            'cantidad' 		=> $request->cantidad,   
            'folio'         => $request->folio,
            'forma_pago'    => $request->forma_pago,
            'identificador' => $this->checkNull($request->identificador),
        ]);
        $pago->save();

        $total = CMSReserva::findOrFail($pago->id_reserva)->precio;
        $pagos = CMSPago::where('id_reserva', $pago->id_reserva)->sum('cantidad');
        if ($pagos == $total) {
            $r = CMSReserva::findOrFail($pago->id_reserva);
            $r->fill(['reserva_pagada' => Carbon::now()->toDateString()]);
            $r->save();
        }
        else{
            $r = CMSReserva::findOrFail($pago->id_reserva);
            $r->fill(['reserva_pagada' => null]);
            $r->save();
        }


        return response()->json(['success' => true, 'pago_id' => $pago->id]);
    }

    public function deletePago(Request $request)
    {

        $pago = CMSPago::findOrFail($request->id);
        $id_pago = $pago->id;
        $id_reserva = $pago->id_reserva; // Guardar Id Reserva

        $pago->delete();


        $total = CMSReserva::findOrFail($id_reserva)->precio;
        $pagos = CMSPago::where('id_reserva', $id_reserva)->sum('cantidad');
        if ($pagos != $total) {
            $r = CMSReserva::findOrFail($id_reserva);
            $r->fill(['reserva_pagada' => null]);
            $r->save();
        }

        return response()->json(['success' => true, 'id_pago' => $id_pago]);
    }

    public function getSuma(Request $request)
    {
        $id = $request->id;

        $pagos = CMSPago::where('id_reserva', $id)->sum('cantidad');
        return $pagos;
    }

    protected function checkNull($value) {
        if ($value == "null") { return null; }
        else { return $value; }
    }

    protected function checkEmpty($value) {
        if ($value == "") { return null; }
        else { return $value; }
    }
}
