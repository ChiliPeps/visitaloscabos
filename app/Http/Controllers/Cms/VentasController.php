<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CMS\CMSVenta;
use App\Models\CMS\CMSPago;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests\CMS\CMSReservasRequest;

use Carbon;
use Excel;

class VentasController extends Controller
{
    public function __construct() 
    {
        $this->middleware('CMSAuthenticate');
    }

    public function index()
    {
        return view('cms.ventas.index');
    }

    public function getReservas(Request $request)
    {   
        // $idUser = Auth::guard('cms')->user()->id;
        // $type = Auth::guard('cms')->user()->type;

        if($request->has('tipo') && $request->has('busqueda')) {

            $tipo = $request->input('tipo');
            $busqueda = $request->input('busqueda');

            if ($request->has('fechas') && $request->input('fechas') != "") {

                $fechas = $request->input('fechas');

                   $results = CMSVenta::with('detalle')
                    ->where($tipo, 'LIKE', $busqueda.'%')
                    ->where(function($q) use ($fechas){
                        $q->whereBetween('fecha_entrada', [$fechas['startDate'], $fechas['endDate'] ])
                        ->orWhereBetween('fecha_salida',  [$fechas['startDate'], $fechas['endDate'] ]);
                    })->orderBy('created_at', 'desc')->paginate(20);
                   

                // Agrego el campo sumaPagos con la suma de todos los pagos
                foreach ($results as $result) {
                    $i = 0;
                    foreach ($result->detalle as $pagos)
                    {
                        $i = $i + $pagos->cantidad;
                    }
                    // unset($result->detalle);
                    $result->sumaPagos = $i;
                }
            }
            else{
 
                    $results = CMSVenta::with('detalle')->where($tipo, 'LIKE', '%'.$busqueda.'%')
                    ->orderBy('created_at', 'desc')->paginate(20);
                              
                // Agrego el campo sumaPagos con la suma de todos los pagos
                foreach ($results as $result) {
                    $i = 0;
                    foreach ($result->detalle as $pagos)
                    {
                        $i = $i + $pagos->cantidad;
                    }
                    // unset($result->detalle);
                    $result->sumaPagos = $i;
                }
                // return response()->json($results);
            }

        } else {

               $results = CMSVenta::with('detalle')->orderBy('created_at', 'desc')->paginate(20);
            
            
            // Agrego el campo sumaPagos con la suma de todos los pagos
            foreach ($results as $result) {
                $i = 0;
                foreach ($result->detalle as $pagos)
                {
                    $i = $i + $pagos->cantidad;
                }
                // unset($result->detalle);
                $result->sumaPagos = $i;
            }
     
        }

        return response()->json($results);
    }      


    public function setReserva(CMSReservasRequest $request)
    {
        // Get Id user
        $idUser = Auth::guard('cms')->user()->id;

        $t = Carbon\Carbon::now();
        $dia = $t->day;
        $year = substr($t->year, -2);

        $reserva = CMSVenta::create([
            'id_user'       => $idUser,
            'tipo'          => $request->tipo,
            'nombre'        => $request->nombre,
            'telefono'      => $request->telefono,
            'correo'        => $request->correo,

            // 'hotel'         => $request->titulo,
            'hotel'         => $request->hotel,
            'fecha_entrada' => $request->fecha_entrada,
            'fecha_salida'  => $request->fecha_salida,
            'tercerAdulto'  => $request->tercerAdulto,
            'menores'       => $request->menores,
            'edad1'         => $request->edad1,
            'edad2'         => $request->edad2,
            'precio'        => $request->precio,

            'folio'      => 'Generando...',

            
        ]);

        $reserva->save();      

        $numb = sprintf('%03d',$reserva->id);
        $folio = $dia.''.$numb.''.$year;
        $reserva->folio = $folio;
        $reserva->save();

        return response()->json(['success' => true, 'reserva_id' => $reserva->id]);
    } 

    public function updateReserva(CMSReservasRequest $request)
    {
        $reserva = CMSVenta::findOrFail($request->id);
        // Update
        $reserva->update([
            'tipo'          => $request->tipo,
            'nombre'        => $request->nombre,
            'telefono'      => $request->telefono,
            'correo'        => $request->correo,

            'hotel'         => $request->hotel,
            'fecha_entrada' => $request->fecha_entrada,
            'fecha_salida'  => $request->fecha_salida,
            'tercerAdulto'  => $request->tercerAdulto,
            'menores'       => $request->menores,
            'edad1'         => $this->checkNull($request->edad1),
            'edad2'         => $this->checkNull($request->edad2),
            'precio'        => $request->precio,
        ]);
        $reserva->save();
    }

    public function confirma(Request $request)
    {
        // $hoy = Carbon\Carbon::now();
        $hoy = date("Y-m-d");
        $reserva = CMSVenta::findOrFail($request->id);

        $reserva->fill([
            'confirmacion' => $hoy            
        ]);

        $reserva->save();

        return response($reserva->confirmacion);
    }

    public function desconfirma(Request $request)
    {
        $reserva = CMSVenta::findOrFail($request->id);
        $reserva->fill([
            'confirmacion' => null            
        ]);
        $reserva->save();

        return response()->json(['reserva' => $reserva]);
    }

    public function deleteReserva(Request $request){
        $reserva = CMSVenta::findOrFail($request->id);
        //Storage::delete($archivo->ruta); // Softdelete Activated
        //Storage::delete($archivo->ruta); // Softdelete Activated
        $id_reserva = $reserva->id; // Guardar Id
        $reserva->delete();
        return response()->json(['success' => true, 'reserva' => $id_reserva]);
    }

    public function lpc(Request $request){
        $reserva = CMSVenta::findOrFail($request->id);
        $fecha = $request->fecha;

        $reserva->fill([
            'limite_pago_cliente' => $fecha            
        ]);
        $reserva->save();

        return response()->json(['reserva' => $reserva]);        
    }

    public function lph(Request $request){
        $reserva = CMSVenta::findOrFail($request->id);
        $fecha = $request->fecha;

        $reserva->fill([
            'limite_pago_hotel' => $fecha            
        ]);
        $reserva->save();

        return response()->json(['reserva' => $reserva]);
        
    }

    public function asignarClave(Request $request){
        $reserva = CMSVenta::findOrFail($request->id);
        $clave = $request->clave;

        $reserva->fill([
            'clave' => $clave
        ]);
        $reserva->save();
        return response()->json(['success' => true, 'reserva' => $reserva]);
    }

    public function hotelPagado (Request $request){
        $reserva = CMSVenta::findOrFail($request->id);
        if ($request->action == 'restore') {
            $reserva->fill([
            'pagado' => null
            ]);
            $reserva->save();
        }
        else{
             // $today = Carbon\Carbon::now();
            $today = date("Y-m-d");

            $reserva->fill([
                'pagado' => $today
            ]);

            $reserva->save();
            
        }
        return response()->json(['success' => true, 'reserva' => $reserva]);
    }



    public function excelParameters($busqueda, $tipo){
         $results = CMSVenta::where($tipo, 'LIKE', $busqueda.'%')
                ->orderBy('created_at', 'desc')->get()->toArray();

        Excel::create('VisitaLosCabos', function($excel) use($results) {

            $excel->sheet('Sheetname', function($sheet) use($results) {
                $sheet->fromArray($results);

            });

        })->download('xls');
    }

    public function excelParametersfecha($start, $end){
        $results = CMSVenta::where(function($q) use ($start, $end){
                $q->whereBetween('fecha_entrada', [$start, $end ])
                ->orWhereBetween('fecha_salida',  [$start, $end ]);
            })->orderBy('created_at', 'desc')->get()->toArray();

        Excel::create('VisitaLosCabos', function($excel) use($results) {

            $excel->sheet('Sheetname', function($sheet) use($results) {
                $sheet->fromArray($results);

            });

        })->download('xls');
    }


    public function excelParametersBusquedaFecha($busqueda, $tipo, $start, $end){

        $results = CMSVenta::where($tipo, 'LIKE', $busqueda.'%')
                ->where(function($q) use ($start, $end){
                     $q->whereBetween('fecha_entrada', [$start, $end ])
                    ->orWhereBetween('fecha_salida',  [$start, $end ]);
                })
                ->orderBy('created_at', 'desc')->get()->toArray();

        Excel::create('VisitaLosCabos', function($excel) use($results) {

            $excel->sheet('Sheetname', function($sheet) use($results) {
                $sheet->fromArray($results);

            });

        })->download('xls');
    }

    protected function checkNull($value) {
        if ($value == "null") { return null; }
        else { return $value; }
    }

    // public function getUpdated(Request $request){
    //     $query = CMSVenta::where('id', $request->id)->get();
    // }

}
