<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CMS\CMSPromocion;

use App\Http\Requests\CMS\CMSPromocionesRequest;


use Image;
use Storage;

class PromocionesController extends Controller
{
    public function __construct() 
    {
        $this->middleware('CMSAuthenticate');
    }

    public function index()
    {
        return view('cms.promociones.index');
    }

    public function getPromociones(Request $request)
    {
    	$results = CMSPromocion::orderBy('created_at', 'desc')->paginate(20);
        return response()->json($results);
    }

    public function crearPromocion(CMSPromocionesRequest $request)
    {
    	$promocion = CMSPromocion::create([
    		'titulo' 		=> $request->titulo,
            'slug_url'      => $request->slug_url,
    		'precio' 		=> $request->precio,
            'descripcion'   => $request->descripcion,
    		'contenido'		=> $request->contenido,
            'fecha_inicio'  => $request->fecha_inicio,
            'fecha_fin'     => $request->fecha_fin,

            'precioTercer'  => $request->precioTercer,
            'menorPrecio1'  => $request->menorPrecio1,
            'menorPrecio2'  => $request->menorPrecio2,
            'menorPrecio3'  => $request->menorPrecio3,
            'menorEdad1'    => $request->menorEdad1,
            'menorEdad2'    => $request->menorEdad2,
            'menorEdad3'    => $request->menorEdad3,
    		'imagen' 		=> 'subiendo...',
    		'thumb' 		=> 'subiendo...',
    	]);

    	if($request->hasFile('imagen')) {
            $paths = $this->uploadImage($request->file('imagen'), $promocion->id);
            $promocion->imagen = $paths['imagen'];
            $promocion->thumb  = $paths['thumb'];
            $promocion->save();
        }

        return response()->json(['success' => true, 'promocion_id' => $promocion->id]);
    }

    public function updatePromocion(CMSPromocionesRequest $request)
    {
        $promocion = CMSPromocion::findOrFail($request->id);
        // Update
        $promocion->fill([
            'titulo'        => $request->titulo,
            'slug_url'      => $request->slug_url,
            'precio'        => $request->precio,
            'descripcion'   => $request->descripcion,
            'contenido'     => $request->contenido,
            'fecha_inicio'  => $request->fecha_inicio,
            'fecha_fin'     => $request->fecha_fin, 

            'precioTercer'  => $request->precioTercer,
            'menorPrecio1'  => $request->menorPrecio1,
            'menorPrecio2'  => $request->menorPrecio2,
            'menorPrecio3'  => $request->menorPrecio3,
            'menorEdad1'    => $request->menorEdad1,
            'menorEdad2'    => $request->menorEdad2,
            'menorEdad3'    => $request->menorEdad3,
        ]);
        $promocion->save();

        // Upload New Image
        if($request->hasFile('imagen')) {
            Storage::delete($promocion->imagen);
            Storage::delete($promocion->thumb);
            $paths = $this->uploadImage($request->file('imagen'), $promocion->id);
            $promocion->imagen = $paths['imagen'];
            $promocion->thumb  = $paths['thumb'];
            $promocion->save();
        }

        // Update Tags
        // $this->tagManager("update", $publicacion->id);

        return response()->json(['success' => true, 'publicacion_id' => $promocion->id]);
    }

    public function deletePromocion(Request $request)
    {
        $promocion = CMSPromocion::findOrFail($request->id);
        //Storage::delete($archivo->ruta); // Softdelete Activated
        //Storage::delete($archivo->ruta); // Softdelete Activated
        $id_promocion = $promocion->id; // Guardar Id
        $promocion->delete();
        return response()->json(['success' => true, 'id_promocion' => $id_promocion]);
    }

    public function tinymceImages(Request $request)
    {
        // Directory
        $directory_name = "media-manager";
        $directory      = $directory_name."/tinymce_images/";
        $file           = $request->file('file');

        // Save Imagen
        $file_name = uniqid('img_').".".$file->getClientOriginalExtension();
        $path = $file->storeAs($directory, $file_name, 'public');
        return response()->json(url($path));
    }

    protected function uploadImage($file, $promocion_id)
    {       
        // Directories
        $directory_name = "media-manager";
        $directory      = $directory_name."/publicaciones/".$promocion_id;

        // Save Imagen
        $file_name = $promocion_id."_".time().".".$file->getClientOriginalExtension();
        $path = $file->storeAs($directory, $file_name, 'public');

        // Thumb ---->
        $img = Image::make($file->getRealPath());
        // $img->resize(186, 125); // Antigua Resolución
        $img->resize(200, 120);
        $file_name_thumb = $promocion_id."_".time()."_thumb.".$file->getClientOriginalExtension();
        $path_thumb = $directory."/".$file_name_thumb;
        $img->save($path_thumb);

        return ['imagen' => $path, 'thumb' => $path_thumb];
    }

}
