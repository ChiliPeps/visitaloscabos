<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CMS\CMSReserva;
use App\Models\CMS\CMSVenta;
use App\Debtor;
use Carbon\Carbon;
use DB;

class AdminController extends Controller
{
    public function __construct() 
    {
        $this->middleware('CMSAuthenticate');
    }

    public function home()
    {          
        // reservas sin confirmacion en Check Central
        $reservasNoCheck = CMSReserva::whereNull('confirmacion')->get()->count();
        $reservasNoPay = CMSReserva::whereNull('hotel_pagado')->get()->count();
        $hotelPay = CMSReserva::whereNotNull('reserva_pagada')->get()->count();

        return view('cms.home', compact('reservasNoCheck','reservasNoPay', 'hotelPay'));
    }

    public function getDebtors()
    {
        // $now = Carbon::now();
        // $debtors = CMSReserva::whereNull('reserva_pagada')
        // ->whereBetween('limite_pago_cliente', [$now->toDateString(), $now->addDay()->toDateString()])
        // ->paginate(10);

        // $now = Carbon::now();
        // $debtors = CMSReserva::whereBetween('limite_pago_cliente', [$now->toDateString(), $now->addDay()->toDateString()])
        // ->paginate(5);

        $debtors = Debtor::with('reserva')->paginate(5);
        return response()->json($debtors);
    }

    public function deleteDebtors(Request $request){
        $debtor = Debtor::findOrFail($request->id);
        $id_debtor = $debtor->id; // Guardar Id
        $debtor->delete();
        return response()->json(['success' => true, 'id_debtor' => $id_debtor]);
    }

    public function getYears(){
        $years = CMSReserva::select(DB::raw('YEAR(created_at) year'))->distinct()->get()->toArray();
        
        return response()->json($years);
    }

    public function getHotelsByDay(Request $request)
    {   
        if($request->year == 'todos')
        {
            $masDias = CMSReserva::select('hotel')->selectRaw('SUM(DATEDIFF(fecha_salida,fecha_entrada)) AS dias')
            ->groupBy('hotel')
            ->orderBy('dias','desc')->take(10); 
        }
        else{
             $masDias = CMSReserva::select('hotel')->selectRaw('SUM(DATEDIFF(fecha_salida,fecha_entrada)) AS dias')
            ->whereYear('created_at', $request->year)
            ->groupBy('hotel')
            ->orderBy('dias','desc')->take(10);  
        }
        
        $dias = $masDias->pluck('dias');
        $hotelServicio = $masDias->pluck('hotel');

        return response()->json(['dias' => $dias, 'hotels' => $hotelServicio ]);
    }

    public function getHotelsByReservas(Request $request)
    {   
        if($request->year == 'todos')
        {
            $chart = CMSReserva::select('hotel', DB::raw('count(*) as total'))
                ->groupBy('hotel')
                ->orderbY('total', 'desc')->take(8);
        }

        else {
            $chart = CMSReserva::select('hotel', DB::raw('count(*) as total'))
                ->whereYear('created_at', $request->year)
                ->groupBy('hotel')
                ->orderbY('total', 'desc')->take(8);  
        }
  
        $total = $chart->pluck('total');
        $hotelServicio = $chart->pluck('hotel');

        return response()->json(['totals' => $total, 'hotels' => $hotelServicio ]);
    }

    public function getMedioContacto(Request $request)
    {
        if($request->year == 'todos')
        {
            $medio_contacto = CMSReserva::select('medio_contacto',DB::raw('count(*) as total'))
                ->whereYear('created_at', $request->year)
                ->groupBy('medio_contacto')
                ->whereNotNull('medio_contacto')->get();
        }

        else{

            $medio_contacto = CMSReserva::select('medio_contacto',DB::raw('count(*) as total'))
                ->groupBy('medio_contacto')
                ->whereNotNull('medio_contacto')->get();

        }
        

        $medio = $medio_contacto->pluck('medio_contacto');
        $medioTotal = $medio_contacto->pluck('total');

        return response()->json(['medio' => $medio, 'medioTotal' => $medioTotal ]);
    }
}
