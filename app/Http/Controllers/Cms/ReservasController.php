<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CMS\CMSReserva;
use App\Models\CMS\CMSPago;
use App\Models\CMS\CMSHabitacion;
use App\Models\CMS\CMSVendedor;
use App\Debtor;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests\CMS\CMSReservasRequest;

use Carbon;
use Excel;

class ReservasController extends Controller
{
    public function __construct() 
    {
        $this->middleware('CMSAuthenticate');
    }

    public function index()
    {
        return view('cms.reservas.index');
    }

    public function getReservas(Request $request)
    {   
        $idUser = Auth::guard('cms')->user()->id;
        $type = Auth::guard('cms')->user()->type;

        if($request->has('tipo') && $request->has('busqueda')) {

            $tipo = $request->input('tipo');
            $busqueda = $request->input('busqueda');

            if ($request->has('fechas') && $request->input('fechas') != "") {

                $fechas = $request->input('fechas');
                $tipo_fecha = $request->input('tipo_fecha'); /*nuevo campo de tipo de busquea */

                if ($type == 'suadmin') {
                   

                    if ($tipo_fecha == 'fecha_entrada') {
                        $results = CMSReserva::with('habitacionInfo' ,'detalle')
                        ->where($tipo, 'LIKE', $busqueda.'%')
                        ->where(function($q) use ($fechas){
                            $q->whereBetween('fecha_entrada', [$fechas['startDate'], $fechas['endDate'] ])
                            ->orWhereBetween('fecha_salida',  [$fechas['startDate'], $fechas['endDate'] ]);
                        })->orderBy('fecha_entrada', 'desc')->paginate(20);
                    }
                    else{
                        $results = CMSReserva::with('habitacionInfo' ,'detalle')
                        ->where($tipo, 'LIKE', $busqueda.'%')
                        ->where(function($q) use ($fechas, $tipo_fecha){
                            $q->whereBetween($tipo_fecha, [$fechas['startDate'], $fechas['endDate'] ]);
                        })->orderBy('fecha_entrada', 'desc')->paginate(20);
                    }
                }
                else{
                    if ($tipo_fecha == 'fecha_entrada') {
                        $results = CMSReserva::with('habitacionInfo' ,'detalle')
                        ->where([
                            ['id_user', $idUser],
                            [$tipo, 'LIKE', $busqueda.'%']                            
                        ])
                        ->where(function($q) use ($fechas){
                            $q->whereBetween('fecha_entrada', [$fechas['startDate'], $fechas['endDate'] ])
                            ->orWhereBetween('fecha_salida',  [$fechas['startDate'], $fechas['endDate'] ]);
                        })->orderBy('fecha_entrada', 'desc')->paginate(20);

                    }
                    else{
                        $results = CMSReserva::with('habitacionInfo', 'detalle')
                        ->where([
                            ['id_user', $idUser],
                            [$tipo, 'LIKE', $busqueda.'%']
                            
                        ])
                        ->where(function($q) use ($fechas, $tipo_fecha){
                            $q->whereBetween($tipo_fecha, [$fechas['startDate'], $fechas['endDate'] ]);
                        })->orderBy('fecha_entrada', 'desc')->paginate(20);
                    }
                    
                }    

                // Agrego el campo sumaPagos con la suma de todos los pagos
                foreach ($results as $result) {
                    $i = 0;
                    foreach ($result->detalle as $pagos)
                    {
                        $i = $i + $pagos->cantidad;
                    }
                    // unset($result->detalle);
                    $result->sumaPagos = $i;
                }
            }
            else{
                if ($type == 'suadmin') {
                    // busuqeda por nombre en habitacion
                    if ($busqueda != '' && $tipo == "nombreHabitacion") {
                        $results = CMSReserva::with('habitacionInfo','detalle')->whereHas('habitacionInfo', function($q) use($busqueda){
                           $q->where('nombre','LIKE' , '%'.$busqueda.'%');
                        })->paginate(20);
                    }
                    // busuqeda por nombre en habitacion
                    else{
                        // noexiste nombreHabitacion por eso lo cambiamos a nombre
                        if ($tipo == "nombreHabitacion") {
                            $tipo = "nombre";
                        }
                        $results = CMSReserva::with('habitacionInfo','detalle')->where($tipo, 'LIKE', '%'.$busqueda.'%')
                        ->orderBy('fecha_entrada', 'desc')->paginate(20);
                    }
                }
                else{
                    // busqueda por nombre en habitacion
                    if ($busqueda != '' && $tipo == "nombreHabitacion") {
                        $results = CMSReserva::with('habitacionInfo','detalle')->whereHas('habitacionInfo', function($q) use($busqueda){
                           $q->where('nombre','LIKE' , '%'.$busqueda.'%');
                        })->paginate(20);
                    }
                    // busqueda por nombre en habitacion
                    
                    // regresa cualquier busqueda siempre y cunado se busque por nombre
                    else if ($busqueda != '' && $tipo == "nombre") {
                        $results = CMSReserva::with('habitacionInfo', 'detalle')->where([
                            // ['id_user' , $idUser],
                            [$tipo, 'LIKE', '%'.$busqueda.'%']
                        ])->orderBy('fecha_entrada', 'desc')->paginate(20);
                    }
                    else{
                        if ($tipo == "nombreHabitacion") {
                            $tipo = "nombre";
                        }
                        $results = CMSReserva::with('habitacionInfo', 'detalle')->where([
                            ['id_user' , $idUser],
                            [$tipo, 'LIKE', '%'.$busqueda.'%']
                        ])->orderBy('fecha_entrada', 'desc')->paginate(20);
                    }
                }              
                // Agrego el campo sumaPagos con la suma de todos los pagos
                foreach ($results as $result) {
                    $i = 0;
                    foreach ($result->detalle as $pagos)
                    {
                        $i = $i + $pagos->cantidad;
                    }
                    // unset($result->detalle);
                    $result->sumaPagos = $i;
                }
                // return response()->json($results);
            }

        } else {

            if ($type == 'suadmin') {
               $results = CMSReserva::with('habitacionInfo','detalle')->orderBy('fecha_entrada', 'desc')->paginate(20);
            }
            else{
                $results = CMSReserva::with('habitacionInfo','detalle')->where('id_user', $idUser)->orderBy('fecha_entrada', 'desc')->paginate(20);
            }
        	
            // Agrego el campo sumaPagos con la suma de todos los pagos
            foreach ($results as $result) {
                $i = 0;
                foreach ($result->detalle as $pagos)
                {
                    $i = $i + $pagos->cantidad;
                }
                // unset($result->detalle);
                $result->sumaPagos = $i;
            }
     
        }

        return response()->json($results);
    }      


    public function setReserva(CMSReservasRequest $request)
    {
        $rooms = json_decode($request->infoHabitaciones);
        // Get Id user
        $idUser = Auth::guard('cms')->user()->id;

        $t = Carbon\Carbon::now();
        $dia = $t->day;
        $year = substr($t->year, -2);

        $reserva = CMSReserva::create([
            'id_user'       => $idUser,
            'id_vendedor'   => $request->id_vendedor,
            'nombre'        => $request->nombre,
            'telefono'      => $request->telefono,
            'correo'        => $request->correo,
            'habitaciones'  => $request->habitaciones,
            'hotel'         => $request->hotel,
            'fecha_entrada' => $request->fecha_entrada,
            'fecha_salida'  => $request->fecha_salida,           
            'precio'        => $request->precio,
            'observaciones' => $request->observaciones,
            'tarifa'        => $request->tarifa,
            'medio_contacto'=> $request->medio_contacto,
            'folio'         => 'Generando...',            
        ]);

        $reserva->save();      

        $numb = sprintf('%03d',$reserva->id);
        $folio = $dia.''.$numb.''.$year;
        $reserva->folio = $folio;
        $reserva->save();

        foreach ($rooms as $r) {
            $habitacion = CMSHabitacion::create([
                'id_reserva'=> $reserva->id,
                'nombre'    => $r->nombre,
                'adultos'   => $r->adultos,
                'menores'   => $r->menores,
                'edad1'     => $this->checkEmpty($r->edad1),
                'edad2'     => $this->checkEmpty($r->edad2),
                'edad3'     => $this->checkEmpty($r->edad3)
            ]);

            $habitacion->save();      
        }
        
        return response()->json(['success' => true, 'reserva_id' => $reserva->id]);
    } 

    public function updateReserva(CMSReservasRequest $request)
    {
        // dd($request->observaciones);
        $rooms = json_decode($request->infoHabitaciones);

        $reserva = CMSReserva::findOrFail($request->id);
        // Update
        // dd($request);
        $reserva->update([
            'id_vendedor'   => $this->checkNull($request->id_vendedor),
            'nombre'        => $request->nombre,
            'telefono'      => $request->telefono,
            'correo'        => $request->correo,
            'habitaciones'  => $request->habitaciones,
            'hotel'         => $request->hotel,
            'fecha_entrada' => $request->fecha_entrada,
            'fecha_salida'  => $request->fecha_salida,           
            'precio'        => $request->precio,
            'observaciones' => $this->checkNull($request->observaciones),
            'tarifa'        => $request->tarifa,
            'medio_contacto'=> $this->checkNull($request->medio_contacto),
        ]);
        // $reserva->save();

        
        $habitaciones = CMSHabitacion::where('id_reserva', $reserva->id)->get();

        // update the rooms
            foreach ($habitaciones as $key=>$value) {
                if ($key < count($rooms)) {
                    $habitacion = CMSHabitacion::findOrFail($value->id);
                    $habitacion->update([
                        'id_reserva' => $reserva->id,
                        'nombre'    => $rooms[$key]->nombre,
                        'adultos'   => $rooms[$key]->adultos,
                        'menores'   => $rooms[$key]->menores,
                        'edad1'     => $this->checkEmpty($rooms[$key]->edad1),
                        'edad2'     => $this->checkEmpty($rooms[$key]->edad2),
                        'edad3'     => $this->checkEmpty($rooms[$key]->edad3)
                    ]);
                    $habitacion->save(); 
                }
            };

        
        // add new Rooms
        if (count($rooms) > $habitaciones->count()) {
            for ($i=$habitaciones->count(); $i < count($rooms); $i++) { 
                $habitacion = CMSHabitacion::create([
                    'id_reserva'=> $reserva->id,
                    'nombre'    => $rooms[$i]->nombre,
                    'adultos'   => $rooms[$i]->adultos,
                    'menores'   => $rooms[$i]->menores,
                    'edad1'     => $this->checkEmpty($rooms[$i]->edad1),
                    'edad2'     => $this->checkEmpty($rooms[$i]->edad2),
                    'edad3'     => $this->checkEmpty($rooms[$i]->edad3),
                ]);
                $habitacion->save();
            }
        }

        // Delete Roooms
        else if (count($rooms) < $habitaciones->count()) {
            for ($i = count($rooms); $i < $habitaciones->count(); $i++) { 

                $habitacion = CMSHabitacion::findOrFail($habitaciones[$i]->id);
                $habitacion->delete();
            }
        }
    }

    public function confirma(Request $request)
    {
        // $hoy = Carbon\Carbon::now();
        $hoy = date("Y-m-d");
        $reserva = CMSReserva::findOrFail($request->id);
        $reserva->fill([
            'confirmacion' => $hoy            
        ]);
        $reserva->save();

        return response($reserva->confirmacion);
    }

    public function desconfirma(Request $request)
    {
        $reserva = CMSReserva::findOrFail($request->id);
        $reserva->fill([
            'confirmacion' => null            
        ]);
        $reserva->save();

        return response($reserva->confirmacion);
    }

    public function deleteReserva(Request $request){
        $reserva = CMSReserva::findOrFail($request->id);
        $id_reserva = $reserva->id; // Guardar Id
        $reserva->delete();
        return response()->json(['success' => true, 'reserva' => $id_reserva]);
    }

    public function lpc(Request $request){
        $reserva = CMSReserva::findOrFail($request->id);
        $fecha = $request->fecha;

        $reserva->fill([
            'limite_pago_cliente' => $fecha            
        ]);
        $reserva->save();

        // save debtor if fechalimite is today or tomorrow;
        $today = Carbon\Carbon::now()->toDateString();
        $tomorrow = Carbon\Carbon::now()->addDay()->toDateString();

        if ($fecha == $today || $fecha == $tomorrow) {
            Debtor::withTrashed()->firstOrCreate([
                'id_reserva' => $reserva->id,
            ]);
        }
        else if (Debtor::where('id_reserva', $reserva->id )->exists()) {
            Debtor::where('id_reserva', $reserva->id )->forceDelete();
        }

        return response()->json(['reserva' => $reserva]);
    }

    public function lph(Request $request){
        // dd($request);
        $reserva = CMSReserva::findOrFail($request->id);
        $fecha = $request->fecha;

        $reserva->fill([
            'limite_pago_hotel' => $fecha            
        ]);
        $reserva->save();

        return response()->json(['reserva' => $reserva]);
        
    }

    public function asignarClave(Request $request){
        $reserva = CMSReserva::findOrFail($request->id);
        $clave = $request->clave;

        $reserva->fill([
            'clave' => $clave
        ]);
        $reserva->save();

        return response()->json(['success' => true, 'reserva' => $reserva]);
    }

    public function hotelPagado (Request $request){
        $reserva = CMSReserva::findOrFail($request->id);
        if ($request->action == 'restore') {
            $reserva->fill([
            'hotel_pagado' => null
            ]);
            $reserva->save();
        }
        else{
             // $today = Carbon\Carbon::now();
            $today = date("Y-m-d");

            $reserva->fill([
                'hotel_pagado' => $today
            ]);

            $reserva->save();
            
        }
        return response()->json(['success' => true, 'reserva' => $reserva]);
    }

    public function papeleta(Request $request){
        $reserva = CMSReserva::findOrFail($request->id);
        $today = date("Y-m-d");

        if (!$reserva->papeleta_enviada) {
            $reserva->fill([
                'papeleta_enviada' => $today
            ]);
        }
        else{
            $reserva->fill([
                'papeleta_enviada' => null
            ]);
        }
        $reserva->save();
        
        return response()->json(['success' => true, 'fecha' => $reserva->papeleta_enviada]); 
    }


    public function excelProof(Request $request){
        \Carbon\Carbon::setLocale('es');
        Carbon\Carbon::setLocale('es');
        setlocale(LC_ALL, 'es_ES');

        $ids = json_decode($request->input('ids'));

        $results = CMSReserva::
        with('habitacionInfo','detalle')->
        select('id','nombre', 'folio as Clave', 'fecha_entrada as Entrada', 'fecha_salida as Salida', 'hotel', 'precio as Costo', 'tarifa', 'limite_pago_cliente','observaciones', 'color')

        // select('id','folio','nombre','correo','telefono', 'hotel','habitaciones','tarifa', 'fecha_entrada', 'fecha_salida','confirmacion as checkCentral','limite_pago_cliente as fecha Limite de Pago de cliente','limite_pago_hotel as Fecha limite de pago a hotel', 'hotel_pagado', 'reserva_pagada', 'papeleta_enviada', 'observaciones','color', 'created_at')

        ->whereIn('id' ,$ids)     
        ->orderBy('fecha_entrada', 'desc')->get();

        // Agregar Pagos en forma de Columna en results
        $results->map(function ($result, $key) {           

            // formato de fechas a todos los campos de fecha
            $result->Entrada = Carbon\Carbon::parse($result->Entrada)->format('d/M/Y'); 
            $result->Salida = Carbon\Carbon::parse($result->Salida)->format('d/M/Y'); 
            if ($result->limite_pago_cliente) {
                $result->limite_pago_cliente = Carbon\Carbon::parse($result->limite_pago_cliente)->format('d/M/Y'); 
            }
            
            // $result->Costo = sprintf('%01.2f', $result->Costo);

            // dd($result->detalle);
            // dd($result->detalle->count());
            
            if (!$result->detalle->count()) {$result->recibo = '';}

            foreach ($result->detalle as $key => $value) {                
                $result->recibo .= 'folio: '.$value->folio .', FormaPago: '.$value->forma_pago . ', Cantidad:'.$value->cantidad .', Identificador:'. $value->identificador. ', Fecha:' .$value->created_at->format('d/M/Y'). ' -- ';
            }

            foreach ($result->habitacionInfo as $key => $value) {
                $result->adultos += $value->adultos;
                $result->menores += $value->menores;
            }
        });

        // dd($results);
        // Crear Excel
        Excel::create('VisitaLosCabos', function($excel) use($results) {

            $excel->sheet('Sheetname', function($sheet) use($results) {
                $sheet->fromArray($results);
                // $sheet->freezeFirstColumn();

                $sheet->setHeight(1, 25);   

                $array =  $results;
                foreach ($array as $key => $value) {
                    $color = $value['color'];
                    $fila = $key + 2;
                    $hex = $this->rgba2hex($color);
                    // dd($color, $hex);
                    
                    $sheet->row($fila, function($row) use ($hex) { 
                        if (!$hex) {return;}
                        $row->setBackground($hex); 
                    });
                    $sheet->setHeight($fila, 20);           
                }

                // Borrar las columnas de ID, Color, Habitacion_info y Detalle
                $sheet->removeColumn('A');
                $sheet->removeColumn('J');
                $sheet->removeColumn('M');
                $sheet->removeColumn('M');
            });

        })->download('xls');
    }

    public function excelPagos(Request $request){

        // dd($request);
        $ids = json_decode($request->input('ids'));

        $results = CMSHabitacion::
                    whereHas('reserva')
                    ->whereIn('id_reserva' ,$ids)
                    ->get();
                    // ->sortByDesc('reserva.fecha_entrada');

        // dd($results);
        $results->map(function ($result, $key) {       
            $result->nombre_reserva = $result->reserva->nombre;
            $result->nombre_habitacion = $result->nombre;
            $result->adultos_en_cuarto = $result->adultos;
            $result->menores_en_cuarto = $result->menores.' ('. $result->edad1 .','.$result->edad2. ','.$result->edad3.')';
            $result->clave = $result->reserva->folio;
            $result->entrada = $result->reserva->fecha_entrada;
            $result->salida = $result->reserva->fecha_salida;
            $result->hotel = $result->reserva->hotel;
            $result->adultos = $result->adultos;
            $result->costo_reserva = $result->reserva->precio;
            $result->costo_habitacion = 'pendiente';
            $result->pago_habitacion = 'pendiente';
            $result->tarifa = $result->reserva->tarifa;
            $result->limite_pago_cliente = $result->reserva->limite_pago_cliente;
            $result->color = $result->reserva->color;
                                
                    
        });

        Excel::create('VisitaLosCabos', function($excel) use($results) {

            $excel->sheet('Sheetname', function($sheet) use($results) {
                $sheet->fromArray($results);

                $sheet->setHeight(1, 25);   
                $array =  $results;


                foreach ($array as $key => $value) {
                    $color = $value['color'];
                    $fila = $key + 2;
                    $hex = $this->rgba2hex($color);
                    // dd($color, $hex);
                    $sheet->row($fila, function($row) use ($hex) { 

                        if (!$hex) {return;}
                        $row->setBackground($hex);

                    });
                    $sheet->setHeight($fila, 20);           
                }

                $sheet->removeColumn('A');
                $sheet->removeColumn('A');
                $sheet->removeColumn('A');
                $sheet->removeColumn('A');
                $sheet->removeColumn('A');
                $sheet->removeColumn('A');
                $sheet->removeColumn('A');
                $sheet->removeColumn('A');
                $sheet->removeColumn('A');
                $sheet->removeColumn('A');
                $sheet->removeColumn('N');
                $sheet->removeColumn('O');

            });

        })->download('xls');
    }

    public function color(Request $request){
        $reserva = CMSReserva::findOrFail($request->id);
        // Update
        $reserva->update([
            'color'        => $this->checkNull($request->color),
        ]);
        $reserva->save();

        return response($reserva->color);
    } 

    protected function checkNull($value) {
        if ($value == "null") { return null; }
        else { return $value; }
    }

    protected function checkEmpty($value) {
        if ($value == "") { return null; }
        else { return $value; }
    }

    public function getVendedores(){
        $vendedores = CMSVendedor::all()->where('active',1);
        // dd($vendedores);
        return response()->json($vendedores);
    }

    public function rgba2hex($string) {
        if (!$string) { return;}
        $rgba  = array();
        $hex   = '';
        $regex = '#\((([^()]+|(?R))*)\)#';
        if (preg_match_all($regex, $string ,$matches)) {
            $rgba = explode(',', implode(' ', $matches[1]));
        } else {
            $rgba = explode(',', $string);
        }


        // $rr = dechex($rgba['0']);
        // $gg = dechex($rgba['1']);
        // $bb = dechex($rgba['2']);
        $rr = sprintf("%02X", $rgba['0']);
        $gg = sprintf("%02X", $rgba['1']);
        $bb = sprintf("%02X", $rgba['2']);

        if (array_key_exists('3', $rgba)) {
            $round = round($rgba['3'] * 255);
            $aa = dechex($round);
        }

        // return strtolower("#$rr$gg$bb$aa");
        return strtolower("#$rr$gg$bb");
    }

}
