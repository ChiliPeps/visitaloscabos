<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Models\CMS\CMSVenta;
use App\Models\CMS\CMSPago;
use App\Models\CMS\CMSConfiguration;
use App\Mail\pagoCorreo;
// use App\Mail\ContactoCorreo;
use Mail;
use Carbon;

class StripeController extends Controller
{
	public function success(Request $request)
    {
    	$datos = Session::get('datos');

    	if (!$request->has('key_id')) {abort(404);}

    	$clave = Session::get('clave');
    	if ($clave != $request->key_id) {abort(404);}



    	$venta = CMSVenta::create([
        	'tipo'          => $datos['tipo'],
        	'nombre' 		=> $datos['nombre'],
        	'correo'        => $datos['correo'],
        	'telefono' 		=> $datos['telefono'],
        	'hotel' 		=> $datos['tituloHotel'],
        	'fecha_entrada' => $datos['fechaInicio'],
        	'fecha_salida'	=> $datos['fechaFin'],
        	'tercerAdulto'  => $datos['tercerAdulto'],
        	'menores' 		=> $datos['menores'],
        	'precio' 		=> $datos['precio'],
        	'edad1'         => $datos['edad1'],
        	'edad2'         => $datos['edad2'],
        	'folio'         => 'Generando...',
       		]);

	        $venta->save();

	        // obtener datos de fecha para generar el folio
	    	$t = Carbon\Carbon::now();
	        $dia = $t->day;
	        $year = substr($t->year, -2);
        	//folio con 3 digitos del ID 
	        $numb = sprintf('%03d',$venta->id);
	        $folio = $dia.''.$numb.''.$year;
	        $venta->folio = $folio;
	        $venta->save();

	    if ($datos['tipo'] == 'reserva') {$cantidad = 500;}
	    else{$cantidad = $datos['precio'];}

	    $pago = CMSPago::create([
	    	'id_venta'		=> $venta->id,
	    	'cantidad'      => $cantidad, 
	    	'usuario'   	=> 'Automatico',
	    	'folio'         => 'Automatico',
	    ]);
	    $pago->save();

	        // Enviar Correo
	        $conf = CMSConfiguration::first();
	        Mail::to($conf->correo_contacto)
	        ->send(new pagoCorreo($venta));

	       Session::forget('clave');
	       return view('pages.success');
       
    }

    public function saveDatos(Request $request)
	{	
		$value = $request->input();
		Session::put('datos', $value);

		return response()->json(['success' => true, 'response' => $value]);
	}

	public function prueba(Request $request)
	{
		$clave = uniqid();
		Session::put('clave', $clave);

		$precio = $request->input('precio');
		$amount = intval($precio * 100);

       \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

		$session = \Stripe\Checkout\Session::create([
		  'payment_method_types' => ['card'],
		  'line_items' => [[
		    'price_data' => [
		      'currency' => 'mxn',
		      'product_data' => [
		        'name' => 'Visita los Cabos',
		      ],
		      'unit_amount' => $amount,
		    ],
		    'quantity' => 1,
		  ]],
		  'mode' => 'payment',
		  'success_url' => env('APP_URL').'/success?key_id='.$clave,
		  // 'success_url' => env('APP_URL').'/visitaloscabos/public/success?key_id='.$clave,
		  'cancel_url' => env('APP_URL').'/error',
		]);

		return response()->json($session->id);
	}

	

}
