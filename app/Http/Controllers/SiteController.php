<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CMS\CMSDestino;
use App\Models\CMS\CMSPublicacion;
use App\Models\CMS\CMSPromocion;
use App\Models\CMS\CMSImages;

class SiteController extends Controller
{
    public function inicio()
    {
        $w = ['publish' => 1];
        $destinos = CMSDestino::where($w)->orderBy('created_at', 'desc')->get();

        $publicaciones = CMSPublicacion::where($w)->orderBy('created_at', 'desc')->take(3)->get();

        return view('pages.inicio', compact('destinos', 'publicaciones'));
    }

    public function hoteles()
    {
        return view('pages.hoteles');
    }

    public function promociones()
    {
        $promociones = CMSPromocion::orderBy('created_at', 'desc')->get();        
        return view('pages.promociones', compact('promociones'));
    }


	public function contacto()
    {
        return view('pages.contacto');
    }

    public function galeria()
    {
    	return view('pages.galeria');
    }

    public function destino()
    {
        return view('pages.destino');
    }

    public function blog()
    {
        $w = ['publish' => 1];

        $publicaciones =CMSPublicacion::where($w)->orderBy('created_at', 'desc')->paginate(3);
        // dd($publicaciones);
        $masLeidas = CMSPublicacion::where($w)->orderBy('hits', 'desc')->take(3)->get();

        return view('pages.blog', compact('publicaciones', 'masLeidas'));
    }

    public function success()
    {
        // enviar el correo
        
        return view('pages.success');
    }

    


}