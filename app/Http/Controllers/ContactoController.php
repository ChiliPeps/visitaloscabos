<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactoRequest;
use App\Http\Requests\ReservaRequest;
use App\Models\CMS\CMSConfiguration;
use App\Mail\ContactoCorreo;
use App\Mail\ReservaCorreo;
use App\Mail\RecordatorioPago;
use App\Debtor;
use Mail;
// 
use App\Models\CMS\CMSReserva;
use App\Jobs\DebtorsMail;


class ContactoController extends Controller
{
    public function enviarCorreo(ContactoRequest $request)
    {
    	$conf = CMSConfiguration::first();
        if ($conf == null) { return response()->json(['nodata' => true, 'error' => 'No hay configuración.'], 422); }
        Mail::to($conf->correo_contacto)->send(new ContactoCorreo($request));
        return response()->json(['success' => true]);
    }

    public function enviarReserva(ReservaRequest $request)
    {
    	$conf = CMSConfiguration::first();
        if ($conf == null) { return response()->json(['nodata' => true, 'error' => 'No hay configuración.'], 422); }
        Mail::to($conf->correo_contacto)->send(new ReservaCorreo($request));
        return response()->json(['success' => true]);
    }

    public function recordatorioPago(Request $request){

        Mail::to($request->correo)
        ->cc('reservareal2@hotmail.com')
        ->send(new RecordatorioPago($request));

        Debtor::where('id_reserva', $request->id)->increment('send_messages');


        return response()->json(['success' => true]);
    }

    public function recordatorioPagoMultiple(){
        $mails = CMSReserva::all();

        // dispatch(new DebtorsMail($mails));
        
        foreach ($mails as $mail) {
            dispatch(new DebtorsMail($mail));
        }

    }
}
