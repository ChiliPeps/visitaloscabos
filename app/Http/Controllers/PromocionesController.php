<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CMS\CMSPromocion;

class PromocionesController extends Controller
{
    public function getPromociones()
    {
        $results = CMSPromocion::orderBy('created_at', 'desc')->paginate(20);
        return response()->json($results);

    }

    public function getPromo($id_promo)
    {
    	// get Publicacion principal
        $w = ['id' => $id_promo];
        $promo = CMSPromocion::where($w)->first();
        if($promo == null) { abort(404); }

        return view('pages.promocion', compact('promo'));
    }


    public function getPromodos($slug)
    {
        // get Publicacion principal
        // $id_promo = 1;
        $w = ['slug_url' => $slug];
        $promo = CMSPromocion::where($w)->first();
        if($promo == null) { abort(404); }

        return view('pages.promocion', compact('promo'));
    }

    public function getPromotres($slug, $id)
    {
        // get Publicacion principal
        // $id_promo = 1;
        $w = ['slug_url' => $slug];
        $promo = CMSPromocion::where($w)->first();
        if($promo == null) { abort(404); }

        return view('pages.promocionReserva', compact('promo'));
    }

    public function getPromoId(Request $request)
    {
        // $id = CMSPromocion::findOrFail($request->idPromo);
        $id = $request->input('id');

        $results = CMSPromocion::where('id', $id)->get();
        return response()->json($results);
    }
}
