<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CMS\CMSPublicacion;

class PublicacionesController extends Controller
{
    public function getPublicacion($id_publicacion)
    {
       // get Publicacion principal
        $w = ['id' => $id_publicacion, 'publish' => 1];
        $publicacion = CMSPublicacion::where($w)->first();
        if($publicacion == null) { abort(404); }
        // Incrementar Hits
        CMSPublicacion::where('id', $publicacion->id)->increment('hits');


        $nuevas = CMSPublicacion::where('publish', 1)->orderBy('created_at', 'desc')->where('id', '<>' ,$publicacion->id)->take(2)->get();

        return view('pages.publicacion', compact('publicacion', 'nuevas'));

    }
}
