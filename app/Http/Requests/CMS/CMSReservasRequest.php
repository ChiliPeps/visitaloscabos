<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;

class CMSReservasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'            => 'required',
            'telefono'          => 'required|min:10',
            'correo'            => 'email|required',
            'hotel'             => 'required',
            'precio'            => 'required',
            'fecha_entrada'     => 'required',
            'fecha_salida'      => 'required',
            // 'nombre'            => 'requred',
        ];
    }
}
