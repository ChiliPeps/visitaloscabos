<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;

class CMSPromocionesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo'        => 'required',
            'precio'        => 'required',
            'precioTercer'  => 'required',
            'contenido'     => 'required',
            'imagen'        => 'required',
            'fecha_inicio'  => 'required',
            'menorPrecio1'  => 'required',
            'menorPrecio2'  => 'required',
            'menorPrecio3'  => 'required',
            'menorEdad1'    => 'required',
            'menorEdad2'    => 'required',
            'menorEdad3'    => 'required',
        ];
    }
}
