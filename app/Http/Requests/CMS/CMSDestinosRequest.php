<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;

class CMSDestinosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'        => 'required',
            'precio'        => 'required',
            'imagen'        => 'required',
            'contenido'     => 'required',
            'descripcion'   => 'required',
            'youtubeUrl'    => 'required',
            'observaciones' => 'required'
        ];
    }
}
