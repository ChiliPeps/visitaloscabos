<?php

namespace App\Helpers;

use Carbon\Carbon;

class PageHelpers {

    // date("w")
    public static $dias = array(
        0  => "Domingo",
        1  => "Lunes",
        2  => "Martes",
        3  => "Miércoles",
        4  => "Jueves",
        5  => "Viernes",
        6  => "Sábado"
    );

    public static $meses = array(
        1  => "Enero",
        2  => "Febrero",
        3  => "Marzo",
        4  => "Abril",
        5  => "Mayo",
        6  => "Junio",
        7  => "Julio",
        8  => "Agosto",
        9  => "Septiembre",
        10 => "Octubre",
        11 => "Noviembre",
        12 => "Diciembre"
    );

    public static function dateString($date)
    {
        if ($date == null) { return ""; }
        $datestr = strtotime($date);
        $numdia  = date("d", $datestr);
        $month   = date("n", $datestr);
        $year    = date("Y", $datestr);
        $strdate = $numdia." de ".self::$meses[$month]." de ".$year;
        return $strdate;
    }

    public static function dateStringWithDay($date)
    {
        if ($date == null) { return ""; }
        $datestr = strtotime($date);
        $dia     = date("w", $datestr);
        $numdia  = date("d", $datestr);
        $month   = date("n", $datestr);
        $year    = date("Y", $datestr);
        $strdate = self::$dias[$dia].", ".$numdia." de ".self::$meses[$month]." de ".$year;
        return $strdate;
    }

    public static function getTime($date)
    {
        if ($date == null) { return ""; }
        $datestr = strtotime($date);
        $time = date('H:i:s', $datestr);
        return $time;
    }

    public static function dateStringWithDayMongo($createdAt)
    {
        $date = $createdAt->toDateTime()->format(DATE_ATOM);
        $datestr = strtotime($date);
        $dia     = date("w", $datestr);
        $numdia  = date("d", $datestr);
        $month   = date("n", $datestr);
        $year    = date("Y", $datestr);
        $strdate = self::$dias[$dia].", ".$numdia." de ".self::$meses[$month]." de ".$year;
        return $strdate;
    }

    public static function getTimeMongo($createdAt)
    {
        $date = $createdAt->toDateTime()->format(DATE_ATOM);
        $datestr = strtotime($date);
        $dateInLocal = date("h:i", $datestr);
        return $dateInLocal;
    }

    public static function dataUri($data, $mime)
    {
        $base64 = base64_encode($data);
        return ('data:' . $mime . ';base64,' . $base64);
    }

    public static function youtube_url_to_embed($youtube_url)
    {
        $search = '/youtube\.com\/watch\?v=([a-zA-Z0-9]+)/smi';
        $replace = "youtube.com/embed/$1";
        $embed_url = preg_replace($search, $replace, $youtube_url);

        $fix = substr($embed_url, 0, 41);
        return $fix;

        // preg_replace("#(^|[\n ])([\w]+?://)(www\.youtube)(\.[\w\.]+?/watch\?v=)([\w-]+)([&][\w=+&;%-_]*)*(^[\t <\n\r\]\[])*#is",'
    }

    public static function tagsToString($tags)
    {   
        $len = count($tags); $text = "";
        if ($len == 0) { return ""; }
        for ($i = 0; $i < $len; $i++) { 
            if ($i == $len - 1) {
                $text .= $tags[$i]['tag']['nombre'];
            } else {
                $text .= $tags[$i]['tag']['nombre'].", ";
            }
        }
        return $text;
    }
}

?>