<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
// 
use App\Mail\RecordatorioPago;
use Mail;

class DebtorsMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $details;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($details)
    {
         $this->details = $details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // foreach ($this->details as $mail) {
        //     $email = new RecordatorioPago($mail);
        //     Mail::to($mail['correo'])->send($email);
        // }

        $email = new RecordatorioPago($this->details);
        Mail::to($this->details['correo'])->send($email);
    }
}
