{{-- Requerimientos: Bootstrap 3, Vue.js 2, VueHelperFunctions.blade.php -> Lodash.js, Laravel 5.3 --}}
{{-- Requiere Componente vueDateRangePicker antes de inicializar este. --}}
<script>
Vue.component('datefiltersearch2', {
    props: ['selected', 'options', 'selected_fecha', 'options_fecha'],
    template: `
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="busqueda">Busqueda</label>
                <input label="Busqueda" name="busqueda" type="text" class="form-control" v-model="busqueda" placeholder="{{trans('cms.search_bar')}}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="tipo">Por</label>
                <select id="tipo" name="tipo" class="form-control" v-model="tipo" @change="filterSearch_tipo">
                    <option v-for="o in options" :value="o.value">@{{ o.text }}</option>
                </select>
            </div>
        </div>

        <div class="col-md-2" v-if="dateRangeActive">
            <div class="form-group">
                <label for="tipo_fecha">Filtro Fecha</label>
                <select id="tipo_fecha" name="tipo_fecha" class="form-control" v-model="tipo_fecha" 
                @change="filterSearch_tipo_fecha">
                    <option v-for="x in options_fecha" :value="x.value">@{{ x.text }}</option>
                </select>
            </div>
            
        </div>

        <div class="col-md-2" v-if="dateRangeActive">
            <label>Rango Fechas</label>
            <daterangepicker v-model="fechas"></daterangepicker>
        </div>
        <div class="col-md-1" style="padding-top: 24px;">
            <button class="btn btn-grey" @click="toggleDateRangePicker">@{{textButtonDateRange}}</button>
        </div>
    </div>
    `,
    mounted: function () {
        this.tipo = this.selected;
        this.tipo_fecha = this.selected_fecha;
    },
    data: function () {
        return {
            tipo: '',
            busqueda: '',
            tipo_fecha:'',
            fechas: '',
            dateRangeActive: false,
            textButtonDateRange: "Agregar Filtrar por Fechas"
        }
    },
    watch: {
        busqueda: function () {
            this.filterSearch();
        },
        fechas: function (val) {
            this.filterSearch_fechas();
        }
    },
    methods: {
        //Filter Search
        filterSearch: _.debounce(function () {
            this.emitUpdateFilters();
        }, 500),

        filterSearch_tipo: _.debounce(function () {
            if(this.busqueda == '') { return; }
            this.emitUpdateFilters();
        }, 500),

        filterSearch_fechas: _.debounce(function () {
            if(this.fechas == '') { return; }
            this.emitUpdateFilters();
        }, 500),

        filterSearch_tipo_fecha: _.debounce(function () {
            if(this.tipo_fecha == '') { return; }
            this.emitUpdateFilters();
        }, 500),

        emitUpdateFilters: function () {
            this.$emit("updatefilters", { busqueda: this.busqueda, tipo: this.tipo, fechas: this.fechas, tipo_fecha:this.tipo_fecha }); 
        },

        toggleDateRangePicker: function () {
            this.dateRangeActive = !this.dateRangeActive;
            if(this.textButtonDateRange == "Agregar Filtrar por Fechas") {
                this.textButtonDateRange = "Quitar Fechas";
            } else {
                this.fechas = '';
                this.emitUpdateFilters();
                this.textButtonDateRange = "Agregar Filtrar por Fechas";
            }
        }
    }
});
</script>