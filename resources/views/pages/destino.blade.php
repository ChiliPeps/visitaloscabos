@extends('master')

@php
    $url = url("/destino/".$destino->id);
    $imagen = url('/')."/".$destino->imagen;

@endphp

@section('title')
    {{$destino->nombre}}
@stop

@section('content')
    <div id="contentapp" v-cloak>
        @include('content.destino.destino')
    </div>
@stop

@push('css')
	<style>
		.portada{
			height: 700px;
			display: inline-block;
    		overflow: hidden;
		}
		.portada img {

		  	width: 100vw;
			}
	
	</style>
@endpush

@push('scripts')
<script>
  var contentapp = new Vue({
    mounted: function () {},

    el: '#contentapp',
    data: {
        
        public_url: "{{ URL::to('/') }}/",
        fecha_now: '',
        destino_url: "{{$url}}",
        imagen:"{{$destino->imagen}}",
        id: "{{$destino->id}}",
        titulo:"{{$destino->nombre}}"

    },
    computed: {

    },
    methods: {   

        shareFacebook: function () {
            window.open("https://facebook.com/sharer.php?display=popup&u=" + this.destino_url, "facebook_share", 
            "height=320, width=640, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no")
        },

        shareTwitter: function () {
            window.open("https://twitter.com/share?text="+this.titulo+"&url="+this.destino_url, "twitter_share",
            "height=320, width=640, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no")
        },

        shareInstagram: function () {
            window.open("https://instagram.com/share?text="+this.titulo.nombre+"&url="+this.destino_url, "twitter_share",
            "height=320, width=640, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no")
        },

        shareWhatsApp: function () {
            window.open("https://wa.me/?text="+this.destino_url);
        },

        shareEmail: function () {
            location.href = "mailto:?subject="+this.titulo+"&body="+this.destino_url;
        },

        urlGenerator: function (destino) {
           return this.public_url + 'destino/' + destino.id;
        },

    }
});
</script>

	{!! Html::script('js/bootstrapstudio/Filterable-Gallery-with-Lightbox.js') !!}
@endpush