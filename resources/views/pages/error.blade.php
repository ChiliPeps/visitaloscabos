@extends('master')

@section('content')
    <div id="contentapp" v-cloak>
      <div class="photo-gallery"></div>

        <section id="filtr-gallery" style="padding-top: 250px;">
            <div class="container">
                <h1 class="text-center">Hubo un Error en el Pago</h1>

                <a  href="{{route('inicio')}}" class="btn btn-success btn-xl text-uppercase">
                  Volver a intentar
                </a>
            </div>
        </section>

    </div>
@stop

@push('scripts')
<script>
  var contentapp = new Vue({
    mounted: function () {},

    el: '#contentapp',
    data: {        

    },
    computed: {

    },
    methods: {   

    }
});
</script>
<script>
  $(document).ready(function() {
      var heightSlider = $('.navbar').outerHeight();
      var pixels = heightSlider + 50;
      $('.principal').css({ marginTop : pixels  + 'px' });
  });
</script>
@endpush