@extends('master')

@section('content')
    <div id="contentapp" v-cloak>
        @include('content.blog.blog')
    </div>
@stop

@push('css')

@endpush