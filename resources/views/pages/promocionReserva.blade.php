@extends('master')

@section('content')
    <div id="contentapp" v-cloak>
        @include('content.promociones.promocionReserva')
    </div>
@stop


@push('css')
<style>

.edades {
    float: right;
    display: block;
    width: 40%;
    /*height: calc(2.25rem + 2px);*/
    height: 30px;
    padding: .375rem .75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}

.residentes{
    font-family: arial black;
    font-size: 50px;
    color: rgb(0,0,0);
    margin: 0px;
    text-align: center;
    display: block !important;
    vertical-align: middle;
}

.colorblanco{
    color:white;
}

/*stripe*/
.StripeElement {
  box-sizing: border-box;

  height: 40px;

  padding: 10px 12px;

  border: 1px solid transparent;
  border-radius: 4px;
  background-color: white;

  box-shadow: 0 1px 3px 0 #e6ebf1;
  -webkit-transition: box-shadow 150ms ease;
  transition: box-shadow 150ms ease;
}

.StripeElement--focus {
  box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid {
  border-color: #fa755a;
}

.StripeElement--webkit-autofill {
  background-color: #fefde5 !important;
}

.input-group-addon{
    color:white;
}

</style>
@endpush

@push('scripts')

@include('components.vue.vueHelperFunctions')
@include('components.vue.vueDateRangePicker')
@include('components.vue.vueDatepicker')
@include('components.vue.vueNotifications')
<script>
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#csrf-token').getAttribute('content');
var contentapp = new Vue({
    mounted: function () {this.getData(this.dataRoute), this.hoyEs();},
    el: '#contentapp',

    mixins: [notifications],

    data: {
        idPage:1,
    	// panelPromocion:true,
    	// panelReservar:false,
        fechas:'',
        cuantoskids:'',
        datos:'',
        tituloHotel:'',
        //edades de infantes 
        edad1:'',
        edad2:'',

        precioPromo:0,
        dias:0,
        total:0,

        dataRoute: "{{route('promoXid')}}",
        public_url: "{{ URL::to('/') }}/",
        promociones:null,
        pagination:null,

        promoSelected:{id:'', titulo:'', precio:'', precioTercer:'', contenido:'', fechaInicio:'', fechaFin:'', menorPrecio1:'', menorPrecio2:'', menorPrecio3:'', menorEdad1:'', menorEdad2:'', menorEdad3:''
        },

        // enviarCorreo

        contacto:{tipo:'', nombre:'', telefono:'', correo:'', comentario:'', tituloHotel:'', fechaInicio:'', fechaFin:'', 
                    tercerAdulto:'', menores:'0', edad1:'0', edad2:'0', precio:''},
        edades:[],
        correoEnviado:false,
        boton:true,
        loading:false,
        saveInAction: false,
        errores:null,
        promocionFechaInicio:'',
        promocionFechaFin:'',
        // public_url:"{{ URL::to('/') }}/",

        preciomenor:0,
        preciomenor2:0,        
        adultosTotal:0,
        tercerAdultoPrecio:0,

        tercerAdulto:false,

        pagoDatosID:'',
        pagoRoute: "{{route('pago')}}",

        check:true,
        checkDatos:false,
        hoy:'',
    },

    watch:{
        // fechas:function(){
        //     var precio = (this.precioPromo * 2)
        //     var one = this.fechas.startDate.substring(8, 10);
        //     var two = this.fechas.endDate.substring(8, 10);
        //     var days = two - one;            
        //     this.dias =  days;
        //     this.adultosTotal = (days * precio);  

        //     this.contacto.fechaInicio = this.fechas.startDate;
        //     this.contacto.fechaFin = this.fechas.endDate;  

        //     this.sacarTotal();
        // }
    },
    computed: {

    },
    methods: {
        limpiarfechaFin:function(){
            this.contacto.fechaFin="";
            this.fechas=false
        },
        
        calculo:function (){
            if (this.contacto.fechaInicio < this.contacto.fechaFin)
            {  
                this.fechas = true;
                var precio = (this.precioPromo * 2)

                dt1 = Date.parse(this.contacto.fechaInicio);
                dt2 = Date.parse(this.contacto.fechaFin);
                var timeDiff = dt2 - dt1;
                var days = Math.floor(timeDiff / (1000 * 60 * 60 * 24));

                // var days = two - one;      
                if (days == 1) {
                    this.notificationError('Error','Minimo 2 noches','topCenter');
                    this.fechas = false;
                    return;
                }      
                this.dias =  days;
                this.adultosTotal = (days * precio);  

                this.sacarTotal();
            }
            else{
                // alert("debe seleccionar la fecha correctamente");
                this.notificationError('Error','Las fechas no estan en el rango','topCenter');
                this.contacto.fechaInicio=""
                this.limpiarfechaFin();
                // this.contacto.fechaFin=""
            }
            
        },

        hoyEs: function (){
            var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
            var fecha = (new Date(Date.now() - tzoffset)).toISOString().slice(0,-1).slice(0, 10);
            this.hoy = fecha;
        },
        getData: function (url) {
            this.loading = true;
            var idPromo = window.location.href.split("/").pop();
            var filter = {id: idPromo};
            var resource = this.$resource(url);
            resource.get(filter).then(function (response) {
                this.promoSelected = response.data[0];
                this.loading = false;

                this.contacto.tituloHotel = this.promoSelected.titulo
                this.precioPromo = this.promoSelected.precio
                this.promocionFechaInicio = this.promoSelected.fecha_inicio
                this.promocionFechaFin = this.promoSelected.fecha_fin
            });

        },   

        reservar: function(idPromocion){
            
            var index = _.findIndex(this.promociones, function(o) { return o.id === idPromocion; });
            this.promoSelected ={
                id:             this.promociones[index].id,
                titulo:         this.promociones[index].titulo,
                precio:         this.promociones[index].precio,
                precioTercer:   this.promociones[index].precioTercer,              
                fecha_inicio:   this.promociones[index].fecha_inicio,
                fecha_fin:      this.promociones[index].fecha_fin,

                menorPrecio1:   this.promociones[index].menorPrecio1,
                menorPrecio2:   this.promociones[index].menorPrecio2,
                menorPrecio3:   this.promociones[index].menorPrecio3,
                menorEdad1:     this.promociones[index].menorEdad1,
                menorEdad2:     this.promociones[index].menorEdad2,
                menorEdad3:     this.promociones[index].menorEdad3,
            }

            this.contacto.tituloHotel = this.promoSelected.titulo
            this.precioPromo = this.promoSelected.precio
            this.promocionFechaInicio = this.promoSelected.fecha_inicio
            this.promocionFechaFin = this.promoSelected.fecha_fin
            // this.panelPromocion = false
            // this.panelReservar  = true
            this.scrollToTop();
        },

        checkImage: function (image_url) {
            if (!/^(f|ht)tps?:\/\//i.test(image_url)) {
                return this.public_url+image_url
            } else { return image_url }
        },

        // EnviarCorreo
        enviar: function () {
            if (this.contacto.fechaInicio < this.promocionFechaInicio || this.contacto.fechaFin > this.promocionFechaFin) {
            this.notificationError('Error','Las fechas no estan en el rango','topCenter');
            }

            else{
            if(this.saveInAction == true) { return; }
            // this.edadesAñadir();
            this.saveInAction = true;
            this.loading = true;
            this.errores = null;

            var form = new FormData();
            form.append('nombre',       this.contacto.nombre);
            form.append('tituloHotel',  this.contacto.tituloHotel);
            form.append('correo',       this.contacto.correo);
            form.append('telefono',     this.contacto.telefono);
            form.append('comentario',   this.contacto.comentario);
            form.append('fechaInicio',  this.contacto.fechaInicio);
            form.append('fechaFin',     this.contacto.fechaFin);
            form.append('menores',      this.contacto.menores);
            form.append('edad1',        this.edad1);
            form.append('edad2',        this.edad2);


            var resource = this.$resource("{{route('reserva.send')}}");
            resource.save(form).then(function (response) {
                this.loading = false;
                this.limpiar();
                this.saveInAction = false;
                this.correoEnviado = true;
                this.boton = false;
                this.notification('fa fa-check-circle', 'OK!', "Correo enviado correctamente.", 'topCenter');
                // this.panelPromocion = true
                // this.panelReservar = false
            }, function (response) {
                this.loading = false;
                this.saveInAction = false;

                // Checar si hay configuración
                if (response.data.nodata) {
                    alert(response.data.error);
                } else {
                    this.errores = response.data.errors;
                }
            });
            }
        },

        onlyNumber ($event) {
           //console.log($event.keyCode); //keyCodes value
           let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
           if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
              $event.preventDefault();
           }
        },

        limpiar: function(){
            this.contacto = {nombre:'', telefono:'', correo:'', comentario:''}
        },

        scrollToTop() {
            window.scrollTo(0,300);
        },

        edadesAñadir:function(value){
            this.edades.push({edad: this.edad1}) 
            this.edades.push({edad: this.edad2})  
        },

        cuantosMenores:function(){
            if (this.contacto.menores == 0) {
                this.edad1 = 0;
                this.edad2 = 0;
                this.calculaPrecioMenor();
                this.calculaPrecioMenor2();
            }
            else if(this.contacto.menores == 1) {
                this.edad2 = 0;
                this.calculaPrecioMenor2();
            }
        },

        calculaPrecioMenor:function(){
            if (this.edad1 == 0) {this.preciomenor=0;}
            else if (this.edad1 <= this.promoSelected.menorEdad1) 
                {
                    this.preciomenor = (this.promoSelected.menorPrecio1 * this.dias);
                }
            else if(this.edad1 > this.promoSelected.menorEdad1 && this.edad1 <= this.promoSelected.menorEdad2)
            {
                this.preciomenor = (this.promoSelected.menorPrecio2 * this.dias);
            }
            else{
                this.preciomenor = (this.promoSelected.menorPrecio3 * this.dias);
                }

            this.sacarTotal();
        },

        calculaPrecioMenor2:function(){
            if (this.edad2 == 0) {this.preciomenor2=0;}            
            else if (this.edad2 <= this.promoSelected.menorEdad1) 
                {
                    this.preciomenor2 = (this.promoSelected.menorPrecio1 * this.dias);
                }
            else if(this.edad2 > this.promoSelected.menorEdad1 && this.edad2 <= this.promoSelected.menorEdad2)
            {
                this.preciomenor2 = (this.promoSelected.menorPrecio2 * this.dias);
            }
            else{
                this.preciomenor2 = (this.promoSelected.menorPrecio3 * this.dias);
                }
            this.sacarTotal();
        },

        tercerAdultoFuncion:function(){
            if (this.tercerAdulto == true) {
                this.tercerAdultoPrecio = (this.promoSelected.precioTercer * this.dias);
                this.contacto.menores = 0;
                this.edad2 = 0;
                this.edad1 = 0;
                this.calculaPrecioMenor2();
                this.calculaPrecioMenor();
                
            }
            else{
                this.tercerAdultoPrecio = 0
                this.sacarTotal();
            }
            
        },

        sacarTotal:function(){
            this.total = 0; 
            this.total = (this.adultosTotal + this.preciomenor + this.preciomenor2 + this.tercerAdultoPrecio);
        },

        dateToString: function (fecha) {
            if(fecha == null){return;}
            var meses = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
            var f = new Date(fecha.replace(/-/g,"/")); //Fix UTC "-" to Local "/"
            return f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear();
        },

        pago500: function (){
            if (this.contacto.fechaInicio < this.promocionFechaInicio || this.contacto.fechaFin > this.promocionFechaFin) {

                this.notificationError('Error','Las fechas no estan en el rango','topCenter');
            }

            else if(this.contacto.fechaInicio < this.hoy || this.contacto.fechaInicio == this.contacto.fechaFin)
            {
                this.notificationError('Error','La fecha ya pasó','topCenter');
            }

            else
            {
                this.contacto.tipo = 'reserva';
                this.check      = false
                this.checkDatos = true
                var datos = {precio: 500};
                var resource = this.$resource(this.pagoRoute);
                resource.get(datos).then(function (response) {
                    this.pagoDatosID = response.data;
                    this.loading = false;
                });
            }
            
        },


        pago: function (total){
            if (this.contacto.fechaInicio < this.promocionFechaInicio || this.contacto.fechaFin > this.promocionFechaFin) {

                this.notificationError('Error','Las fechas no estan en el rango','topCenter');

            }
            else if(this.contacto.fechaInicio < this.hoy || this.contacto.fechaInicio == this.contacto.fechaFin)
            {
                this.notificationError('Error','La fecha ya pasó o no es valida','topCenter');
            }
            else
            {
                this.contacto.tipo = 'pago completo';
                this.check      = false
                this.checkDatos = true
                var datos   = {precio:total}
                var resource = this.$resource(this.pagoRoute);
                resource.get(datos).then(function (response) {
                    this.pagoDatosID = response.data;
                    this.loading = false;
                });
            }
            
        },

        procesarPago: function(datos){
            if (this.contacto.nombre == '' || this.contacto.correo == '' || this.contacto.telefono == '') 
            {
                this.notification('fa fa-exclamation-triangle', 'Error', 'Nombre, correo y telefono no pueden estar vacios', 'topCenter');

            }
            else{
                this.asignarDatos();

                var resource = this.$resource("{{route('saveDatos')}}");
                resource.save(datos).then(function (response) {
                    // alert(response);

                    console.log(response);
                    // this.loading = false;
                });

                var stripe = Stripe('pk_live_51HKVaKDxgbmeRGfzqaHGJjpHnTCKZ6OoCUyV0TUILGxjHLbUkgRDqhUntUrkv3jDp6yRWfJgtUL8NCeCj4oM2mxD0012s5r4sp');

                // test
                // var stripe = Stripe('pk_test_51HKVaKDxgbmeRGfzU6pMVRyAnVgKGQts3sZQIJLliR9Lav8i1G1oPM7L3ABgRPxScPKIHxECnDpUrhfwem1rBvkw0002vLT4Vy');
                // test

                stripe.redirectToCheckout({
                    // Make the id field from the Checkout Session creation API response
                    // available to this file, so you can provide it as argument here
                    // instead of the checkout session id placeholder.
                   sessionId: this.pagoDatosID
                }).then(function (result) {
                    // If `redirectToCheckout` fails due to a browser or network
                    // error, display the localized error message to your customer
                    // using `result.error.message`.
                });
            }
        },
        asignarDatos: function(){
            this.contacto.edad1 = this.edad1;
            this.contacto.edad2 = this.edad2;
            this.contacto.tercerAdulto = this.tercerAdulto;
            this.contacto.precio = this.total;
        }
    }
});

</script>

<script>
    $(document).ready(function() {
      var heightSlider = $('.navbar').outerHeight();
      var pixels = heightSlider;
      $('.principal').css({ marginTop : pixels  + 'px' });
  });

    $('.carousel').carousel({
      interval: 2000
    });

    $('.click_to_scroll').click(function(){
    var clickedId = $(this).attr('href');
    $('html, body').animate({ scrollTop: ($(clickedId).offset().top)} , 1000);
    return false;
    });


</script>
<script src="https://js.stripe.com/v3/">

</script>
@endpush
