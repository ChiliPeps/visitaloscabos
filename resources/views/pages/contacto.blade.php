@extends('master')

@section('content')
    <div id="contentapp" v-cloak>
        @include('content.contacto.contacto')
    </div>
@stop

@push('css')
    <style>
        .containSpiner{
            position: fixed;
            top: 0;
            left: 0;
            z-index: 9999;
            width: 100vw;
            height: 100vh;
            background: rgba(0, 0, 0, 0.7);
            transition: opacity 0.2s;
        }
        
        .spinner{
            color: white;
            position: fixed;
            top: 40%;
        }
    
    </style>
@endpush
{{-- Vue Script --}}
@push('scripts')
{{-- Components --}}
@include('components.vue.vueNotifications')
<script src="https://www.mercadopago.com.mx/integrations/v1/web-payment-checkout.js"data-preference-id="78234606-4922061c-e331-410d-99bf-6697483526fc">
    
</script>

<script>
    Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#csrf-token').getAttribute('content');
	var contentapp = new Vue({
    mounted: function () {},

    el: '#contentapp',

    mixins: [notifications],

    data: {
        
        contacto:{nombre:'', telefono:'', correo:'', comentario:''},
        correoEnviado:false,
        boton:true,
        loading:false,
        saveInAction: false,
        errores:null,
        public_url:"{{ URL::to('/') }}/",
    },
    computed: {
        
    },
    methods: {   

         enviar: function () {
            if(this.saveInAction == true) { return; }
            this.saveInAction = true;
            this.loading = true;
            this.errores = null;

            var form = new FormData();
            form.append('nombre', this.contacto.nombre);
            form.append('correo', this.contacto.correo);
            form.append('telefono', this.contacto.telefono);
            form.append('comentario', this.contacto.comentario);

            var resource = this.$resource("{{route('contacto.send')}}");
            resource.save(form).then(function (response) {
                this.loading = false;
                this.limpiar();
                this.saveInAction = false;
                this.correoEnviado = true;
                this.boton = false;
                this.notification('fa fa-check-circle', 'OK!', "Correo enviado correctamente.", 'topCenter');
            }, function (response) {
                this.loading = false;
                this.saveInAction = false;

                // Checar si hay configuración
                if (response.data.nodata) {
                    alert(response.data.error);
                } else {
                    this.errores = response.data.errors;
                }
            });
        },

        onlyNumber ($event) {
           //console.log($event.keyCode); //keyCodes value
           let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
           if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
              $event.preventDefault();
           }
        },

        limpiar: function(){
            this.contacto = {nombre:'', telefono:'', correo:'', comentario:''}
        }
    }
});

</script>
@endpush