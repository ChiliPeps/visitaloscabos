@extends('master')

@php
    $url = url("/promociones/".$promo->slug_url);
    $imagen = url('/')."/".$promo->imagen;
    $title = $promo->titulo;

@endphp

@section('title')
    {{$title}} - 
@stop

@section('metas')
    @include('content.promociones.metas')
@stop

@section('content')
    <div id="contentapp" v-cloak>
        @include('content.promociones.promocion')
    </div>
@stop


@push('css')

@endpush

@push('scripts')

@include('components.vue.vueHelperFunctions')
@include('components.vue.vueDateRangePicker')
@include('components.vue.vueDatepicker')
@include('components.vue.vueNotifications')

<script>
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#csrf-token').getAttribute('content');
var contentapp = new Vue({
    mounted: function () {},
    el: '#contentapp',

    mixins: [notifications],

    data: {
        public_url: "{{ URL::to('/') }}/",
        fecha_now: '',
        noticia_url: "{{$url}}",
        imagen:"{{$imagen}}",
        noticia_titulo: "{{$title}}",
    },

    watch:{
       
    },
    computed: {

    },
    methods: {
        shareWhatsApp:function(){
            window.open("https://wa.me/?text="+this.noticia_url);
        },

        shareFacebook:function(){
            window.open("https://facebook.com/sharer.php?display=popup&u=" + this.noticia_url, "facebook_share", 
            "height=320, width=640, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no")
        },

        shareTwitter: function(){
             window.open("https://twitter.com/share?text="+this.noticia_titulo+"&url="+this.noticia_url, "twitter_share",
            "height=320, width=640, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no")
        },
   
    }
});
</script>
@endpush
