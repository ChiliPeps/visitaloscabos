@extends('master')

@section('content')
    <div id="contentapp" v-cloak>
        @include('content.promociones.success')
    </div>
@stop

@push('scripts')
<script>
  var contentapp = new Vue({
    mounted: function () {},

    el: '#contentapp',
    data: {        

    },
    computed: {

    },
    methods: {   

    }
});
</script>
<script>
  $(document).ready(function() {
      var heightSlider = $('.navbar').outerHeight();
      var pixels = heightSlider + 50;
      $('.principal').css({ marginTop : pixels  + 'px' });
  });
</script>
@endpush