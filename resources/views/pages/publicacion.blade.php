@extends('master')
@php
    $url = url("/blog/".$publicacion->id);
    $imagen = url('/')."/".$publicacion->imagen;

@endphp

@section('title')
    {{$publicacion->titulo}}
@stop

@section('metas')
    @include('content.blog.metas')
@stop

@section('content')
    <div id="contentapp" v-cloak>
        @include('content.blog.entrada')
    </div>
@stop

@push('scripts')
<script>
  var contentapp = new Vue({
    mounted: function () {},

    el: '#contentapp',
    data: {
        
        public_url: "{{ URL::to('/') }}/",
        fecha_now: '',
        noticia_url: "{{$url}}",
        imagen:"{{$imagen}}",
        noticia_titulo: "{{$publicacion->titulo}}",

    },
    computed: {

    },
    methods: {   

        shareFacebook: function () {
            window.open("https://facebook.com/sharer.php?display=popup&u=" + this.noticia_url, "facebook_share", 
            "height=320, width=640, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no")
        },

        shareTwitter: function () {
            window.open("https://twitter.com/share?text="+this.noticia_titulo+"&url="+this.noticia_url, "twitter_share",
            "height=320, width=640, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no")
        },

        shareInstagram: function () {
            window.open("https://instagram.com/share?text="+this.noticia_titulo+"&url="+this.noticia_url, "twitter_share",
            "height=320, width=640, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no")
        },

        shareWhatsApp: function () {
            window.open("https://wa.me/?text="+this.noticia_url);
        },

        shareEmail: function () {
            location.href = "mailto:?subject="+this.noticia_titulo+"&body="+this.noticia_url;
        },

        urlGenerator: function (publicacion) {
           return this.public_url + 'publicacion/' + publicacion.id;
        },

    }
});
</script>
<script>
  $(document).ready(function() {
      var heightSlider = $('.navbar').outerHeight();
      var pixels = heightSlider + 50;
      $('.principal').css({ marginTop : pixels  + 'px' });
  });
</script>


@endpush


@push('css')
<style>
.principal {
	position: relative !important;
	text-align: center !important;
	color: white;
}

/* Bottom left text */
.bottom-left {
  position: absolute;
  bottom: 220px;
  left: 16px;
  font-size:60px;
}

	/* Centered text */
.centered {
 	position: absolute;
  	top: 50%;
  	left: 50%;
  	transform: translate(-50%, -50%);
}
.black{
	color:black;
}

a:link {
  text-decoration: none;
  color:black;
}

.imagenesTxt img{
    display: block;
    max-width: 100%;
    height: auto;
}

</style>
@endpush