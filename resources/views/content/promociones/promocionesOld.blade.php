{{-- <div class="simple-slider principal">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide" style="background-image: url('img/1.jpeg');"></div>
            <div class="swiper-slide" style="background-image: url('img/2.jpeg');"></div>
            <div class="swiper-slide" style="background-image: url('img/3.jpeg');"></div>
        </div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
</div> --}}  

<div id="carouselExampleIndicators" class="carousel slide principal" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="img/1.jpeg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="img/2.jpeg" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="img/3.jpeg" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<div class="container " v-show="panelPromocion">
  <h1 class="residentes">Promociones para Residentes</h1>

  <div class="social-icons">
    <a href="#"><i class="icon ion-social-whatsapp"></i></a>
    <a href="#"><i class="icon ion-social-facebook"></i></a>
    <a href="#"><i class="icon ion-social-twitter"></i></a>
    <a href="#"><i class="icon ion-social-instagram"></i></a>
  </div>

  <div class="card-group" style="padding-bottom: 50px;">        

    <div class="card" v-for="p in promociones" style="padding-right: 10px;padding-left: 10px;border: 0;">

    {{-- {!! Html::image($promocion->imagen, $promocion->titulo, ['class' => 'img-fluid']) !!} --}}
    <img :src="p.imagen" :alt="p.titulo" class="img-fluid">
    <div class="card-body text-center border rounded-0">
      <button class="btn btn-primary btn-sm bounce animated" type="button" style="background-color: rgb(0,221,251);filter: sepia(0%);margin-top: 15px;" 
      @click="goto_route(p.id)"  >
       {{-- <a href="{{route('inicio')}}" class="nav-link js-scroll-trigger">Inicio</a> --}}
      Reservar Aquí
      </button>
      <br><br>
      <div style="padding-bottom: 15px;">
        <i class="fab fa-whatsapp" style="padding-right: 5px;padding-left: 5px;"></i>
        <i class="fa fa-facebook-f" style="padding-right: 5px;padding-left: 5px;"></i>
        <i class="fab fa-twitter" style="padding-right: 5px;padding-left: 5px;"></i>
        <i class="fab fa-instagram" style="padding-right: 5px;padding-left: 5px;"></i>
      </div>

      <h4 class="text-center card-title"> $ @{{p.precio}}</h4>

      <ul class="list-group" style="border: 0 !imnportant;">
        <div class="video-responsive">
          <div v-html = "p.contenido"></div>
        </div>
        
      </ul>

    <a  class="click_to_scroll">
      <button class="btn btn-primary btn-sm bounce animated" type="button" style="background-color: rgb(0,221,251);filter: sepia(0%);margin-top: 15px;" 
      @click="goto_route(p.id)"  >
       {{-- <a href="{{route('inicio')}}" class="nav-link js-scroll-trigger">Inicio</a> --}}
      Reservar Aquí
      </button>
    </a>
    </div>

    </div>

  </div>   

</div>

<div v-show="panelReservar">
<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="text-uppercase section-heading">Reservación</h2>
                <h3 class="section-subheading text-muted"></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                {{-- <form id="contactForm" name="contactForm" novalidate="novalidate"> --}}
              <div class="form-row">
                <div class="col col-md-6">
                  <div class="form-group">
                    <input type="text" required class="form-control" id="hotel" placeholder="Hotel"  :value="contacto.tituloHotel" readonly/>
                    <small class="form-text text-danger flex-grow-1 help-block lead"> 
                    </small>
                  </div>
                    
                  <div class="form-group">
                    <label style="color: white;">Selecciona la fecha entre el</label>
                    <br>
                    <label style="color: white;">
                    @{{dateToString(promocionFechaInicio)}} al @{{dateToString(promocionFechaFin)}}</label>
                    <daterangepicker v-model="fechas" v-model="contacto.fechas"> 
                    </daterangepicker>
                    {{-- <button href="" @click="calcularPrecio"> click me</button> --}}
                  </div>

                  <div v-show= "fechas != '' "> 

                  <div class="form-group">
                    <input type="checkbox" id="checkbox" v-model="tercerAdulto" @change="tercerAdultoFuncion">
                    <label for="checkbox" class="colorblanco">Tercer Adulto</label>
                  </div>

                    <div class="form-group">
                      <input type="text" required class="form-control" id="name" placeholder="Nombre encargado *" v-model="contacto.nombre" />
                      <small class="form-text text-danger flex-grow-1 help-block lead"></small>
                    </div>

                    <div class="form-group">
                      <label style="color: white;">Menores</label>
                      <select class="form-control" id="cars" v-model="contacto.menores" @change="cuantosMenores">
                        <option selected :value="0" > 0</option>
                        <option :value="1">1</option>
                        <option :value="2" v-show="tercerAdulto==false">2</option>
                      </select>
                    </div>

                    <div class="form-group" v-if="contacto.menores > 0">
                      <div class="row">

                        <div class="col-md-6"> 
                          <label style="color: white;float: right;">Edad menor 1: </label>
                        </div>

                        <div class="col-md-6">
                          <select class="edades" v-model="edad1"{{-- "$data['edad' + k]" --}} @change="calculaPrecioMenor">
                            <option :value="n" v-for="n in 17" >@{{n}}</option>
                          </select>
                        </div>

                      </div>

                      <div class="row" v-show="contacto.menores > 1">

                        <div class="col-md-6"> 
                          <label style="color: white;float: right;">Edad menor 2: </label>
                        </div>

                        <div class="col-md-6">
                          <select class="edades" v-model="edad2" @change="calculaPrecioMenor2">
                            <option :value="n" v-for="n in 17" >@{{n}}</option>
                          </select>
                        </div>

                      </div>


                    </div>

                    <div class="form-group">
                      <input type="email" required class="form-control" id="email" placeholder="Email *" v-model="contacto.correo"/>
                      <small class="form-text text-danger help-block lead"></small>
                    </div>

                    <div class="form-group">
                      <input @keypress="onlyNumber" type="tel" required class="form-control" placeholder="Telefono *" v-model="contacto.telefono"/>
                      <small class="form-text text-danger help-block lead"></small>
                    </div>
                    </div>
                </div>

                <div class="col-md-6" v-show= "fechas != '' ">
                  <div class="form-group">  
                    <textarea required class="form-control" v-model="contacto.comentario" placeholder="Escribenos *">                          
                    </textarea>
                    <br><br>

                    <div class="col-xs-6 colorblanco">
                      <p class="lead text-center">RESUMEN</p>

                      <div class="table-responsive">
                        <table class="table">
                          <tbody><tr>
                            <th style="width:50%">2 Adultos</th>
                            <td>$ @{{adultosTotal}}</td>
                          </tr>
                          <tr v-if = "tercerAdulto">
                            <th>Tercer Adulto</th>
                            <td>$ @{{tercerAdultoPrecio}}</td>
                          </tr>
                          <tr v-if ="edad1 != 0">
                            <th>Menor 1</th>
                            <td>$ @{{preciomenor}}</td>
                          </tr>
                          <tr v-if ="edad2 != 0">
                            <th>Menor 2:</th>
                            <td>$ @{{preciomenor2}}</td>
                          </tr>
                          <tr>
                            <th>Total:</th>
                            <td>$ @{{total}}</td>
                          </tr>
                        </tbody></table>
                      </div>
                    </div>

                  </div>

                  </div>
                  <div class="col">
                      <div class="clearfix"></div>
                  </div>

                  <div class="col-lg-12 text-center" v-show= "fechas != '' ">
                    <a class="btn btn-info btn-xl text-uppercase" v-show="boton" @click="enviar()">
                      MercadoPago
                    </a>
                    <br>
                    <br>                  
                 {{--    <button class="btn btn-success btn-xl text-uppercase" v-show="boton" @click="enviar()">
                      Pagar
                    </button> --}}
                  </div>
                </div>
                {{-- </form> --}}
            </div>
        </div>
    </div>
</section>

</div>