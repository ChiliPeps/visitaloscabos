@php
    $url = url("/promocion/".$promo->id);
@endphp

<section id="contact" v-show="check">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="text-uppercase section-heading">Reservación</h2>
        <h3 class="section-subheading text-muted"></h3>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <div class="form-group">
          <input type="text" required class="form-control" id="hotel" placeholder="Hotel"  :value="contacto.tituloHotel" readonly 
          style="text-align: center;"/>
          <small class="form-text text-danger flex-grow-1 help-block lead"> 
          </small>
        </div>

        <div class="form-group">
          <label style="color: white;">Selecciona la fecha entre el</label>
          <br>
          <label style="color: white;">
          @{{dateToString(promocionFechaInicio)}} al @{{dateToString(promocionFechaFin)}}</label>

          

         {{--  <daterangepicker v-model="fechas" style="text-align: center;"> 
          </daterangepicker> --}}

          <div class="row">
            <div class="col-md-6">
              <h3 style="color:white;">Fecha de Entrada</h3>
              <datepicker v-model=contacto.fechaInicio @change="limpiarfechaFin"></datepicker>
            </div>
            <div class="col-md-6">
              <h3 style="color:white;">Fecha de Salida</h3>
              <datepicker v-model=contacto.fechaFin @change="calculo"></datepicker>
            </div>

          </div>
        </div>
        
      </div>
    </div>

    {{-- <div class="row" v-show= "fechas != '' "> --}}
      <div class="row" v-show= "fechas">
      <div class="col-lg-12">
        <h3 style="color: white;">Complete la información de acompañantes para los 2 adultos</h3>
        <br><br>

        <div class="form-row">
          <div class="col col-md-6">              
                  <br><br>
            <div class="form-group">
              <input type="checkbox" id="checkbox" v-model="tercerAdulto" @change="tercerAdultoFuncion">
              <label for="checkbox" class="colorblanco">Tercer Adulto</label>
            </div>

            <div class="form-group">
              <label style="color: white;">Menores</label>
              <select class="form-control" id="cars" v-model="contacto.menores" @change="cuantosMenores">
                <option selected :value="0" > 0</option>
                <option :value="1">1</option>
                <option :value="2" v-show="tercerAdulto==false">2</option>
              </select>
            </div>

            <div class="form-group" v-if="contacto.menores > 0">

              <div class="row">

                <div class="col-md-6"> 
                  <label style="color: white;float: right;">Edad menor 1: </label>
                </div>

                <div class="col-md-6">
                  <select class="edades" v-model="edad1"{{-- "$data['edad' + k]" --}} @change="calculaPrecioMenor">
                    <option :value="n" v-for="n in 17" >@{{n}}</option>
                  </select>
                </div>

              </div>

              <div class="row" v-show="contacto.menores > 1">

                <div class="col-md-6"> 
                  <label style="color: white;float: right;">Edad menor 2: </label>
                </div>

                <div class="col-md-6">
                  <select class="edades" v-model="edad2" @change="calculaPrecioMenor2" v-model="contacto.edad2">
                    <option :value="n" v-for="n in 17" >@{{n}}</option>
                  </select>
                </div>

              </div>                      
            </div>                   
          </div>

          <div class="col-md-6">
            <div class="form-group">            
              <div class="col-xs-6 colorblanco">
                <p class="lead text-center">RESUMEN</p>

                <div class="table-responsive">
                  <table class="table">
                    <tbody>
                      <tr>
                        <th style="width:50%">Habitaciones</th>
                        <td>1</td>
                      </tr>
                      <tr>
                        <th style="width:50%">Noches</th>
                        <td>@{{dias}}</td>
                      </tr>
                      <tr>
                        <th style="width:50%">2 Adultos</th>
                        <td>$ @{{adultosTotal}}</td>
                      </tr>
                    <tr v-if = "tercerAdulto">
                      <th>Tercer Adulto</th>
                      <td>$ @{{tercerAdultoPrecio}}</td>
                    </tr>
                    <tr v-if ="edad1 != 0">
                      <th>Menor 1</th>
                      <td>$ @{{preciomenor}}</td>
                    </tr>
                    <tr v-if ="edad2 != 0">
                      <th>Menor 2:</th>
                      <td>$ @{{preciomenor2}}</td>
                    </tr>
                    <tr>
                      <th>Total:</th>
                      <td>$ @{{total}}</td>
                    </tr>
                  </tbody></table>
                </div>
              </div>

              <div class="form-group">
                <input type="text" required class="form-control"  placeholder="Hotel"  
                :value="'$'+ total" readonly/>
                <small class="form-text text-danger flex-grow-1 help-block lead"> 
                </small>
              </div>
            </div>
            
          </div>            
        </div>
      </div>

      <div class="col-lg-12 text-center">
        <button class="btn btn-info btn-xl text-uppercase" v-show="boton" @click="pago500()">
          Paga anticipo 500.00 aquí
        </button>
        <br>
        <br>                  
        <button class="btn btn-success btn-xl text-uppercase" v-show="boton" @click="pago(total)">
          Pago total aquí
        </button>
      </div>
      
    </div>
  </div>

</section>

<section id="contact" v-show="checkDatos">
  <div class="container">
    <div class="col-xs-6 colorblanco">
      <p class="lead text-center">RESUMEN</p>

      <div class="table-responsive">
        <table class="table">
          <tbody>
            <tr>
              <th  style="width:50%"> Fecha de entrada</th>
              <td style="text-align: right;"> @{{contacto.fechaInicio}}</td>
            </tr>

            <tr>
              <th  style="width:50%"> Fecha de Salida</th>
              <td style="text-align: right;"> @{{contacto.fechaFin}}</td>
            </tr>

            <tr>
              <th style="width:50%">2 Adultos</th>
              <td style="text-align: right;">$ @{{adultosTotal}}</td>
            </tr>

            <tr v-if = "tercerAdulto">
              <th>Tercer Adulto</th>
              <td style="text-align: right;">$ @{{tercerAdultoPrecio}}</td>
            </tr>

            <tr v-if ="edad1 != 0">
              <th>Menor 1</th>
              <td style="text-align: right;">$ @{{preciomenor}}</td>
            </tr>

            <tr v-if ="edad2 != 0">
              <th>Menor 2:</th>
              <td style="text-align: right;">$ @{{preciomenor2}}</td>
            </tr>

            <tr>
              <th>Total a pagar:</th>
              <td style="text-align: right;">$ @{{total}}</td>
            </tr>

        </tbody></table>
      </div>
    </div>

    <br>
    <p class="lead text-center colorblanco">DATOS DE CONTACTO</p>
    <br>

    <div class="form-group">
      <input type="text" required class="form-control" id="name" placeholder="Nombre *" v-model="contacto.nombre" />
      <small class="form-text text-danger flex-grow-1 help-block lead"></small>
    </div>

    <div class="form-group">
      <input type="email" required class="form-control" id="email" placeholder="Email *" v-model="contacto.correo"/>
      <small class="form-text text-danger help-block lead"></small>
    </div>

    <div class="form-group">
      <input @keypress="onlyNumber" type="tel" required class="form-control formError" placeholder="Telefono *" v-model="contacto.telefono"/>
      <small class="form-text text-danger help-block lead"></small>
    </div>

    <div class="text-center">
      <button class="btn btn-success btn-xl text-uppercase " v-show="boton" @click="procesarPago(contacto)">
        Confirmar pago
      </button>
    </div>

  </div>
</section>

