{{-- 
<div id="carouselExampleIndicators" class="carousel slide principal" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="img/1.jpeg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="img/2.jpeg" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="img/3.jpeg" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div> --}}

<div class="container principal" v-show="panelPromocion">
  <h1 class="residentes">Promociones para Residentes</h1>

  {{-- <div class="social-icons">
    <a href="#"><i class="icon ion-social-whatsapp"></i></a>
    <a href="#"><i class="icon ion-social-facebook"></i></a>
    <a href="#"><i class="icon ion-social-twitter"></i></a>
    <a href="#"><i class="icon ion-social-instagram"></i></a>
  </div> --}}

  <div class="card-group" style="padding-bottom: 50px;">        

    <div class="card" v-for="d in promociones" style="padding-right: 10px;padding-left: 10px;border: 0;">
      <div class="col-md-3 item">
                        
                        
      </div>
    </div>
  </div>

  <div class="row articles">               
                
     @foreach ($promociones as $d)
    <div class="col-md item text-center">   

      <a href="{{route('promocionNombre', $d->slug_url)}}">
          {!! Html::image($d->imagen, $d->titulo, ['class' => 'img-fluid']) !!}
      </a>

      <a href="{{route('promocionNombre', $d->slug_url)}}" style="color:black;">
        <h3 class="name" style="text-align: center;" >{{$d->titulo}}</h3>
      </a>

      <p class="description">{!!$d->descripcion!!}</p>

      <a href="{{ URL::route('promocionNombre', $d->slug_url) }}"  class="btn btn-primary btn-xl bounce animated" type="button" style="background-color: rgb(0,221,251);filter: sepia(0%);margin-top: 15px;"> Mas Información </a>    
    </div>

    

    @endforeach
  </div>

</div>