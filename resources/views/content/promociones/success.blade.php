<div class="photo-gallery"></div>

<section id="filtr-gallery" style="padding-top: 250px;">
    <div class="container">
        <h1 class="text-center">Pago correcto</h1>
        <p>
        	 te avisaremos de tu confirmación en un máximo de 24 hrs
        </p>

        <a  href="{{route('inicio')}}" class="btn btn-success btn-xl text-uppercase">
          Deacuerdo
        </a>
    </div>
</section>
