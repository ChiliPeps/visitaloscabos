<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="text-uppercase section-heading">Contactanos</h2>
                <h3 class="section-subheading text-muted"></h3>
            </div>
        </div>

        <!-- VueLoading icon -->
        <div class="text-center containSpiner" v-show="loading">
            <i  class="fa fa-spinner fa-spin fa-5x spinner"></i>
        </div>

        <div class="row">
            <div class="col-lg-12">
                {{-- <form id="contactForm" name="contactForm" novalidate="novalidate"> --}}
                    <div class="form-row">
                        <div class="col col-md-6">

                            <div class="form-group">
                                <input type="text" required class="form-control" id="name" placeholder="Nombre *" v-model="contacto.nombre"/>
                                <small class="form-text text-danger flex-grow-1 help-block lead"></small>
                            </div>

                            <div class="form-group">
                                <input type="email" required class="form-control" id="email" placeholder="Email *" v-model="contacto.correo"/>
                                <small class="form-text text-danger help-block lead"></small>
                            </div>

                            <div class="form-group">
                                <input type="tel" required class="form-control" placeholder="Telefono *" v-model="contacto.telefono"/>
                                <small class="form-text text-danger help-block lead"></small>
                            </div>
                        </div>

                        <div class="col-md-6">

                            <div class="form-group">
                                <textarea required class="form-control" id="message" placeholder="Escribenos *" v-model="contacto.comentario">                       
                                </textarea>
                                <small class="form-text text-danger help-block lead"></small>
                            </div>

                        </div>

                        <div class="col">
                            <div class="clearfix"></div>
                        </div>

                        <div class="col-lg-12 text-center">
                            <div id="success"></div>
                            <button v-show="boton" class="btn btn-info btn-xl text-uppercase"  @click="enviar()">
                                Enviar
                            </button>
                            
                            {{-- <a href="https://www.mercadopago.com.mx/checkout/v1/redirect?pref_id=78234606-4922061c-e331-410d-99bf-6697483526fc">
                            <button v-show="boton" class="btn btn-info btn-xl text-uppercase"  >
                                MercadoPago
                            </button>
                            </a> --}}

                            
                        </div>
                    </div>
                {{-- </form> --}}
            </div>
        </div>
    </div>
</section>