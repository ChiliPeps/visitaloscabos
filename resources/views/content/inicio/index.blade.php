    <div class="jumbotron hero-photography"style="margin-top: 0px; !important">
        <iframe src="https://www.portalagenteslomas.com.mx/28941/booking" width="80%" height="450px" seamless="seamless" frameborder="0" style="margin-top: 10rem;"></iframe>
    </div>

    <div class="article-list">
        <div class="container" style="max-width: 1240px !important;">
            <div class="intro">
                <h2 class="text-center">Destinos más visitados</h2>
                <p class="text-center">Vistia los mejores destinos de mexico</p>
            </div>
            <div class="row articles">               
                
                @foreach ($destinos as $d)
                    <div class="col-md-3 item">
                        
                        <a href="{{route('destinos', $d->id)}}">
                            {!! Html::image($d->imagen, $d->titulo, ['class' => 'img-fluid']) !!}
                        </a>
                        <h3 class="name">{{$d->nombre}}</h3>

                        <p class="description">{{$d->descripcion}}</p>
                    </div>
                @endforeach
        </div>
    </div>


    <div>
        <div id="box-2">
            
            <h1 style="font-family: Amiri, serif;color: rgb(255,255,255);">Accede a  nuestras promociones
                <br>
                <a href="{{route('promociones')}}">
                    <button class="btn btn-info" style="height: 50px;" type="button">Promociones</button>
                </a>
                
            </h1>

        </div>
    </div>

    <div class="article-list">
        <div class="container">
            <div class="intro">
                <h2 class="text-center" style="color: rgb(20,133,238);">Blog</h2>
                <p class="text-center"> </p>
            </div>

            <div class="row articles">
                
                    @foreach($publicaciones as $p)
                    <div class="col-sm-6 col-md-4 item">
                        <a href="{{route('publicacion', $p->id)}}">
                            {{-- <img class="img-fluid" src="img/ChiquiClub.jpg"> --}}
                            {!! Html::image($p->imagen, $p->titulo, ['class' => 'img-fluid']) !!}
                            <h3 class="name">{{$p->titulo}}</h3>
                            <p class="description">{{$p->descripcion}}</p>
                        </a>
                    </div>
                    @endforeach

                  
                </div>
            </div>
        </div>
    </div>