<div class="jumbotron hero-photography" style="background-image:url({{url($destino->imagen)}});" title="Portada" alt="portada">
    <h1 class="hero-title">{{$destino->nombre}}</h1>
</div>

{{-- <div class="portada">
    {!! Html::image($destino->imagen, 'portada', ['class' => 'img-fluid']) !!}
</div> --}}

 <div class="social-icons">

    <a href="" @click = "shareWhatsApp"><i class="icon ion-social-whatsapp"></i></a>
    <a href="" @click = "shareFacebook"><i class="icon ion-social-facebook"></i></a>
    <a href="" @click = "shareTwitter"><i class="icon ion-social-twitter"></i></a>
    {{-- <a href="" @click= "shareInstagram"><i class="icon ion-social-instagram"></i></a> --}}
</div>
<hr>

<div class="container">
    <div class="intro">
        <h2 class="text-center">{{$destino->nombre}}</h2>
        <p class="text-center"> </p>
    </div>

    <div class="text-center">
        <iframe width="100%" height="450" src="{{PageHelpers::youtube_url_to_embed($destino->youtubeUrl)}}" 
        frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>

    <div class="row mt-5">
        <div class="col-md-8" style="background-color: white;">
            <h2 class="text-center">Descripcion</h2>
            <div>
                {!!$destino->contenido!!}
              
            </div>
        </div>

        <div class="col-md-4" style="background-color:#f1f1f1;">
            <br>

            <h3 class="text-center"> Detalles </h3>
            <hr>      

            <p class="text-center">{{$destino->nombre}}</p>
            
            <h5>Tipo</h5>
            <p class="text-right">{{$destino->tipo}}</p>
            
            <h5>Duracion</h5>
            <p class="text-right">{{$destino->duracion}}</p>

            <h5>Precio</h5>
            <p class="text-right">$ {{$destino->precio}}</p>

            <h5>Incluye:</h5>
            <p class="text-right">{{$destino->incluye}}</p>

            <h5>Vigencia</h5>
            <p class="text-right">del {{$destino->fecha_inicio}} a {{$destino->fecha_fin}} </p>

            <h5 class="text-center">Observaciones</h5>
                <div>
                    <p class="text-center"> {!!$destino->observaciones!!}</p>
                </div>
        </div>    
    </div>
    
</div>

<section id="filtr-gallery" style="padding-top: 50px;">
    <div class="container">
        <h1 class="text-center">Galeria de imagenes</h1>

        <div class="filtr-controls">
            <span class="active" data-filter="1">Galeria</span>
        </div>

        <div class="row filtr-container">


            @foreach($images as $img)
                <div class="col-6 col-sm-4 col-md-3 filtr-item" data-category="1">
                    <a href="{{url($img->ruta)}}">
                    {!! Html::image($img->ruta, $img->nombre, ['class' => 'img-fluid']) !!}
                    </a>
                </div>
            @endforeach          

        </div>
    </div>
</section>