<!-- Page Content -->
<div class="container principal">

  {{-- <img class="img-fluid" src="http://placehold.it/1920x1280" alt=""> --}}
  {!! Html::image($publicacion->imagen, $publicacion->titulo, ['class' => 'img-fluid']) !!}
  <div class="row black">
    <div class="col-md-6 text-left"> Por: <strong>{{$publicacion->autor_foto}}</strong></div>
    <div class="col-md-6 text-right">Miercoles 17 de Junio, 2020</div>
  </div>
  
  <div class="col-lg-12 mt-30 ">
    <h1> {{$publicacion->titulo}}</h1>
  </div>   

</div>

  <div class="container">

    <div class="row">

      <!-- Post Content Column -->
      <div class="col-lg-8">
        <hr>

        <div class="social-icons">
          <a href="" @click = "shareWhatsApp"><i class="icon ion-social-whatsapp"></i></a>
          <a href="" @click = "shareFacebook"><i class="icon ion-social-facebook"></i></a>
          <a href="" @click = "shareTwitter"><i class="icon ion-social-twitter"></i></a>
        </div>
        
        <div class="text-justify imagenesTxt">
        {!!$publicacion->contenido!!}  
        </div>
        
        
        <div class="social-icons">
          <a href="" @click = "shareWhatsApp"><i class="icon ion-social-whatsapp"></i></a>
          <a href="" @click = "shareFacebook"><i class="icon ion-social-facebook"></i></a>
          <a href="" @click = "shareTwitter"><i class="icon ion-social-twitter"></i></a>
        </div>

        <div class="fb-comments" data-href="{{$url}}" data-numposts="100" data-width="100%"></div>

      </div>

      <!-- Sidebar Widgets Column -->
      <div class="col-md-4">

        <!-- Side Widget -->
        
          <h5 class="card-header">Ultimas entradas</h5>
          @foreach($nuevas as $n)
          <div class="card my-4">
            <div class="card-body">
              <a href="{{route('publicacion', $n->id)}}">
                {!! Html::image($n->imagen, $n->titulo, ['class' => 'card-img-top']) !!}
              </a>
                <div class="card-body">
                  <a href="{{route('publicacion', $n->id)}}">
                    <h5 class="card-title">{{$n->titulo}}</h5>
                  </a> 
                  <p class="card-text">{{$n->descripcion}}</p>
                  {{-- <a href="{{route('publicacion', $n->id)}}"  class="btn btn-primary">Leer</a> --}}
                </div>
                <div class="card-footer text-muted">
                  {{PageHelpers::dateStringWithDay($n->created_at)}}
                  <a href="#"></a>
                </div>
            </div>
          </div>
          @endforeach            

      </div>

    </div>
    <!-- /.row -->

  </div>