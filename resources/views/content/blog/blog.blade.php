
<div class="jumbotron hero-photography">
  <h1 class="hero-title">Vacaciones Reales</h1>
  <p class="hero-subtitle">Ofrece precios especiales a residentes de BCS.</p>
</div>

<!-- Page Content -->
  <div class="container mt-5">

    <div class="row">

      <!-- Blog Entries Column -->
      <div class="col-md-8">

       {{--  <h1 class="my-4">Page Heading
          <small>Secondary Text</small>
        </h1> --}}

        <!-- Blog Post -->
        @foreach($publicaciones as $pub)
          <div class="card mb-4">
            {!! Html::image($pub->imagen, $pub->titulo, ['class' => 'card-img-top']) !!}
            <div class="card-body">
              <h2 class="card-title">{{$pub->titulo}}</h2>
              <p class="card-text">{{$pub->descripcion}}</p>
              <a href="{{route('publicacion', $pub->id)}}"  class="btn btn-primary">Leer</a>
            </div>
            <div class="card-footer text-muted">
              {{PageHelpers::dateStringWithDay($pub->created_at)}}
              <a href="#">{{$pub->autor_foto}}</a>
            </div>
          </div>
          
        @endforeach


        <!-- Pagination -->          
        @if ($publicaciones->lastPage() > 1)
        <ul class="pagination  justify-content-center mb-4">
            <li class="page-item {{ ($publicaciones->currentPage() == 1) ? ' disabled' : '' }}">
                <a class="page-link" href="{{ $publicaciones->url(1) }}"> << </a>
            </li>
            @for ($i = 1; $i <= $publicaciones->lastPage(); $i++)
                <li class="{{ ($publicaciones->currentPage() == $i) ? ' active' : '' }}">
                    <a class="page-link" href="{{ $publicaciones->url($i) }}">{{ $i }}</a>
                </li>
            @endfor
           <li class="page-item {{ ($publicaciones->currentPage() == $publicaciones->lastPage()) ? ' disabled' : '' }}" >
                <a class="page-link" href="{{ $publicaciones->url($publicaciones->currentPage()+1) }}" > >> </a>
            </li>
        @endif

        {{-- <ul class="pagination justify-content-center mb-4">
          <li class="page-item">
            <a class="page-link" href="#"><<</a>
          </li>
          <li class="page-item disabled">
            <a class="page-link" href="#">>></a>
          </li>
        </ul> --}}

      </div>

      <!-- Sidebar Widgets Column -->
      <div class="col-md-4">

        <!-- Search Widget -->
        <div class="card my-4">
          <h5 class="card-header">Search</h5>
          <div class="card-body">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search for...">
              <span class="input-group-btn">
                <button class="btn btn-secondary" type="button">Go!</button>
              </span>
            </div>
          </div>
        </div>

        <!-- Side Widget -->
        <h5 class="text-center card-header">Las más leidas</h5>
         @foreach($masLeidas as $leidas)
        <div class="card my-4">            
          <a href="{{route('publicacion', $leidas->id)}}">
            {!! Html::image($leidas->imagen, $leidas->titulo, ['class' => 'card-img-top']) !!}
            </a>
            
              <div class="card-body">
                <h2 class="text-center card-title">{{$leidas->titulo}}</h2>
                <p class="text-center card-text">{{$leidas->descripcion}}</p>                
                
              </div>
              <div class="card-footer text-muted">
                {{PageHelpers::dateStringWithDay($leidas->created_at)}}
                {{-- <a href="#">Start Bootstrap</a> --}}
              </div>
            
              
        </div>
        @endforeach

      </div>

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->