<h2 class="display-3 text-center text-dark" style="margin: -13px;padding: -21px;margin-top: 187px; margin-bottom:70px;">Hoteles</h2><section class="showcase">
    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image:url('img/hoteles/royalSolaris.jpg');"><span></span></div>
            <div class="col-lg-6 my-auto order-lg-1 showcase-text">
                <h2>ROYAL SOLARIS </h2>
                <p class="lead mb-0">Royal Solaris Los Cabos es un magnífico resort frente al mar estilo hacienda mexicana 
                con hermosos jardines, albercas y 390 habitaciones, diseñado estratégicamente en forma 
                de herradura para brindar un fácil y rápido acceso a todas las áreas y servicios del hotel. 
                Hoteles Todo Incluido en los Cabos. 
                </p>
                <p class="lead mb-0">
                El hotel  se encuentra a tan  solo 20 minutos de distancia del Aeropuerto Internacional de 
                San José (SJD), a 5 min del Centro de San Jose del Cabo y a sólo  30 minutos de Cabo 
                San Lucas. </p>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-lg-6 text-white showcase-img" style="background-image:url('img/hoteles/riu.jpg');"><span></span></div>
            <div class="col-lg-6 my-auto order-lg-1 showcase-text">
                <h2>RIU SANTA FE </h2>
                <p class="lead mb-0">
                    El Hotel Riu Santa Fe, ubicado en el sur de la península de Baja California y al borde de 
                    la playa de El Médano, pone a tu disposición unas impresionantes instalaciones para 
                    hacer de tu estancia algo inolvidable. Este hotel en Los Cabos con Todo Incluido 24 
                    horas te ofrece conexión WiFi gratuita, una variada oferta gastronómica, entretenidos 
                    programas de animación y el servicio exclusivo característico de la marca. Y además, 
                    puedes descubrir la mejor diversión gracias a la RIU POOL PARTY, unas animadas fiestas 
                    en la piscina con música y espectáculos en directo, exclusivas para adultos. 
                    Todas las instalaciones del Hotel Riu Santa Fe han sido diseñadas para ofrecerte las 
                    mejores comodidades, como sus más de 1.000 habitaciones en las que dispones de aire 
                    acondicionado, minibar y dispensadores de bebidas, entre muchas otras facilidades. Y si 
                    lo que te apetece es disfrutar del cálido clima y refrescarte durante tus vacaciones, tienes 
                    a tu disposición las 7 piscinas del hotel, algunas de ellas con swim-up bar y zona de 
                    hidromasaje.
                </p>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image:url('img/hoteles/hardRock.jpg');"><span></span></div>
            <div class="col-lg-6 my-auto order-lg-1 showcase-text">
                <h2>HARD ROCK</h2>
                <p class="lead mb-0">Ubicado en el extremo sur de la península de Baja California de México, el todo incluido 
                Hard Rock Hotel Los Cabos ofrece la mezcla perfecta de ritmo y relajación con una vista 
                increíble de fondo. Toma el sol en cualquiera de nuestras refrescantes piscinas o disfruta 
                de un sinfín de actividades y diversión desenfrenada. Así que agarra tu cámara y a los 
                tuyos porque estas vacaciones serán una de las mejores. 
                En ocasiones si es posible tenerlo todo y nosotros lo tenemos, desde el lugar perfecto 
                para estar bajo el sol hasta las cenas más exquisitas, mucha diversión, el mejor 
                entretenimiento, espectáculos en vivo, vinos y licores de la mejor calidad.  </p>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-lg-6 text-white showcase-img" style="background-image:url('img/hoteles/araiza.jpg');"><span></span></div>
            <div class="col-lg-6 my-auto order-lg-1 showcase-text">
                <h2>ARAIZA PALMIRA, LA PAZ </h2>
                <p class="lead mb-0">Araiza Palmira es un hotel en Baja California Sur situado en la espectacular bahía de La 
                Paz, a unos cuantos pasos del malecón y a tan solo unos minutos del centro de la ciudad.​
                Experimente lo mejor de La Paz en un hotel que lo tiene todo. Sus confortables 
                instalaciones y sobretodo su exquisita cocina harán que sus vacaciones sean únicas e 
                inolvidables.​
                Hotel Araiza Palmira se distingue de los demás hoteles en La Paz por su tranquilidad y 
                calidad en el servicio. 
                ​
                La Paz es un lugar que se caracteriza por tratar a sus visitantes como invitados 
                especiales y por tenerles preparados grandes experiencias que recordaran para toda su 
                vida. Playas de agua cristalina y suave oleaje, islas secretas, ballenas grises y tiburones 
                ballena presentes en fechas especiales para ser vistos en famosos tours, museos y sitios 
                llenos de historia, una gran variedad de bares y restaurantes, y un impresionante malecón 
                con vista al Mar de Cortés es un poco de las maravillas que sólo en La Paz podrá 
                encontrar. </p>
            </div>
        </div>

        <div class="row no-gutters">
            <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image:url('img/hoteles/hotelOne.jpg');"><span></span></div>
            <div class="col-lg-6 my-auto order-lg-1 showcase-text">
                <h2>HOTEL ONE, LA PAZ </h2>
                <p class="lead mb-0">
                    One La Paz se encuentra ubicado en el corazón de la ciudad, es un hotel con servicios 
                    prácticos y las comodidades ideales para quienes visitan La Paz por motivo de negocios. 
                    Esta propiedad pone a tu disposición desayunador en cortesía, lavandería de 
                    autoservicio, sala de juntas, estacionamiento e internet inalámbrico en cortesía. 
                    El One La Paz, situado a solo 10 minutos a pie de la costa, en el centro de La Paz, a 5 
                    minutos a pie del muelle, cuenta con piscina al aire libre. Este hotel moderno sirve un 
                     
                     
                    desayuno gratuito y proporciona aparcamiento gratuito y WiFi, también gratuita. 
                    Las habitaciones son prácticas y están provistas de aire acondicionado, escritorio, TV por 
                    cable y baño privado con ducha y artículos de aseo gratuitos. 
                    El One La Paz está situado a 1 km de la catedral de La Paz y de la plaza de la 
                    Constitución y varios museos. El aeropuerto internacional de La Paz se encuentra a 10 
                    minutos en coche.
                </p>
            </div>
        </div>
    </div>
</section>