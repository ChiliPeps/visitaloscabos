<!doctype html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Somos un Grupo de Personas con el gusto de atender y dar asesoría para que disfrutes tus vacaciones al Máximo, paquetes Nacionales e Internacionales">
	<meta name="keywords" content="en edicion...">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '728169327731731');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=728169327731731&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->


	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-174141818-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-174141818-1');
	</script>

	{{-- Titulo --}}
	<title> @yield('title') Visita los Cabos / Vacaciones Reales ©. </title>

	<!-- Metas -->
	@hasSection('metas')
		@yield('metas')
	@else
        <meta property="fb:app_id" content=""/>
		<meta property="og:title" content="Visita los cabos / Vacaciones Reales">
		<meta property="og:type" content="website">
		<meta property="og:url" content="https://visitaloscabos.mx"/>
		<meta property="og:description" content="Somos un Grupo de Personas con el gusto de atender y dar asesoría para que disfrutes tus vacaciones al Máximo, paquetes Nacionales e Internacionales">
		<meta property="og:image" content="{{ url('/')."/img/newLogo.png" }}"/>
	@endif

	{!! Html::favicon('favicon.ico') !!}
	@include('inc/header_common')

</head>
<body class="body">
	@include('inc/header')

	<!-- Content -->
	@yield('content')
	<!-- Content -->

	@include('inc/footer')
	@include('inc/footer_common')
</body>
</html>