{{-- jquery --}}
{!! Html::script('js/bootstrapstudio/jquery.min.js') !!}
{{-- bootstrap --}}
{!! Html::script('js/bootstrap.min.js') !!}
{{-- {!! Html::script('js/bootstrapv4.2.1.min.js') !!} --}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

{{-- BootstrapStudio --}}
{!! Html::script('js/bootstrapstudio/agency.js') !!}
{{-- {!! Html::script('js/bootstrapstudio/Filterable-Gallery-with-Lightbox.js') !!} --}}
{!! Html::script('js/bootstrapstudio/Simple-Slider.js') !!}

{{-- ajusta el jumbotron despues del navbar --}}
{{-- {!! Html::script('js/bootstrapstudio/size.js') !!} --}}

<!-- Vue.js & Vue-Resource -->
{{-- {!! Html::script('plugins/vue/vue.js') !!} --}}
{!! Html::script('plugins/vue/vue.min.js') !!}
{!! Html::script('plugins/vue-resource/vue-resource.min.js') !!}

<script>Vue.config.devtools = '{{ env('APP_DEBUG') }}';</script>

{{-- jquery --}}
{{-- {!! Html::script('js/jquery/jquery-2.1.3.min.js') !!} --}}



<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>

<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "TouristAttraction",
    "name": "Visita los cabos",
    "url": "visitaloscabos.mx",
    "address": "Isabel La Católica 1380 loc 4 Colonia centro. La Paz B.C.S., México",
    "sameAs": [
      "https://www.facebook.com/VACACIONESREALESOFICIAL/",
      "https://www.instagram.com/Vacacionesreales/"
    ]
  }
</script>

{{-- Stack Scripts --}}
@stack('scripts')