{{-- <div class="footer-dark" style="background-color: #f8b037;">--}}
<div class="footer-dark" style="background-color: #7CBBE5;">
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-3 item">
                        <h3>Contenido</h3>
                        <ul>
                            <li><a href="{{route('hoteles')}}">Hoteles</a></li>
                            <li><a href="{{route('promociones')}}">Promociones</a></li>
                            <li><a href="{{route('galeria')}}">Galeria</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-3 item">
                        <h3>Nosotros</h3>
                        <ul>
                            <li><a href="{{route('contacto')}}">Contacto</a></li>
                            <li><a href="{{route('blog')}}">Blog</a></li>
                            <li></li>
                        </ul>
                    </div>
                    <div class="col-md-6 item text">
                        <h3>Vacaciones reales</h3>
                        <p>Somos una empresa encargada de encotrarte las mejores vacaciones a un precio justo, para que tu y tu familia tengan una experiencia inolvidable.</p>
                    </div>
                    <div class="col item social">
                        <a href="#"><i class="icon ion-social-facebook"></i></a>
                        <a href="#"><i class="icon ion-social-twitter"></i></a>
                        <a href="#"><i class="icon ion-social-whatsapp"></i></a>
                        <a href="#"><i class="icon ion-social-instagram"></i></a>
                    </div>
                </div>
                <br>
                <p class="">Vacaciones Reales© 2020</p>
                <p class="">Powered By: <a href="https://www.komvac.com/"> Komvac</a></p>
            </div>
        </footer>
    </div>
    {{-- BOTONES FLOTANTES --}}
    <a href="https://wa.me/526121792418?text=Solicito cotizacion de visitaloscabos.mx " class="whatsapp" target="_blank"> <i class="fa fa-whatsapp whatsapp-icon"></i></a>

    <a href="https://m.me/VACACIONESREALESOFICIAL/" class="messenger" target="_blank"> <i class="fab fa-facebook-messenger whatsapp-icon"></i></a>