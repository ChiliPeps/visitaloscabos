 {{-- comentarios de facebook --}}
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v7.0" nonce="rckDVj95"></script>

{{-- header --}}
 <nav class="navbar navbar-expand-lg fixed-top" id="mainNav" style="background-color:rgba(124,187,229,0.8);">
    <div class="container">
        <a class="navbar-brand" href="{{route('inicio')}}">
            <div class="logo">
                <img src="{{asset('img/logo.png')}}" alt="logo">
            </div>
        </a>
        <button class="navbar-toggler navbar-toggler-right" data-toggle="collapse" data-target="#navbarResponsive" type="button" data-toogle="collapse" aria-controls="navbarResponsive"
            aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="nav navbar-nav ml-auto text-uppercase">
                <li class="nav-item" role="presentation">
                    <a href="{{route('inicio')}}" class="nav-link js-scroll-trigger">Inicio</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a href="{{route('hoteles')}}" class="nav-link">Hoteles</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a href="{{route('promociones')}}" class="nav-link">Promociones</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a href="{{route('galeria')}}" class="nav-link">Galería</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a href="{{route('blog')}}" class="nav-link">Blog</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a href="{{route('contacto')}}" class="nav-link">Cotiza Aquí</a>
                </li>
            </ul>
        </div>
    </div>
</nav>