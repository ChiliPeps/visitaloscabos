<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title" v-if="saveButton"> Crear Publicación</h3>
        <h3 class="box-title" v-if="updateButton"> Actualizar Publicación</h3>
        <div class="box-tools pull-right">
{{--             @if(Auth::guard('cms')->user()->type != "reportero")
            <a class="btn bg-orange btn-sm" @click="aprobarNoticia" v-if="noticia.aprobada == 0"><i class="fa fa-thumbs-o-up"></i> Aprobar Noticia</a>
            @endif --}}
            <a class="btn bg-navy btn-sm" @click="closePanelInputs"><i class="fa fa-chevron-left"></i> Regresar</a>
            <a class="btn btn-success btn-sm" v-show="saveButton" @click="setData()"><i class="fa fa-floppy-o"></i> 
            Guardar</a>
            <a class="btn btn-success btn-sm" v-show="updateButton" @click="updateData()"><i class="fa fa-floppy-o"></i> Actualizar</a>
        </div>
    </div>
</div>


{{-- Upload Progress Bar --}}
{{-- <vprogress :progress="uploadProgress"></vprogress> --}}

{{-- Form Errros --}}
<formerrors :errorsbag="errores"></formerrors>

<vue-tabs v-model="tabName">
    <br>
    <v-tab title="Datos de la Publicación">
        <div class="row">
            <div class="col-md-6">
        
                <div class="box box-primary">
                    <div class="box-body">
        
                        {{-- titulo --}}
                        <div class="form-group">
                            <label>Título</label>
                            <input type="text" class="form-control" 
                            placeholder="título" v-model="publicacion.titulo" 
                            :class="formError(errores, 'titulo', 'inputError')">                            
                        </div>


                       {{--  <div class="form-group">
                            <label>Epígrafe</label>
                            <textarea  placeholder="Epigrafe" rows="4" class="form-control" v-model="publicacion.epigrafe"></textarea>
                        </div>


                        <div class="form-group">
                            <label>Dedicatoria</label>
                            <input type="text" class="form-control" 
                            placeholder="dedicatoria" v-model="publicacion.dedicatoria" 
                            :class="formError(errores, 'dedicatoria', 'inputError')">
                        </div> --}}

                        {{-- tipo --}}
                        {{-- <div class="form-group">
                            <label>Tipo</label>
                            <select class="form-control" v-model="noticia.tipo" 
                            :class="formError(errores, 'tipo', 'inputError')" 
                            @change="tipoChanged">
                                <option value="normal">Normal</option>
                                <option value="video">Video</option>
                            </select>
                        </div> --}}
        
                        {{-- categoria --}}
                       {{--  <div class="form-group">
                            <label>Categoría</label>
                            <select class="form-control" v-model="noticia.id_categoria">
                                <option v-for="categoria in categorias" :value="categoria.id">@{{categoria.nombre}}</option>
                            </select>
                        </div> --}}
        
                        {{-- fecha --}}
                        {{-- <div class="form-group">
                            <label>Fecha</label>
                            <datepicker v-model="noticia.fecha" 
                            :class="formError(errores, 'fecha', 'inputError')">
                            </datepicker>
                        </div> --}}
        
                    </div>
                </div>
        
            </div>
            <div class="col-md-6">
        
                <div class="box box-danger">
                    <div class="box-body">
        
                        {{-- slugurl --}}
                        <div class="form-group">
                            <label>SlugURL</label>
                            <input type="text" class="form-control" 
                            placeholder="slugurl" v-model="slugurl" readonly>
                        </div>
        
                        {{-- Tags --}}
                        {{-- <div class="form-group">
                            <label>Tags (Presione Enter para agregar)</label> 
                            <v-select multiple label="nombre" :options="tags" v-model="noticia.tags" 
                            @search="onSearchTag" :loading="loadingTags" taggable>
                                <span slot="no-options">Escribe para buscar opciones!</span>
                                <div slot="spinner" class="text-center">
                                    <i v-show="loadingTags" class="fa fa-spinner fa-spin fa-2x"></i>
                                </div>
                            </v-select>
                        </div> --}}

                       

                        {{-- Pública --}}
                        <div class="form-group" >
                            <label>Pública 
                                <i v-if="publicacion.publish == 1" class="fa fa-eye"></i>
                                <i v-else class="fa fa-eye-slash"></i>
                            </label>
                            <select class="form-control" v-model="publicacion.publish" 
                            :class="formError(errores, 'publish', 'inputError')">
                                <option value="1" selected>Pública</option>
                                <option value="0">No Pública</option>
                            </select>
                        </div>
                        
                        {{-- Pública --}}
                        
                    </div>
                </div>
        
            </div>
        </div>
    </v-tab>

    <v-tab title="Contenido">
        <div class="box box-primary">
            <div class="box-body">
                <div class="form-group">
                    <div class="form-group">
                        <label>Descripción (@{{desCharCount}})</label>
                        <textarea rows="4" class="form-control" v-model="publicacion.descripcion"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label>Contenido</label>
                    <tinymce id="tinyMCE" imageuploadurl="{{route('admin.noticias.tinyimages')}}" 
                    v-model="publicacion.contenido"></tinymce>
                </div>
            </div>
        </div>
    </v-tab>

    <v-tab title="Imagen">
        <div class="box box-primary">
            <div class="box-header"><h2 class="box-title">Imagen de Noticia (960 x 540)</h2></div>
            <div class="box-body">

                {{-- autor_foto --}}
                <div class="form-group">
                    <label>Autor de la Foto</label>
                    <input type="text" class="form-control" 
                    placeholder="autor de la foto" v-model="publicacion.autor_foto" 
                    :class="formError(errores, 'autor_foto', 'inputError')">
                </div>

                {{-- Image Upload --}}
                <imageupload v-model="publicacion.imagen"></imageupload>
            </div>
        </div>
    </v-tab>
</vue-tabs>