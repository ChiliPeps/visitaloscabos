<!-- Vue Components & Mixins -->
@include('components.vue.vuePagination')
@include('components.vue.vueHelperFunctions')
@include('components.vue.vueFormErrors')
@include('components.vue.vueNotifications')

@include('components.vue.vueImageUpload')
@include('components.vue.vueFilterSearch')
@include('components.vue.vueTinyMce')

@include('components.vue.vueTabs')
<!-- Vue Components & Mixins -->

<script>
//Laravel's Token
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

var app = new Vue ({
    mounted: function () {
    	this.getData(this.dataRoute);
    },

    el: "#app",

    mixins: [helperFunctions, notifications],

    data: {

    	publicacion:{id:'', titulo:'', id_autor:'', descripcion:'', contenido:'', publish:'1', epigrafe:'', dedicatoria:'', autor_foto:'', slugurl:'', imagen:'', italica:''},
    	loading:false,
    	//panels
    	panelIndex:true,
    	panelInputs:false,

    	// Data Route
        dataRoute: "{{route('admin.publicaciones.get')}}",
        public_url: "{{ URL::to('/') }}/",

        pagination:null,
        publicaciones:null,
        // autores:null,

        saveButton:true,
        updateButton:false,

        errores:null,

        tabName: '',
    },

     computed: {
        slugurl: function() {
            return this.slugify(this.publicacion.titulo);
        },
        desCharCount: function () {
            return this.publicacion.descripcion.length;
        },
    },

    methods: {
    	getData: function (url) {
            this.loading = true;
            var filter = { busqueda: this.busqueda, tipo: this.tipo };
            var resource = this.$resource(url);
            resource.get(filter).then(function (response) {
                this.pagination = response.data;
                this.publicaciones = response.data.data;
                this.loading = false;
            });
        },

    	openPanelInputs:function(){
    		this.panelIndex=false
    		this.panelInputs=true
    	},

    	setData:function(){

    		var form = new FormData();
            form.append('titulo',            this.publicacion.titulo);
            form.append('slugurl',	 		 this.slugurl);
            form.append('descripcion',       this.publicacion.descripcion);
            form.append('contenido',         this.publicacion.contenido);
            // form.append('epigrafe',          this.publicacion.epigrafe);
            // form.append('id_autor',	 		 this.publicacion.id_autor);
            form.append('autor_foto',	 	 this.publicacion.autor_foto);
            // form.append('dedicatoria',       this.publicacion.dedicatoria);
            form.append('publish',           this.publicacion.publish);

            if(this.publicacion.imagen != '') { form.append('imagen', this.publicacion.imagen); }


            //Upload Progress Bar
            const progress = (e) => {
                if (e.lengthComputable) {
                    this.uploadProgress = Math.floor(e.loaded  / e.total * 100);
                }
            }

            var resource = this.$resource("{{route('admin.publicaciones.set')}}", {}, {}, { progress: progress });
            resource.save(form).then(function (response) {
                this.loadingSave = false;
                this.saveInAction = false;
                this.notification('fa fa-check-circle', 'OK!', "Noticia agregada correctamente.", 'topCenter');
                // this.cleanErrors();
                this.closePanelInputs();
                this.getData(this.dataRoute);
                
                this.uploadProgress = null;
            }, function (response) {
                
                this.loadingSave = false;
                this.saveInAction = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.errores = response.data.errors;
                this.uploadProgress = null;
            });
    	},

    	deletePublicacion: function (idPublicacion) {
            swal({
                title: '{{trans("cms.are_you_sure")}}',
                text: '{{trans("cms.wont_revert_this")}}',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, borrala!',
                cancelButtonText: '{{trans("cms.cancel")}}'
                }).then(function () {
                    var resource = this.$resource("{{route('admin.publicaciones.delete')}}");
                    resource.delete({id:idPublicacion}).then(function (response) {
                        this.notification('fa fa-check-circle', 'OK!', "Noticia Borrada!", 'topCenter');
                        this.getData(this.dataRoute);
                    }, function (response) {
                        this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                        this.checkExtraErrorMessages(response);
                    });
            }.bind(this)).catch(swal.noop);
        },

        openUpdateInputPanel: function (idPublicacion) {
            var index = _.findIndex(this.publicaciones, function(o) { return o.id === idPublicacion; });

            this.publicacion = {
                id:             this.publicaciones[index].id,
                titulo: 		this.publicaciones[index].titulo,

                descripcion: 	this.publicaciones[index].descripcion,
                contenido: 		this.publicaciones[index].contenido,
                
                autor_foto: 	this.publicaciones[index].autor_foto,
                imagen: 		this.publicaciones[index].imagen,
                publish: 		this.publicaciones[index].publish,
               
            }

            this.saveButton = false;
            this.updateButton = true;
            this.panelIndex = false;
            this.panelInputs = true;

            // this.comprobarUrl(); // Obtener Thumb de video de youtube
        },

        updateData: function () {
            // if(this.saveInAction == true) { return; }
            // this.isCatVideo(); // Update video_url
            // this.saveInAction = true;

            // this.loadingSave = true;
            // this.errores = null;

            var form = new FormData();
            form.append('id',              	 this.publicacion.id);
            form.append('titulo',            this.publicacion.titulo);
            form.append('slugurl',	 		 this.slugurl);
            form.append('descripcion',       this.publicacion.descripcion);
            form.append('contenido',         this.publicacion.contenido);

            form.append('autor_foto',	 	 this.publicacion.autor_foto);
            form.append('publish',           this.publicacion.publish);
            
            //Is File ?? o String ??
            if(this.publicacion.imagen != '') {
                if(Object.prototype.toString.call(this.publicacion.imagen) === '[object File]' ) {
                    form.append('imagen', this.publicacion.imagen);
                } else { form.append('imagen', 'no valid'); }
            }
            
            // Tags
            // form.append('tags', JSON.stringify(this.noticia.tags));

            //Upload Progress Bar
            const progress = (e) => {
                if (e.lengthComputable) {
                    this.uploadProgress = Math.floor(e.loaded  / e.total * 100);
                }
            }

            var resource = this.$resource("{{route('admin.publicaciones.update')}}", {}, {}, { progress: progress });
            resource.save(form).then(function (response) { 
                this.loadingSave = false;
                this.saveInAction = false;
                this.notification('fa fa-check-circle', 'OK!', "Noticia actualizada correctamente.", 'topCenter');
                // this.notifyWS(response.data.noticia_id);
                // this.cleanErrors();
                this.closePanelInputs();
                this.getData(this.dataRoute);
                this.uploadProgress = null;
            }, function (response) {
                this.loadingSave = false;
                this.saveInAction = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.errores = response.data.errors;
                this.uploadProgress = null;
            });
        },

        togglePublish: function (idPublicacion) {
            // Tiene que estar aprobada
            // var indexa = _.findIndex(this.noticias, function(o) { return o.id === idPublicacion; });
            // if (this.noticias[indexa].aprobada == false) {
            //     this.notification('fa fa-exclamation-triangle', 'Error', "La noticia no está aprobada.", 'topCenter');
            //     return;
            // }

            swal({
                title: '{{trans("cms.are_you_sure")}}',
                text: 'Se cambiará la visibilidad de la publicacion.',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí',
                cancelButtonText: '{{trans("cms.cancel")}}'
                }).then(function () {
                    var resource = this.$resource("{{route('admin.publicacion.publish')}}");
                    resource.save({id:idPublicacion}).then(function (response) { 
                        this.loadingPublish = false;
                        this.saveInAction = false;
                        var index = _.findIndex(this.publicaciones, function(o) { return o.id === idPublicacion; });
                        this.publicaciones[index].publish = response.data['publish'];
                    }, function (response) {
                        this.loadingPublish = false;
                        this.saveInAction = false;
                        this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                        this.checkExtraErrorMessages(response);
                    });
            }.bind(this)).catch(swal.noop);
        },

        previewNoticia: function (idPublicacion) {
            var index = _.findIndex(this.publicaciones, function(o) { return o.id === idPublicacion; });
            var noticia = this.publicaciones[index];
            // if (noticia.categoria == null) {return;}
            // var id_categoria = noticia.categoria.id;
            var url = this.public_url + 'preview/' + idPublicacion;
            window.open(url, '_blank');
        },

    	textTruncate: function (str, length, ending) {
            if (length == null) { length = 100; }  
            if (ending == null) { ending = '...'; }  
            if (str.length > length) {  
                return str.substring(0, length - ending.length) + ending;  
            } else { return str; }  
        },

    	checkImage: function (image_url) {
            if (!/^(f|ht)tps?:\/\//i.test(image_url)) {
                return this.public_url+image_url
            } else { return image_url }
        },

        closePanelInputs: function () {
            // this.cleanNoticiaInput();
            // this.cleanErrors();
            this.panelInputs = false;
            this.panelIndex = true;
            this.saveButton = true;
            this.updateButton = false;
            this.cleanInputs();
            this.cleanErrors();
            // this.emitOn = 0;

        },

        cleanInputs:function () {
        	this.publicacion = {id:'', titulo:'', id_autor:'', descripcion:'', contenido:'', publish:'1', epigrafe:'', dedicatoria:'', autor_foto:'', slugurl:'', imagen:'',}
        },

   		cleanErrors:function(){
   			this.errores=null
   		}
        
    }
});
</script>