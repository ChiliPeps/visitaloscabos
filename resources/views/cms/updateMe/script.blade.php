{{-- Mixins --}}
@include('components.vue.vueNotifications')
@include('components.vue.vueHelperFunctions')

{{-- Components --}}
@include('components.vue.vueFormErrors')
@include('components.vue.vueImageUpload')
@include('components.vue.vuePagination')
@include('components.vue.vueFilterSearch')
@include('components.vue.vueProgress')

<script>
//Laravel's Token
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

var app = new Vue ({

    mounted: function () {
        this.getCurrentUser();
    },

    el: '#app',

    mixins: [helperFunctions, notifications],

    data: {
        users: null,
        user: { id: '', name: '', avatar: '', email: '', password: '', password_confirmation: '', 
                type: 'suadmin', blocket_at: '', created_at: ''},
        loading: false,
        loadingSave: false,
        loadingPassword: false,
        saveInAction: false,

        public_url: "{{ URL::to('/') }}/",
        pagination: null,
        saveButton: false,
        updateButton: false,
        userTypes: [
            {text:'Super Admin', value:'suadmin'},
            {text:'Admin', value:'admin'},
            {text:'Editor', value:'editor'}
        ],
        uploadProgress: null,

        //Formulario errores
        errores: null
    },

    methods:{

        updateData: function () {
            if(this.saveInAction == true) { return; }
            this.saveInAction = true;
            this.loadingSave = true;

            var form = new FormData();
            form.append('id', this.user.id);
            form.append('name', this.user.name);

            //Is File ?? o String ??
            if(this.user.avatar != '') {
                if(Object.prototype.toString.call(this.user.avatar) === '[object File]' ) {
                    form.append('avatar', this.user.avatar);
                }
            }

            form.append('email', this.user.email);
            form.append('type', this.user.type);

            //Upload Progress Bar
            const progress = (e) => {
                if (e.lengthComputable) {
                    this.uploadProgress = Math.floor(e.loaded  / e.total * 100);
                }
            }

            var resource = this.$resource("{{route('admin.users.update')}}", {}, {}, { progress: progress });
            resource.save(form).then(function (response) { 
                this.loadingSave = false;
                this.saveInAction = false;
                this.notification('fa fa-check-circle', 'OK!', "{{trans('cms.msg_user_updated')}}", 'topCenter');
                this.cleanErrors();
                // this.closePanelInputs();
                // this.getData(this.dataRoute);
                // this.uploadProgress = null;
            }, function (response) {
                this.loadingSave = false;
                this.saveInAction = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.errores = response.data;
                this.uploadProgress = null;
            });
        },

        updatePassword: function () {
            if(this.saveInAction == true) { return; }
            this.saveInAction = true;
            this.loadingPassword = true;

            var form = new FormData();
            form.append('id', this.user.id);
            form.append('password', this.user.password);
            form.append('password_confirmation', this.user.password_confirmation);

            var resource = this.$resource("{{route('admin.users.password')}}");
            resource.save(form).then(function (response) { 
                this.loadingPassword = false;
                this.saveInAction = false;
                this.notification('fa fa-check-circle', 'OK!', "{{trans('cms.msg_password_updated')}}", 'topCenter');
                this.cleanErrors();
                this.user.password = ''; this.user.password_confirmation = '';
            }, function (response) {
                this.loadingPassword = false;
                this.saveInAction = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.errores = response.data;
            });
        },


        cleanErrors: function () {
            this.errores = null;
        },


        getCurrentUser:function(){
            var resource = this.$resource("{{route('admin.user.getCurrent')}}");
            resource.get().then(function (response) { 
                    this.loadingPassword = false;
                    this.saveInAction = false;
                    this.cleanErrors();
                    this.user = response.data;
                    this.user.password = ''; this.user.password_confirmation = '';
                }, function (response) {
                    this.loadingPassword = false;
                    this.saveInAction = false;
                    this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                    this.errores = response.data;
                });
        },

        // updateMe:function(){

        //     if (this.user.password == this.user.password_confirmation && this.user.password != '') {
        //         var form = new FormData();
        //         form.append('password', this.user.password);
        //         form.append('password_confirmation', this.user.password_confirmation);

        //         var resource = this.$resource("{{route('admin.user.updateMe')}}");
        //         resource.save(form).then(function (response) { 
        //             this.loadingPassword = false;
        //             this.saveInAction = false;
        //             this.notification('fa fa-check-circle', 'OK!', "{{trans('cms.msg_password_updated')}}", 'topCenter');
        //             this.user.password = ''; this.user.password_confirmation = '';
        //             this.cleanErrors();
        //         }, function (response) {
        //             this.loadingPassword = false;
        //             this.saveInAction = false;
        //             this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
        //             this.errores = response.data;
        //         });
        //     }
        //     else{
        //        this.notification('fa fa-exclamation-triangle', 'Error', "Las contraseñas no son iguales", 'topCenter');
        //     }
            
        // }
    }
});
</script>