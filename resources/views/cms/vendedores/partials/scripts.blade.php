<!-- Vue Components & Mixins -->
@include('components.vue.vuePagination')
@include('components.vue.vueHelperFunctions')
@include('components.vue.vueFormErrors')
@include('components.vue.vueNotifications')

@include('components.vue.vueImageUpload')
@include('components.vue.vueFilterSearch')
@include('components.vue.vueTinyMce')
@include('components.vue.vueTinyMceSmall')
@include('components.vue.vueDateRangePicker')

@include('components.vue.vueTabs')
<!-- Vue Components & Mixins -->

<script>
//Laravel's Token
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

var app = new Vue ({
    mounted: function () {
        this.getData(this.dataRoute);
    },

    el: "#app",

    mixins: [helperFunctions, notifications],

    data: {
        panelIndex:true,
        loading:false,
        panelInputs:false,

        saveButton:true,
        updateButton:false,

        vendedor:{id:'', nombre:'', active:'1'},

        vendedores:null,
        pagination:null,

        errores:null,
        dataRoute: "{{route('admin.vendedores.get')}}",


    },

    methods: {
        openPanelInputs: function(){
            this.panelIndex=false
            this.panelInputs=true
        },

        closePanelInputs:function(){
            this.vendedor = {nombre:'', active:''};
            this.panelIndex=true
            this.panelInputs=false
        },

        getData: function (url) {
            this.loading = true;
            var filter = { busqueda: this.busqueda, tipo: this.tipo };
            var resource = this.$resource(url);
            resource.get(filter).then(function (response) {
                this.pagination = response.data;
                this.vendedores = response.data.data;
                this.loading = false;
            });
        },

        setData:function(){
            this.loading = true;
            var form = new FormData();

            form.append('nombre',           this.vendedor.nombre);
            form.append('active',         this.vendedor.active);

            var resource = this.$resource("{{route('admin.vendedores.set')}}");
            resource.save(form).then(function (response) {
                // this.saveInAction = false;
                this.notification('fa fa-check-circle', 'OK!', "Vendedor agregado correctamente.", 'topCenter');
                // this.cleanErrors();
                this.loading = false;
                this.closePanelInputs();
                this.getData(this.dataRoute);
                
                // this.uploadProgress = null;
            }, function (response) {                
                
                this.saveInAction = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.loading = false;
                this.errores = response.data.errors;
                // this.uploadProgress = null;
            });
        },

        toggleActive: function (idVendedor) {
            swal({
                title: '{{trans("cms.are_you_sure")}}',
                text: 'Se cambiará la visibilidad del vendedor.',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí',
                cancelButtonText: '{{trans("cms.cancel")}}'
                }).then(function () {
                    var resource = this.$resource("{{route('admin.vendedores.active')}}");
                    resource.save({id:idVendedor}).then(function (response) { 
                        this.loadingPublish = false;
                        this.saveInAction = false;
                        var index = _.findIndex(this.vendedores, function(o) { return o.id === idVendedor; });
                        this.vendedores[index].active = response.data['active'];
                    }, function (response) {
                        this.loadingPublish = false;
                        this.saveInAction = false;
                        this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                        this.checkExtraErrorMessages(response);
                    });
            }.bind(this)).catch(swal.noop);
        },

        openUpdateInputPanel: function (idVendedor) {
            var index = _.findIndex(this.vendedores, function(o) { return o.id === idVendedor; });

            this.vendedor = {
                id:         this.vendedores[index].id,
                nombre:     this.vendedores[index].nombre,
                active:     this.vendedores[index].active,          
            }

            this.saveButton = false;
            this.updateButton = true;
            this.panelIndex = false;
            this.panelInputs = true;
        },

        updateData:function(){
            this.loading = true;

            var form = new FormData();
            form.append('id',              this.vendedor.id);
            form.append('nombre',          this.vendedor.nombre);
            form.append('active',          this.vendedor.active);
    

            var resource = this.$resource("{{route('admin.vendedores.update')}}");
            resource.save(form).then(function (response) { 
                this.loading = false;
                this.notification('fa fa-check-circle', 'OK!', "vendedor actualizado correctamente.", 'topCenter');
                // this.notifyWS(response.data.noticia_id);
                // this.cleanErrors();

                this.closePanelInputs();
                this.getData(this.dataRoute);
                // this.uploadProgress = null;
            }, function (response) {
                this.loading = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.errores = response.data.errors;
                this.uploadProgress = null;
            });
        },

        deleteVendedor:function(idVendedor){
             swal({
                title: '{{trans("cms.are_you_sure")}}',
                text: '{{trans("cms.wont_revert_this")}}',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, borrala!',
                cancelButtonText: '{{trans("cms.cancel")}}'
                }).then(function () {
                    var resource = this.$resource("{{route('admin.vendedores.delete')}}");
                    resource.delete({id:idVendedor}).then(function (response) {
                        this.notification('fa fa-check-circle', 'OK!', "Vendedor Borrado!", 'topCenter');
                        this.getData(this.dataRoute);
                    }, function (response) {
                        this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                        this.checkExtraErrorMessages(response);
                    });
            }.bind(this)).catch(swal.noop);
        }
    }
});
</script>