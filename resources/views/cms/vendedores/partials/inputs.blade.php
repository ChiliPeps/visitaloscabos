<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title" v-if="saveButton"> Guardar Vendedor</h3>
        <h3 class="box-title" v-if="updateButton"> Actualizar Vendedor</h3>
        <div class="box-tools pull-right">
{{--             @if(Auth::guard('cms')->user()->type != "reportero")
            <a class="btn bg-orange btn-sm" @click="aprobarNoticia" v-if="noticia.aprobada == 0"><i class="fa fa-thumbs-o-up"></i> Aprobar Noticia</a>
            @endif --}}

            <a class="btn bg-navy btn-sm" @click="closePanelInputs"><i class="fa fa-chevron-left"></i> Regresar</a>            
            <button :disabled="loading" class="btn btn-success btn-sm" v-show="saveButton" @click="setData()"> <i class="fa fa-floppy-o"></i> Guardar</button>

            <button :disabled="loading" class="btn btn-success btn-sm" v-show="updateButton" @click="updateData()"><i class="fa fa-floppy-o"></i> Actualizar</button>
        </div>
    </div>
</div>

<formerrors :errorsbag="errores"></formerrors>

<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Nombre</label>
                    <input type="text" class="form-control" 
                    placeholder="Titulo" v-model="vendedor.nombre" 
                    :class="formError(errores, 'titulo', 'inputError')">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group" >
                    <label>Activo 
                        <i v-if="vendedor.active == 1" class="fa fa-eye"></i>
                        <i v-else class="fa fa-eye-slash"></i>
                    </label>
                    <select class="form-control" v-model="vendedor.active" 
                    :class="formError(errores, 'active', 'inputError')">
                        <option value="1" selected>Activo</option>
                        <option value="0">Desactivo</option>
                    </select>
                </div>

            </div>
        </div>
    </div>
</div>

