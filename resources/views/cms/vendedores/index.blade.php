@extends('cms.master')

@section('content')
    <section class="content" id="app" v-cloak>
        <div class="box box-primary" v-show="panelIndex">

            <div class="box-header">
                <h3 class="box-title"></h3>
                <div class="box-tools pull-right">
                    <a href="#" class="btn bg-navy" @click="openPanelInputs">
                    <i class="fa fa-plus-circle"></i> Nuevo Vendedor</a>
                </div>

                {{-- <filtersearch :selected="tipo" :options="tipos" @updatefilters="updateFilters"></filtersearch> --}}
            </div>

            <div class="box-body">

                <!-- VueLoading icon -->
                <div class="text-center"><i v-show="loading" class="fa fa-spinner fa-spin fa-5x"></i></div>

                <div class="table-responsive" v-show="!loading">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Activo</th>
                            <th></th>


                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for = "v in vendedores">
                           <td>@{{v.nombre}}</td>
                           <td>
                                <button v-if="v.active == 1" class="btn btn-xl btn-success" @click="toggleActive(v.id)" 
                                style="margin-bottom: 10px;" title="No Publicar"><i class="fa fa-eye"></i></button>

                                <button v-else class="btn btn-xl btn-danger" @click="toggleActive(v.id)" 
                                style="margin-bottom: 10px;" title="No Publicar"><i class="fa fa-eye"></i></button>                                
                           </td>
                           <td>
                               <button class="btn btn-xs btn-warning" @click="openUpdateInputPanel(v.id)"
                                style="margin-bottom: 10px;" title="Actualizar Vendedor"><i class="fa fa-refresh"></i></button>

                                <button class="btn btn-xs btn-danger" @click="deleteVendedor(v.id)" 
                                style="margin-bottom: 10px;" title="Borrar Noticia"><i class="fa fa-trash"></i></button>
                           </td>

                        </tr>
                    </tbody>
                </table>
                </div>
            </div>

            <div class="box-footer clearfix">
                <pagination @setpage="getData" :param="pagination"></pagination>
            </div>
            
        </div>

        {{-- Panel de Inputs --}}
        <div v-show="panelInputs">
            @include('cms.vendedores.partials.inputs')
        </div>

    </section>
@endsection

@push('scripts')
    @include('cms.vendedores.partials.scripts')
@endpush