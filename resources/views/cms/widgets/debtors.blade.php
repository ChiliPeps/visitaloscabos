
<div class="box box-primary">
    <div class="box-body">

        <div class="text-center cargando" v-show="loading">
            {{-- <i  class="fa fa-spinner fa-spin fa-5x spinner"></i>     --}}
            <i class="fa fa-spinner fa-spin fa-5x spinner" style="top: 30%;position: absolute;"></i>
        </div>

        <h2>Deudores</h2>
        <div class="text-center">
            {{-- <i v-show="loading" class="fa fa-spinner fa-spin fa-5x"></i> --}}
        </div>

        <div class="table-responsive">
            <table class="table table-bordered {{-- table-hover --}}">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Hotel</th>
                        <th>Fecha de pago cliente</th>
                        <th>Cliente/pagado</th>
                        <th>Fecha de pago a hotel</th>
                        <th>Mensajes Enviados</th>
                        <th><button class="btn btn-success">Enviar a todos</button></th>

                    </tr>
                </thead>
                <tbody>
                    {{-- <tr v-for="d in debtors">
                        <td>@{{d.nombre}}</td>
                        <td>@{{d.hotel}}</td>
                        <td>
                            @{{d.limite_pago_cliente}}
                        </td>
                        <td  style="color: blue;">@{{d.limite_pago_hotel}}</td>
                        <td><button class="btn btn-warning" @click="mailDebtors(d)">Send</button></td>                           
                    </tr> --}}

                    <tr v-for="d in debtors">
                        <td>@{{d.reserva.nombre}}</td>
                        <td>@{{d.reserva.hotel}}</td>
                        <td nowrap="nowrap">
                            @{{shortDate(d.reserva.limite_pago_cliente)}}
                        </td>
                        <td>
                            <i v-if="d.reserva.reserva_pagada != null" style="color:green" class="fa fa-fw fa-check"></i>
                            <i v-else style="color:red" class="fa fa-fw fa-close"></i>
                        </td>
                        <td  style="color: blue;">@{{shortDate(d.reserva.limite_pago_hotel)}}</td>
                        <td>@{{d.send_messages}}</td>
                        <td nowrap="nowrap">
                            <button class="btn btn-warning btn-md" @click="mailDebtors(d.reserva)">Send</button>
                            <button class="btn btn-danger btn-md" @click="deleteDebtor(d.id)">Eliminar</button>
                        </td>                           
                    </tr>

                </tbody>
            </table>
        </div>
    </div>

    <div class="box-footer clearfix">
        <pagination @setpage="getDebtors" :param="pagination"></pagination>
    </div>
</div>
