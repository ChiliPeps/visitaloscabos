<!-- Vue Components & Mixins -->
@include('components.vue.vuePagination')
@include('components.vue.vueHelperFunctions')
@include('components.vue.vueFormErrors')

@include('components.vue.vueNotifications')
@include('components.vue.vueDatepicker')
@include('components.vue.vueDateRangePicker')
@include('components.vue.vueModal')
@include('components.vue.vueFilterSearch')

@include('components.vue.vueDateFilterSearch')
<!-- Vue Components & Mixins -->

<script>
//Laravel's Token
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

var app = new Vue ({
    created:function(){
        
    },

    mounted: function () {
        this.getYears();
        this.getDebtors(this.dataRoute);
        this.getCharts();

        
        this.createChartHotelsByDay();
        this.getHotelsByDay();

        this.createChartHotelsByReservas();
        this.getHotelsByReservas();

        this.createChartMedioContacto();
        this.getChartsMedioContacto();
    },

    el: "#app",

    mixins: [helperFunctions, notifications],

    data: {
        dataRoute: "{{route('admin.debtors.get')}}",

        loading:false,

        debtors:null,
        pagination:null,

        hotels:'',
        years:'',

        // chart mas dias
        selectedYear:'todos',
        chartDays: null,

        // chartMasReservas
        selectedYearReservas:'todos',
        chartMasReservas:null,

        // 
        selecteYearContacto:'2022',
        chartMedioContacto:null,
        
    },
    watch:{
       
    },

    computed:{


    },

    methods: {
    	getDebtors:function(url){
            this.loading = true;
            // var filter = { busqueda: this.busqueda, tipo: this.tipo };
            var resource = this.$resource(url);
            resource.get().then(function (response) {
                // console.log(response.data);
                this.pagination = response.data;
                this.debtors = response.data.data;
                this.loading = false;
            });
        },
        mailDebtors:function(info){
            // $datos = JSON.stringify(info)
            this.loading = true;
            var resource = this.$resource("{{route('recordatorio.send')}}");
            resource.save(info).then(function (response) {
                this.loading = false;
                this.getDebtors(this.dataRoute);
                this.notification('fa fa-check-circle', 'OK!', "Correo enviado correctamente.", 'topCenter');
            }, function (response) {
                this.loading = false;
                this.saveInAction = false;

                // Checar si hay configuración
                if (response.data.nodata) {
                    alert(response.data.error);
                } else {
                    this.errores = response.data.errors;
                }
            });
        },

        deleteDebtor:function(id){
            swal({
                title: '{{trans("cms.are_you_sure")}}',
                text: '{{trans("cms.wont_revert_this")}}',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, borrala!',
                cancelButtonText: '{{trans("cms.cancel")}}'
                }).then(function () {
                    var resource = this.$resource("{{route('admin.debtors.delete')}}");
                    resource.delete({id:id}).then(function (response) {
                        this.notification('fa fa-check-circle', 'OK!', "Deudor Borrado de la lista!", 'topCenter');
                        // this.getDebtors(this.dataRoute);
                        
                        var index = _.findIndex(this.debtors, function(o) { return o.id === response.data.id_debtor; });
                        this.debtors.splice(index, 1);

                    }, function (response) {
                        this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                        this.checkExtraErrorMessages(response);
                    });
            }.bind(this)).catch(swal.noop);
        },

        getCharts:function(){
            var resource = this.$resource("{{route('admin.charts.get')}}");
            resource.get().then(function (response) {
                // console.log(response.data);
                this.hotels = response.data
                // this.pagination = response.data;
                // this.debtors = response.data.data;
                this.loading = false;
            });
        },

        getYears(){
            var resource = this.$resource("{{route('admin.charts.getYears')}}");
            resource.get().then(function (response) {

                this.years = response.data
                this.loading = false;
            });
        },

        getHotelsByDay(){
            var year = this.selectedYear;
            this.loading = true
            var resource = this.$resource("{{route('admin.charts.get')}}");
            resource.get({year:year}).then(function (response) {
                // this.hotelsDays = response.data
                // console.log(reponse.data);
                this.chartDays.data.labels = response.data.hotels
                this.chartDays.data.datasets[0].data = response.data.dias
                // console.log(this.chartDays.data.datasets[0].data);
                
                this.chartDays.update();
                this.loading = false;
            });
        },

        createChartHotelsByDay() {
            var ctxProof= document.getElementById("proof");
            this.chartDays = new Chart(ctxProof, {
            type: 'bar',
            data: {
                labels: ['uno', 'dos', 'tres'],
                // labels: this.hotelsDays.hotels,
                datasets: [{
                    label: 'Hotel/Servicio con más días',
                    // data: this.hotelsDays.dias,
                    data:[12,12,12],
                    // backgroundColor: colors,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(255, 205, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(201, 203, 207, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                    ],
                    borderColor: [
                        'rgb(255, 99, 132)',
                        'rgb(255, 159, 64)',
                        'rgb(255, 205, 86)',
                        'rgb(75, 192, 192)',
                        'rgb(54, 162, 235)',
                        'rgb(153, 102, 255)',
                        'rgb(201, 203, 207)',
                        'rgb(255, 99, 132)',
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                    responsive:true,
                    maintainAspectRatio:true,
                    scales: {
                        y: {
                            beginAtZero: true
                        },
                    }
                }
          });
          // return proof // <<< this returns the created chart
        },

        createChartHotelsByReservas:function(){
            var ctxReservas = document.getElementById("reservas");
            this.chartMasReservas = new Chart(ctxReservas, {
            type: 'line',
            data: {
                labels: ['uno', 'dos', 'tres'],
                datasets: [{
                    label: 'Hotel/Servicio con más Reservas',
                    data:[30,10,52],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(255, 205, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(201, 203, 207, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                    ],
                    borderColor: [
                        'rgb(255, 99, 132)',
                        'rgb(255, 159, 64)',
                        'rgb(255, 205, 86)',
                        'rgb(75, 192, 192)',
                        'rgb(54, 162, 235)',
                        'rgb(153, 102, 255)',
                        'rgb(201, 203, 207)',
                        'rgb(255, 99, 132)',
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                    responsive:true,
                    maintainAspectRatio:true,
                    scales: {
                        y: {
                            beginAtZero: true
                        },
                    }
                }
          });
          // return chartMasReservas
        },

        getHotelsByReservas(){
            var year = this.selectedYearReservas;
            this.loading = true
            var resource = this.$resource("{{route('admin.charts.getReservas')}}");
            resource.get({year:year}).then(function (response) {
                this.chartMasReservas.data.labels = response.data.hotels
                this.chartMasReservas.data.datasets[0].data = response.data.totals

                this.chartMasReservas.update();
                this.loading = false;
            });
        },

        createChartMedioContacto:function(){
            var ctxMedioContacto = document.getElementById("medioContacto");
            this.chartMedioContacto = new Chart(ctxMedioContacto, {
            type: 'pie',
            data: {
                labels: ['uno', 'dos', 'tres'],
                datasets: [{
                    label: 'Hotel/Servicio con más Reservas',
                    data:[30,10,52],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(255, 205, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(201, 203, 207, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                    ],
                    borderColor: [
                        'rgb(255, 99, 132)',
                        'rgb(255, 159, 64)',
                        'rgb(255, 205, 86)',
                        'rgb(75, 192, 192)',
                        'rgb(54, 162, 235)',
                        'rgb(153, 102, 255)',
                        'rgb(201, 203, 207)',
                        'rgb(255, 99, 132)',
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                    responsive:true,
                    maintainAspectRatio:true,
                    scales: {
                        y: {
                            beginAtZero: true
                        },
                    }
                }
          });
          // return chartMasReservas
        },

        getChartsMedioContacto(){
            var year = this.selecteYearContacto;
            this.loading = true
            var resource = this.$resource("{{route('admin.charts.getMedioContacto')}}");
            resource.get({year:year}).then(function (response) {
                console.log(response.data);

                this.chartMedioContacto.data.labels = response.data.medio
                this.chartMedioContacto.data.datasets[0].data = response.data.medioTotal

                this.chartMedioContacto.update();
                this.loading = false;
            });
        },
    }
});
</script>