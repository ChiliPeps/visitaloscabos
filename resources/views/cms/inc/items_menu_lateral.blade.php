
{!! CMSHelpers::makeLinkForSidebarMenu('admin.configuraciones.index', 'Configuración', 'fa fa-cogs') !!}
{!! CMSHelpers::makeLinkForSidebarMenu('admin.errores.index', 'Bítacora de Errores', 'fa fa-bomb') !!}
{!! CMSHelpers::makeLinkForSidebarMenu('admin.destinos.index', 'Destinos', 'fa fa-tasks') !!}
{!! CMSHelpers::makeLinkForSidebarMenu('admin.promociones.index', 'Promociones', 'fa fa-dollar ') !!}
{!! CMSHelpers::makeLinkForSidebarMenu('admin.publicaciones.index', 'Publicaciones', 'fa fa-edit') !!}
{!! CMSHelpers::makeLinkForSidebarMenu('admin.reservas.index', 'Reservas', 'fa fa-calendar-check-o') !!}
{!! CMSHelpers::makeLinkForSidebarMenu('admin.ventas.index', 'Ventas en Linea', 'fa Example of globe fa-globe') !!}

@if($user_type == 'suadmin' || $user_type == 'admin')
	{!! CMSHelpers::makeLinkForSidebarMenu('admin.vendedores.index', 'Vendedores', 'fa fa-male') !!}
@endif

