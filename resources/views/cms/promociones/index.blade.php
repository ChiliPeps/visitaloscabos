@extends('cms.master')

@section('content')
    <section class="content-header">
        <h1><i class="fa fa-dollar"></i> Promociones </h1>
    </section>

    <section class="content" id="app" v-cloak>
    	<div class="box box-primary" v-show="panelIndex">

            <div class="box-header">
                <h3 class="box-title"></h3>
                <div class="box-tools pull-right">
                    <a href="#" class="btn bg-navy" @click="openPanelInputs">
                    <i class="fa fa-plus-circle"></i> Nueva Promocion</a>
                </div>

                {{-- <filtersearch :selected="tipo" :options="tipos" @updatefilters="updateFilters"></filtersearch> --}}
            </div>

            <div class="box-body">

                <!-- VueLoading icon -->
                <div class="text-center"><i v-show="loading" class="fa fa-spinner fa-spin fa-5x"></i></div>

                <div class="table-responsive" v-show="!loading">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th colspan="2">Nombre</th>
                            <th>Precio</th>
                            <th></th>


                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="p in promociones">
                           <td style="width: 85px;"><img :src="checkImage(p.thumb)" class="img-rounded" width="75" height="50"></td>
                            <td>
                               <div>
                                    <div style="color: #3c8dbc; font-weight: bold;" :title="p.titulo" v-html="p.titulo"></div>
                                    <div>@{{ dateString2(p.created_at) }}</div>
                                    <div></div>
                                </div>
                            </td>

                           <td>@{{p.precio}}</td>
                           <td>
                            <button class="btn btn-xs btn-warning" @click="openUpdateInputPanel(p.id)"
                                style="margin-bottom: 10px;" title="Actualizar Noticia"><i class="fa fa-refresh"></i></button>

                            <button class="btn btn-xs btn-danger" @click="deletePromocion(p.id)" 
                                style="margin-bottom: 10px;" title="Borrar Noticia"><i class="fa fa-trash"></i></button>
                           </td>

                        </tr>
                    </tbody>
                </table>
                </div>
            </div>

            <div class="box-footer clearfix">
                {{-- <pagination @setpage="getData" :param="pagination"></pagination> --}}
            </div>
            
        </div>

        {{-- Panel de Inputs --}}
        <div v-show="panelInputs">
            @include('cms.promociones.partials.inputs')
        </div>

    </section>
@endsection

@push('scripts')
    @include('cms.promociones.partials.scripts')
@endpush