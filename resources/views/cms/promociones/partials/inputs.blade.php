<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title" v-if="saveButton"> Crear Promocion</h3>
        <h3 class="box-title" v-if="updateButton"> Actualizar Promocion</h3>
        <div class="box-tools pull-right">
{{--             @if(Auth::guard('cms')->user()->type != "reportero")
            <a class="btn bg-orange btn-sm" @click="aprobarNoticia" v-if="noticia.aprobada == 0"><i class="fa fa-thumbs-o-up"></i> Aprobar Noticia</a>
            @endif --}}

            <a class="btn bg-navy btn-sm" @click="closePanelInputs"><i class="fa fa-chevron-left"></i> Regresar</a>            
            <button :disabled="loading" class="btn btn-success btn-sm" v-show="saveButton" @click="setData()"> <i class="fa fa-floppy-o"></i> Guardar</button>

            <button :disabled="loading" class="btn btn-success btn-sm" v-show="updateButton" @click="updateData()"><i class="fa fa-floppy-o"></i> Actualizar</button>
        </div>
    </div>
</div>

<formerrors :errorsbag="errores"></formerrors>


<vue-tabs>
     <v-tab title="Contenido">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Titulo</label>
                            <input type="text" class="form-control" 
                            placeholder="Titulo" v-model="promocion.titulo" 
                            :class="formError(errores, 'titulo', 'inputError')">
                        </div>

                        <div class="form-group">
                            <label>SlugURL</label>
                            <input type="text" class="form-control" 
                            placeholder="slugurl" v-model="slugurl" readonly>
                        </div>

                        
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Precio Adulto</label>
                            <input @keypress="onlyNumber" type="text" class="form-control" 
                            placeholder="Precio Adulto (2)" v-model="promocion.precio" 
                            :class="formError(errores, 'precio', 'inputError')">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Precio Tercer Adulto</label>
                            <input @keypress="onlyNumber" type="text" class="form-control" 
                            placeholder="Precio 3er Adulto" v-model="promocion.precioTercer" 
                            :class="formError(errores, 'precio', 'inputError')">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Vigencia</label>
                            <daterangepicker v-model="promocion.vigencia"></daterangepicker>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for=""> Menor 1</label>
                            <input @keypress="onlyNumber" type="text" class="form-control" 
                            placeholder="Precio menor 1" v-model="promocion.menorPrecio1" 
                            :class="formError(errores, 'precio', 'inputError')">
                            <p>Edad maxima</p>

                            <select class="form-group" v-model="promocion.menorEdad1" @change="cleanFirst()">
                                <option :value="n" v-for="n in 17"> @{{n}} </option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for=""> Menor 2</label>
                            <input @keypress="onlyNumber" type="text" class="form-control" 
                            placeholder="Precio menor 2" v-model="promocion.menorPrecio2" 
                            :class="formError(errores, 'precio', 'inputError')">
                            <p>Edad maxima</p>
                            <select class="form-group" v-model="promocion.menorEdad2" @change="mayorque()">
                                <option :value="n" v-for="n in 17"> @{{n}} </option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for=""> Menor 3</label>
                            <input @keypress="onlyNumber" type="text" class="form-control" 
                            placeholder="Precio menor 3" v-model="promocion.menorPrecio3" 
                            :class="formError(errores, 'precio', 'inputError')">
                        </div>
                            <p>Edad maxima</p>
                            <select class="form-group" v-model="promocion.menorEdad3" @change="mayorque()">
                                <option :value="n" v-for="n in 17"> @{{n}} </option>
                            </select>
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Descripción</label>
                            {{-- <textarea rows="4" class="form-control" v-model="promocion.descripcion"></textarea> --}}
                            <tinymce2 id="tinyMCE2" v-model="promocion.descripcion"></tinymce2>
                        </div>

                        <div class="form-group">
                            <label>Contenido</label>
                            <tinymce id="tinyMCE" imageuploadurl="{{route('admin.promociones.tinyimages')}}" 
                            v-model="promocion.contenido"></tinymce>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </v-tab>

      <v-tab title="Imagen">
        <div class="box box-primary">
            <div class="box-header"><h2 class="box-title">Imagen de Noticia (960 x 540)</h2></div>
            <div class="box-body">
                {{-- Image Upload --}}
                <imageupload v-model="promocion.imagen"></imageupload>
            </div>
        </div>
      </v-tab>
</vue-tabs>
