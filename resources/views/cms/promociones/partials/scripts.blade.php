<!-- Vue Components & Mixins -->
@include('components.vue.vuePagination')
@include('components.vue.vueHelperFunctions')
@include('components.vue.vueFormErrors')
@include('components.vue.vueNotifications')

@include('components.vue.vueImageUpload')
@include('components.vue.vueFilterSearch')
@include('components.vue.vueTinyMce')
@include('components.vue.vueTinyMceSmall')
@include('components.vue.vueDateRangePicker')

@include('components.vue.vueTabs')
<!-- Vue Components & Mixins -->

<script>
//Laravel's Token
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

var app = new Vue ({
    mounted: function () {
    	this.getData(this.dataRoute);
    },

    el: "#app",

    mixins: [helperFunctions, notifications],

    data: {
    	loading:false,
    	panelInputs:false,
    	panelIndex:true,

    	saveButton:true,
    	updateButton:false,

    	errores:null,

    	pagination:null,
        promociones:null,

        dataRoute: "{{route('admin.promociones.get')}}",
        public_url: "{{ URL::to('/') }}/",

        promocion:{id:'', titulo:'', slug_url:'', precio:'', precioTercer:'', descripcion:'', contenido:'', imagen:'', fecha_inicio:'', fecha_fin:'', vigencia:'', 
        menorPrecio1:'', menorPrecio2:'', menorPrecio3:'', menorEdad1:'', menorEdad2:'', menorEdad3:''}

    },

    computed: {
        slugurl: function() {
            return this.slugify(this.promocion.titulo);
        },
    },

    methods: {
    	getData: function (url) {
            this.loading = true;
            var filter = { busqueda: this.busqueda, tipo: this.tipo };
            var resource = this.$resource(url);
            resource.get(filter).then(function (response) {
                this.pagination = response.data;
                this.promociones = response.data.data;
                this.loading = false;
            });
        },

        setData:function(){
            this.loading = true;
            var form = new FormData();

            form.append('titulo',           this.promocion.titulo);
            form.append('slug_url',         this.slugurl);
            form.append('precio',           this.promocion.precio);
            form.append('precioTercer',     this.promocion.precioTercer);
            form.append('imagen',           this.promocion.imagen);
            form.append('fecha_inicio',     this.promocion.vigencia.startDate);
            form.append('fecha_fin',        this.promocion.vigencia.endDate);
            form.append('thumb',            this.promocion.thumb);
            form.append('descripcion',      this.promocion.descripcion);     
            form.append('contenido',        this.promocion.contenido);
            form.append('menorPrecio1',     this.promocion.menorPrecio1);
            form.append('menorPrecio2',     this.promocion.menorPrecio2);
            form.append('menorPrecio3',     this.promocion.menorPrecio3);
            form.append('menorEdad1',       this.promocion.menorEdad1);
            form.append('menorEdad2',       this.promocion.menorEdad2);
            form.append('menorEdad3',       this.promocion.menorEdad3);


            if(this.promocion.imagen != '') { form.append('imagen', this.promocion.imagen); }


            //Upload Progress Bar
            const progress = (e) => {
                if (e.lengthComputable) {
                    this.uploadProgress = Math.floor(e.loaded  / e.total * 100);
                }
            }

            var resource = this.$resource("{{route('admin.promociones.set')}}", {}, {}, { progress: progress });
            resource.save(form).then(function (response) {
                this.saveInAction = false;
                this.notification('fa fa-check-circle', 'OK!', "Noticia agregada correctamente.", 'topCenter');
                this.cleanErrors();
                this.loading = false;
                this.closePanelInputs();
                this.getData(this.dataRoute);
                
                // this.uploadProgress = null;
            }, function (response) {                
                
                this.saveInAction = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.loading = false;
                this.errores = response.data.errors;
                // this.uploadProgress = null;
            });
        },

        deletePromocion: function (idPromocion) {
            swal({
                title: '{{trans("cms.are_you_sure")}}',
                text: '{{trans("cms.wont_revert_this")}}',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, borrala!',
                cancelButtonText: '{{trans("cms.cancel")}}'
                }).then(function () {
                    var resource = this.$resource("{{route('admin.promociones.delete')}}");
                    resource.delete({id:idPromocion}).then(function (response) {
                        this.notification('fa fa-check-circle', 'OK!', "Promocion Borrada!", 'topCenter');
                        this.getData(this.dataRoute);
                    }, function (response) {
                        this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                        this.checkExtraErrorMessages(response);
                    });
            }.bind(this)).catch(swal.noop);
        },

        openUpdateInputPanel: function (idPromocion) {
            var index = _.findIndex(this.promociones, function(o) { return o.id === idPromocion; });

            this.promocion = {
                id:             this.promociones[index].id,
                titulo:         this.promociones[index].titulo,
                precio:         this.promociones[index].precio,
                precioTercer:   this.promociones[index].precioTercer,  
                descripcion:    this.promociones[index].descripcion,
                contenido:      this.promociones[index].contenido,
                fecha_inicio:   this.promociones[index].fecha_inicio,
                fecha_fin:      this.promociones[index].fecha_fin,
                imagen:         this.promociones[index].imagen,

                menorPrecio1:   this.promociones[index].menorPrecio1,
                menorPrecio2:   this.promociones[index].menorPrecio2,
                menorPrecio3:   this.promociones[index].menorPrecio3,
                menorEdad1:     this.promociones[index].menorEdad1,
                menorEdad2:     this.promociones[index].menorEdad2,
                menorEdad3:     this.promociones[index].menorEdad3,

                vigencia: {startDate:this.promociones[index].fecha_inicio, endDate:this.promociones[index].fecha_fin}
          
            }
            this.saveButton = false;
            this.updateButton = true;
            this.panelIndex = false;
            this.panelInputs = true;

            // this.comprobarUrl(); // Obtener Thumb de video de youtube
        },


        updateData: function () {
            this.loading = true;
            // if(this.saveInAction == true) { return; }
            // this.isCatVideo(); // Update video_url
            // this.saveInAction = true;

            // this.loadingSave = true;
            // this.errores = null;

            var form = new FormData();
            form.append('id',                this.promocion.id);
            form.append('titulo',            this.promocion.titulo);
            form.append('slug_url',          this.slugurl);

            form.append('descripcion',       this.promocion.descripcion);
            form.append('contenido',         this.promocion.contenido);
            form.append('fecha_inicio',      this.promocion.vigencia.startDate);
            form.append('fecha_fin',         this.promocion.vigencia.endDate);
            form.append('imagen',            this.promocion.imagen);
            form.append('precio',            this.promocion.precio);

            form.append('precioTercer',      this.promocion.precioTercer);
            form.append('menorPrecio1',      this.promocion.menorPrecio1);
            form.append('menorPrecio2',      this.promocion.menorPrecio2);
            form.append('menorPrecio3',      this.promocion.menorPrecio3);
            form.append('menorEdad1',        this.promocion.menorEdad1);
            form.append('menorEdad2',        this.promocion.menorEdad2);
            form.append('menorEdad3',        this.promocion.menorEdad3);
            
            //Is File ?? o String ??
            if(this.promocion.imagen != '') {
                if(Object.prototype.toString.call(this.promocion.imagen) === '[object File]' ) {
                    form.append('imagen', this.promocion.imagen);
                } else { form.append('imagen', 'no valid'); }
            }
            
            // Tags
            // form.append('tags', JSON.stringify(this.noticia.tags));

            //Upload Progress Bar
            const progress = (e) => {
                if (e.lengthComputable) {
                    this.uploadProgress = Math.floor(e.loaded  / e.total * 100);
                }
            }

            var resource = this.$resource("{{route('admin.promociones.update')}}", {}, {}, { progress: progress });
            resource.save(form).then(function (response) { 
                this.loading = false;
                this.notification('fa fa-check-circle', 'OK!', "Promocion actualizada correctamente.", 'topCenter');
                // this.notifyWS(response.data.noticia_id);
                // this.cleanErrors();
                this.closePanelInputs();
                this.getData(this.dataRoute);
                this.uploadProgress = null;
            }, function (response) {
                this.loading = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.errores = response.data.errors;
                this.uploadProgress = null;
            });
        },

        openPanelInputs:function(){
    		this.panelIndex=false
    		this.panelInputs=true
    	},

    	closePanelInputs(){
    		this.panelIndex=true
    		this.panelInputs=false
            this.saveButton = true
            this.updateButton = false
            this.cleanInputs();
    	},

    	checkImage: function (image_url) {
            if (!/^(f|ht)tps?:\/\//i.test(image_url)) {
                return this.public_url+image_url
            } else { return image_url }
        },

        cleanInputs:function () {
            this.promocion = {id:'', titulo:'',imagen:'', descripcion:'', contenido:'', precio:'', precioTercer:'', menorPrecio1:'', menorPrecio2:'', menorPrecio3:'', menorEdad1:'', menorEdad2:'', menorEdad3:''}
            this.promocion.vigencia = {startDate:moment().format('YYYY-MM-DD'), endDate:moment().format('YYYY-MM-DD')}
            this.cleanErrors();
        },

        cleanErrors:function(){
            this.errores=null
        },

        onlyNumber ($event) {
           //console.log($event.keyCode); //keyCodes value
           let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
           if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
              $event.preventDefault();
           }
        },

        cleanFirst:function(){
            this.promocion.menorEdad2 = ""
            this.promocion.menorEdad3 = ""
        },

        mayorque:function(){
            if (this.promocion.menorEdad1 >= this.promocion.menorEdad2) {
                this.notificationError('Error', "debes seleccionar una edad mayor a la del primer menor", 'topCenter');               
                this.promocion.menorEdad2="";
            }

            if (this.promocion.menorEdad3 != "" && this.promocion.menorEdad2 >= this.promocion.menorEdad3 ) {
                this.notificationError('Error', "debes seleccionar una edad mayor a la del segundo menor", 'topCenter');               
                this.promocion.menorEdad3="";
            }


        }




    }
});
</script>