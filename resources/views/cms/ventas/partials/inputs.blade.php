<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title" v-if="saveButton"> Crear Reserva</h3>
        <h3 class="box-title" v-if="updateButton"> Actualizar Reserva</h3>
        <div class="box-tools pull-right">
{{--             @if(Auth::guard('cms')->user()->type != "reportero")
            <a class="btn bg-orange btn-sm" @click="aprobarNoticia" v-if="noticia.aprobada == 0"><i class="fa fa-thumbs-o-up"></i> Aprobar Noticia</a>
            @endif --}}
            <a class="btn bg-navy btn-sm" @click="regresarReserva"><i class="fa fa-chevron-left"></i> Regresar</a>
            <a class="btn btn-success btn-sm" v-show="saveButton" @click="setData()"><i class="fa fa-floppy-o"></i> 
            Guardar</a>
            <a class="btn btn-success btn-sm" v-show="updateButton" @click="updateData()"><i class="fa fa-floppy-o"></i> Actualizar</a>
        </div>
    </div>
</div>

{{-- Form Errros --}}
<formerrors :errorsbag="erroresReserva"></formerrors>

    {{-- <br> --}}
        <div class="row">
            <div class="col-md-6">
        
                <div class="box box-primary">
                    <div class="box-body">
                        
                        <div class="form-group">
                            <label>Tipo</label>
                            <select class="form-control" v-model="reserva.tipo" :class="formError(errores, 'type', 'inputError')">
                                <option value="reserva"> Reserva</option>
                                <option value="pago"> Pago </option>
                            </select>
                            
                        </div>
                        {{-- titulo --}}
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" class="form-control" 
                            placeholder="Nombre" v-model="reserva.nombre" 
                            :class="formError(errores, 'nombre', 'inputError')">
                        </div>

                        <div class="form-group">
                            <label>Telefono</label>
                            <input type="text" class="form-control"  v-model="reserva.telefono"  
                            placeholder="612 14 12345">
                        </div>

                        <div class="form-group">
                            <label>Correo</label>
                            <input type="text" class="form-control"  v-model="reserva.correo"  
                            placeholder="Correo" 
                            >
                        </div>

                    </div>
                </div>
        
            </div>
            {{-- calcula el precio automatico, etc, activar el watch de fechas --}}
            {{-- <div class="col-md-6">
        
                <div class="box box-danger">
                    <div class="box-body">

                        <div class="form-group">
                            <label>Hotel:</label>
                            <select class="form-control" v-model="reserva.titulo" :class="formError(errores, 'type', 'inputError')" @change="getPreciosHotel(reserva.titulo)">
                                <option  v-for="promo in promoInfo" :value="promo.titulo">@{{promo.titulo}}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Fechas</label>
                            <daterangepicker v-model="fechas"></daterangepicker>
                        </div>

                        <div class="form-group">
                            <label>Tercer Adulto:</label>
                            <input type="checkbox" id="checkbox" v-model="reserva.tercerAdulto" @change="tresAdultos">
                            
                        </div>

                        <div class="form-group">
                            <label>Menores</label>
                            <select class="form-control" id="cars" v-model="reserva.menores" @change="cuantosMenores">
                                <option selected :value="0" > 0</option>
                                <option :value="1">1</option>
                                <option :value="2" v-show="reserva.tercerAdulto==false">2</option>
                            </select>
                        </div>

                        <div class="form-group" v-if="reserva.menores > 0">
                            <div class="row">

                                <div class="col-md-6"> 
                                    <label>Edad menor 1: </label>
                                </div>

                                <div class="col-md-6">
                                    <select class="edades" v-model="reserva.edad1">
                                        <option :value="n" v-for="n in 17" >@{{n}}</option>
                                    </select>
                                </div>

                            </div>

                            <div class="row" v-show="reserva.menores > 1 || reserva.tercerAdulto == false">
                                <div class="col-md-6"> 
                                    <label>Edad menor 2: </label>
                                </div>

                                <div class="col-md-6">
                                    <select class="edades" v-model="reserva.edad2">
                                        <option :value="n" v-for="n in 17" >@{{n}}</option>
                                    </select>
                                </div>

                            </div>                      
                        </div>

                        <div class="form-group">
                            <label>Precio</label>
                            <input v-show="readonly == false"type="text" class="form-control" :readonly="readonly==false" v-model="total">  

                            <input v-show="readonly" type="text" class="form-control" :readonly="readonly==false" v-model="reserva.precio"> 

                            <input type="checkbox" id="checkbox" @change="recuperaValor" v-model="readonly">Editar                        
                        </div>

                    </div>
                </div>
        
            </div> --}}
            {{-- Manuales --}}
            <div class="col-md-6">
                <div class="box box-danger">
                    <div class="box-body">

                        <div class="form-group">
                            <label>Hotel:</label>
                            <input type="text" class="form-control" 
                            placeholder="Hotel" v-model="reserva.hotel">
                            
                        </div>

                        <div class="form-group">
                            <label>Fechas</label>
                            <daterangepicker v-model="fechas" ref="foo"></daterangepicker>
                        </div>

                        <div class="form-group">
                            <label>Tercer Adulto:</label>
                            <input type="checkbox" id="checkbox" v-model="reserva.tercerAdulto" >
                            
                        </div>

                        <div class="form-group">
                            <label>Menores</label>
                            <select class="form-control" id="cars" v-model="reserva.menores">
                                <option selected :value="0" > 0</option>
                                <option :value="1">1</option>
                                <option :value="2" v-show="reserva.tercerAdulto==false">2</option>
                            </select>
                        </div>

                        <div class="form-group" v-if="reserva.menores > 0">
                            <div class="row">

                                <div class="col-md-6"> 
                                    <label>Edad menor 1: </label>
                                </div>

                                <div class="col-md-6">
                                    <select class="edades" v-model="reserva.edad1">
                                        <option :value="n" v-for="n in 17" >@{{n}}</option>
                                    </select>
                                </div>

                            </div>

                            <div class="row" v-show="reserva.menores > 1 || reserva.tercerAdulto == false">
                                <div class="col-md-6"> 
                                    <label>Edad menor 2: </label>
                                </div>

                                <div class="col-md-6">
                                    <select class="edades" v-model="reserva.edad2">
                                        <option :value="n" v-for="n in 17" >@{{n}}</option>
                                    </select>
                                </div>

                            </div>                      
                        </div>

                        <div class="form-group">
                            <label>Precio</label>
                            
                            <input type="text" class="form-control" v-model="reserva.precio"> 
                        </div>

                    </div>
                </div>
            </div>
            
        </div>

    {{-- <v-tab title="Galeria">
        
    </v-tab> --}}
</vue-tabs>