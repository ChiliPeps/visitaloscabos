<div class="box box-primary" >
            <div class="box-header">
                <h3 class="box-title"></h3>
                <div class="box-tools pull-right">
                    <a href="#" class="btn bg-yellow" @click="regresar">
                    <i class="fa fa-backward"></i>Regresar</a>

                    <a href="#" class="btn bg-navy" @click="prueba">
                    <i class="fa fa-plus-circle"></i>Registrar Pago</a>
                </div>

                <div class="box-body">
                    {{-- informacion --}}
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          
                          <address>
                            <strong>Información personal</strong><br>
                            Nombre:@{{reserva.nombre}}<br>
                            Tel: @{{reserva.telefono}}<br>
                            Email: @{{reserva.correo}}<br>
                            Menores: @{{reserva.menores}}
                          </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          
                          <address>
                            <strong>Informacion Reserva</strong><br>
                            @{{reserva.hotel}}<br>
                            Entrada: @{{reserva.fecha_entrada}}<br>
                            Salida: @{{reserva.fecha_salida}}<br>
                           
                          </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          <b>Datos de pago</b><br>
                          <br>
                          <b>Precio     :</b>$@{{reserva.precio}}<br>
                          <b>Total pagos:</b> @{{totalPagos}}<br>
                          <b>Restante   :</b> @{{restante}}
                        </div>                       
                      </div>
                       {{-- informacion --}}
                    <div class="text-center">

                        <i v-show="loading" class="fa fa-spinner fa-spin fa-5x"></i>
                    </div>

                    <div class="table-responsive" v-show="!loading">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID Pago</th>
                                    <th>Folio</th>
                                    <th>Cantidad</th>
                                    <th>Registrado por:</th>
                                    <th>Fecha</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for ="p in pagos">
                                    <td>@{{p.id}}</td>
                                    <td>@{{p.folio}}</td>
                                    <td>@{{p.cantidad}}</td>
                                    <td>@{{p.usuario}}</td>
                                    <td>@{{p.created_at}}</td>
                                    <td>
                                    <button class="btn btn-xs btn-warning" @click="loadDatosPago(p.id)"
                                        style="margin-bottom: 10px;" title="Actualizar Pago"><i class="fa fa-refresh"></i></button>

                                    <button class="btn btn-xs btn-danger" @click="borrarPago(p.id)" 
                                        style="margin-bottom: 10px;" title="Borrar Pago"><i class="fa fa-trash"></i></button>
                                    </td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                
            </div>

            <modal v-if="modalPago" @close="modalPago = false" >
                <div slot="head">Pago</div>
                <div slot="body">
                    <div class="row">                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Folio</label>
                                <input type="text" class="form-control" 
                                placeholder="Folio" v-model="pago.folio" 
                                :class="formError(errores, 'titulo', 'inputError')">
                            </div>                                        
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Cantidad</label>
                                <input type="text" class="form-control" 
                                placeholder="Cantidad $ 123.11" v-model="pago.cantidad" 
                                :class="formError(errores, 'titulo', 'inputError')">
                            </div>
                        </div>

                    </div>       
                </div>

                <div slot="footer">
                    <div class="text-right"> 
                        <a class="modal-default-button btn bg-navy" @click="closeModalPago()"><i class="fa fa-times-circle"></i> Cerrar</a>
                        <a v-show="saveButtonPago" class="modal-default-button btn bg-green" @click="registrarPago()"><i class="fa fa-times-circle"></i> Registrar </a>    

                        <a v-show="updateButtonPago" class="modal-default-button btn bg-green" @click="updatePago()"><i class="fa fa-times-circle"></i> Actualizar </a> 
                    </div>
                </div>
            </modal>

        </div>