<!-- Vue Components & Mixins -->
@include('components.vue.vuePagination')
@include('components.vue.vueHelperFunctions')
@include('components.vue.vueFormErrors')

@include('components.vue.vueNotifications')
@include('components.vue.vueDatepicker')
@include('components.vue.vueDateRangePicker')
@include('components.vue.vueModal')
@include('components.vue.vueFilterSearch')

@include('components.vue.vueDateFilterSearch')
<!-- Vue Components & Mixins -->

<script>
//Laravel's Token
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

var app = new Vue ({
    mounted: function () {
        this.getCompras(this.dataRoute);
    },

    el: "#app",

    mixins: [helperFunctions, notifications],

    data: {
        // inputs 
        panelReserva:false,
        saveButton:true,
        updateButton:false,

        //FilterSearch
        tipo:'nombre',
        tipos:[
            { text: 'Nombre', value:'nombre' },
            { text: 'Folio', value:'folio' },
            { text: 'Hotel', value:'hotel' },
            { text:'Fecha entrada', value:'fecha_entrada'},
            { text:'Limite de pago/Cliente', value:'limite_pago_cliente'},
            { text:'Limite de pago Hotel', value:'limite_pago_hotel'}
        ],

        // 
        fechas:'',
        reserva:{id:'', tipo:'', nombre:'', telefono:'', correo:'', hotel:'', fecha_entrada:'', fecha_salida:'', tercerAdulto:false, menores:'0', edad1:'', edad2:'', precio:'', limite_pago_cliente:'', limite_pago_hotel:'', clave:'', pagado:''}, 
        dias:'',

        panelIndex:true,
        panelPagos:false,
        panelCreate:false,

        loading:false,
        reservas:null,
        pagination:null,
        dataRoute: "{{route('admin.ventas.get')}}",

        modalPago:false,

        pago:{id:'', id_venta:'', cantidad:'', folio:'', nombreUser:''},

        errores:null,
        erroresReserva:null,
        // idVenta:'',
        pagos:null,

        promoInfo:null,
        datosPrecio:null,
        readonly:false,

        // Modal fechas lp
        modalLPC:false,
        modalfechaHotel:false,
        idSelected:'',
        modalClave:false,

        // botones pago
        saveButtonPago:true,
        updateButtonPago:false,

        pagosIndex:null,
        pagoIndex:0,
        suma:0,
    },

    computed:{
        // calculo de total de pagos y restantes.
        totalPagos: function(){
            var sum = 0;

            if (this.pagos) {
              this.pagos.forEach(function(pago) {
                sum += pago.cantidad;

               });
             return sum;
            }
       },

       restante: function(){
            var sum = 0;
            
            if (this.pagos) {
              this.pagos.forEach(function(pago) {
                sum += pago.cantidad;

               });

                var total = this.reserva.precio - sum;
                return total;             
            }
       },

       // precios de reserva

       total:{
        get:function(){
            if (this.datosPrecio) {
            var precio = (this.datosPrecio.precio * 2)
            var sum = (precio * this.dias);

            if (this.reserva.tercerAdulto) {
               var tercer = (this.datosPrecio.tercerAdulto * this.dias);
               sum += tercer; 
            }

            if (this.reserva.menores > 0) {
                if (this.reserva.edad1) {
                    if (this.reserva.edad1 <= this.datosPrecio.menorEdad1) {
                        // alert("entrandodos");
                        var menorUno = (this.datosPrecio.menorPrecio1 * this.dias);
                        sum += menorUno;
                    }
                    else if (this.reserva.edad1 <= this.datosPrecio.menorEdad2) {
                        var menorDos = (this.datosPrecio.menorPrecio2 * this.dias);
                        sum += menorDos;
                    }
                    else if (this.reserva.edad1 <= this.datosPrecio.menorEdad3) {
                        var menorTres = (this.datosPrecio.menorPrecio3 * this.dias);
                        sum += menorTres;
                    }
                }
                // Segundo Menor

                if (this.reserva.edad2) {
                    if (this.reserva.edad2 <= this.datosPrecio.menorEdad1) {
                        var menor2Uno = (this.datosPrecio.menorPrecio1 * this.dias);
                        sum += menor2Uno;
                    }
                    else if (this.reserva.edad2 <= this.datosPrecio.menorEdad2) {
                        var menor2Dos = (this.datosPrecio.menorPrecio2 * this.dias);
                        sum += menor2Dos;
                    }
                    else if (this.reserva.edad2 <= this.datosPrecio.menorEdad3) {
                        var menor2Tres = (this.datosPrecio.menorPrecio3 * this.dias);
                        sum += menor2Tres;
                    }
                }
            }
            this.reserva.precio = sum;
            return sum;
        }


        },
        set:function(){}
       },


    },

    methods: {
        getCompras:function(url){
            this.loading = true;
            var filter = { busqueda: this.busqueda, tipo: this.tipo, fechas:this.fechas };
            var resource = this.$resource(url);
            resource.get(filter).then(function (response) {
                this.pagination = response.data;
                // this.pagination.current_page = 3;
                this.reservas = response.data.data;
                this.loading = false;
            });            
        },

        updateFilters: function (data) {
            this.busqueda = data.busqueda;
            this.tipo = data.tipo;
            this.fechas = data.fechas;
            this.getCompras(this.dataRoute);
        },


        getPagos:function(id){
            this.pago.id_venta = id
            this.panelPagos = true
            this.panelIndex = false
            this.getAllPagos();

            var index = this.findIndexByKeyValue(this.reservas, "id", id);

            this.reserva = {
                id:             this.reservas[index].id,
                nombre:         this.reservas[index].nombre,
                telefono:       this.reservas[index].telefono,
                correo:         this.reservas[index].correo,
                hotel:          this.reservas[index].hotel,             
                fecha_entrada:  this.reservas[index].fecha_entrada,
                fecha_salida:   this.reservas[index].fecha_salida,
                tercerAdulto:   this.reservas[index].tercerAdulto,
                precio:         this.reservas[index].precio,
            }
        },

        getAllPagos:function(){
            this.loading = true;
            var resource = this.$resource("{{route('admin.pagos.get')}}");
            var filter = { id: this.pago.id_venta};
            resource.get(filter).then(function (response) {
                this.pagos = response.data;
                this.loading = false;
            });
        },

        regresar:function(){
            this.panelPagos=false
            this.panelIndex=true
            this.reserva = {id:'', precio:'', tipo:'', nombre:'', telefono:'', correo:'', hotel:'', fecha_entrada:'', fecha_salida:'', tercerAdulto:false, menores:'0', edad1:'', edad2:''};   

        },

        prueba:function(){
            this.modalPago=true
        },

        closeModalPago(){
            this.modalPago = false
            this.pago.cantidad = ''
            this.pago.folio = ''
            this.saveButtonPago=true
            this.updateButtonPago=false
        },

        registrarPago(){
            var form = new FormData();
            form.append('folio',              this.pago.folio);
            form.append('cantidad',           this.pago.cantidad);
            form.append('id_venta',           this.pago.id_venta);

            //Upload Progress Bar
            // const progress = (e) => {
            //     if (e.lengthComputable) {
            //         this.uploadProgress = Math.floor(e.loaded  / e.total * 100);
            //     }
            // }

            var resource = this.$resource("{{route('admin.pagos.set')}}");
            resource.save(form).then(function (response) {
                this.loadingSave = false;
                this.saveInAction = false;
                this.notification('fa fa-check-circle', 'OK!', "Pago Registrado.", 'topCenter');
                // this.cleanErrors();
                // this.closePanelInputs();
                this.closeModalPago();
                this.getAllPagos();
                this.getCompras(this.dataRoute);
                
                // this.uploadProgress = null;
            }, function (response) {                
                this.loadingSave = false;
                this.saveInAction = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.errores = response.data.errors;
                // this.uploadProgress = null;
            });
        },
        // 

        nuevaReserva:function(){

            this.panelIndex = false
            this.panelReserva = true
            this.reserva.tipo="reserva";

            this.loading = true;
            var filter = { busqueda: this.busqueda, tipo: this.tipo };
            var resource = this.$resource("{{route('admin.promociones.get')}}");
            resource.get(filter).then(function (response) {
                this.promoInfo = response.data.data;
                // this.reservas = response.data.data;
                this.loading = false;
            });

        },

        tresAdultos:function(){
            if (this.reserva.tercerAdulto) 
            {
                this.reserva.edad2 = 0;
                this.reserva.menores = 0;
            }
        },

        cuantosMenores:function(){
            if (this.reserva.menores == 0) {
                this.reserva.edad1 = 0;
                this.reserva.edad2 = 0;

            }
            else if(this.reserva.menores == 1) {
                this.reserva.edad2 = 0;

            }
        },

        recuperaValor:function(){
            if (this.readonly == false) {
                this.reserva.precio = this.total;
            }
        },

        regresarReserva:function(){
            var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
            var fecha = (new Date(Date.now() - tzoffset)).toISOString().slice(0,-1).slice(0, 10);
            this.fechas.startDate = fecha;
            this.fechas.endDate   = fecha;
            // this.fechas = "";

            this.$refs.foo.reajustarFecha(fecha,fecha);


            this.datosPrecio=null
            this.panelReserva = false
            this.panelIndex = true
            this.dias = 0
            this.reserva = {id:'', precio:'', tipo:'', nombre:'', telefono:'', correo:'', hotel:'', fecha_entrada:'', fecha_salida:'', tercerAdulto:false, menores:'0', edad1:'', edad2:''};   
            this.erroresReserva=null   
            this.saveButton = true;
            this.updateButton = false;      
        },

        getPreciosHotel: function(id){
            // alert(id);
            var index = this.findIndexByKeyValue(this.promoInfo, "titulo", id);

            this.datosPrecio = {
                
                precio:         this.promoInfo[index].precio,
                tercerAdulto:   this.promoInfo[index].precioTercer,
                menorPrecio1:   this.promoInfo[index].menorPrecio1,
                menorPrecio2:   this.promoInfo[index].menorPrecio2,
                menorPrecio3:   this.promoInfo[index].menorPrecio3,
                menorEdad1:     this.promoInfo[index].menorEdad1,
                menorEdad2:     this.promoInfo[index].menorEdad2,
                menorEdad3:     this.promoInfo[index].menorEdad3,
            }
        },

        setData:function(){
            if (this.reserva.tercerAdulto == false) {
                var valor = 0;
            }
            else{var valor =1;}

            // obtener el dia de hoy
            // var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
            // var fecha = (new Date(Date.now() - tzoffset)).toISOString().slice(0,-1).slice(0, 10);

            // if (this.fechas.endDate != null || this.fechas.endDate !=  fecha) {

            // }

            // else{
            //     this.notification('fa fa-check-circle', 'OK!', "Seleciona bien la fecha", 'topCenter');
            // }

            this.loading = true;
            var form = new FormData();
            form.append('tipo',             this.reserva.tipo);
            form.append('nombre',           this.reserva.nombre);
            form.append('telefono',         this.reserva.telefono);
            form.append('correo',           this.reserva.correo);

            // form.append('titulo',           this.reserva.titulo);
            form.append('hotel',            this.reserva.hotel);

            if (this.fechas != "") {
                form.append('fecha_entrada',    this.fechas.startDate);
                form.append('fecha_salida',     this.fechas.endDate);
            }
            
            form.append('tercerAdulto',     valor);
            form.append('menores',          this.reserva.menores);
            form.append('edad1',            this.reserva.edad1);
            form.append('edad2',            this.reserva.edad2);
            form.append('precio',           this.reserva.precio);

            var resource = this.$resource("{{route('admin.ventas.set')}}");
            resource.save(form).then(function (response) {


                this.notification('fa fa-check-circle', 'OK!', "Reserva agregada correctamente.", 'topCenter');
                this.regresarReserva();
                this.getCompras(this.dataRoute);
                this.loading = false;
            }, function (response) {                
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');

                this.erroresReserva = response.data.errors;
                this.loading = false
            });
            
        },

        borrarRerserva:function(idReserva){
            swal({
                title: '{{trans("cms.are_you_sure")}}',
                text: '{{trans("cms.wont_revert_this")}}',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, borrala!',
                cancelButtonText: '{{trans("cms.cancel")}}'
                }).then(function () {
                    var resource = this.$resource("{{route('admin.ventas.delete')}}");
                    resource.delete({id:idReserva}).then(function (response) {
                        this.notification('fa fa-check-circle', 'OK!', "reserva Borrada!", 'topCenter');
                        this.getCompras(this.dataRoute);
                    }, function (response) {
                        this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                        this.checkExtraErrorMessages(response);
                    });
            }.bind(this)).catch(swal.noop);
        },

        borrarPago: function (idPago){
            swal({
                title: '{{trans("cms.are_you_sure")}}',
                text: '{{trans("cms.wont_revert_this")}}',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, borrala!',
                cancelButtonText: '{{trans("cms.cancel")}}'
                }).then(function () {
                    var resource = this.$resource("{{route('admin.pagos.delete')}}");
                    resource.delete({id:idPago}).then(function (response) {
                        this.notification('fa fa-check-circle', 'OK!', "Pago borrado!", 'topCenter');
                        this.getAllPagos();
                    }, function (response) {
                        this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                        this.checkExtraErrorMessages(response);
                    });
            }.bind(this)).catch(swal.noop);
        },

        loadDatosPago: function(idPago){
            this.modalPago=true
            this.saveButtonPago = false
            this.updateButtonPago= true

            var index = _.findIndex(this.pagos, function(o) { return o.id === idPago; });

            this.pago = {
                id:             this.pagos[index].id,
                id_venta:       this.pagos[index].id_venta,
                folio:          this.pagos[index].folio,
                cantidad:       this.pagos[index].cantidad,
                         
            }

            this.saveButtonPago = false;
            this.updateButtonPago= true;
        },

        updatePago: function () {
            var form = new FormData();
            form.append('id',                 this.pago.id);
            form.append('folio',              this.pago.folio);
            form.append('cantidad',           this.pago.cantidad);
            form.append('id_venta',           this.pago.id_venta);

            //Upload Progress Bar
            // const progress = (e) => {
            //     if (e.lengthComputable) {
            //         this.uploadProgress = Math.floor(e.loaded  / e.total * 100);
            //     }
            // }

            var resource = this.$resource("{{route('admin.pagos.update')}}");
            resource.save(form).then(function (response) {
                this.loadingSave = false;
                this.saveInAction = false;

                this.notification('fa fa-check-circle', 'OK!', "Pago Registrado.", 'topCenter');
                this.saveButtonPago = true;
                this.updateButtonPago= false;
                this.closeModalPago();
                this.getAllPagos();
                
                // this.uploadProgress = null;
            }, function (response) {                
                this.loadingSave = false;
                this.saveInAction = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.saveButtonPago = true;
                this.updateButtonPago= false;
                this.errores = response.data.errors;
                // this.uploadProgress = null;
            });
        },

        checkToday: function(idReserva){
            // this.loading = true;
            var resource = this.$resource("{{route('admin.ventas.confirma')}}");
            resource.save({id:idReserva}).then(function (response) { 
                this.loading = false;
                this.notification('fa fa-check-circle', 'OK!', "Reservacion Confirmada.", 'topCenter');
                // this.getCompras(this.dataRoute);
                // no actualiza la lista solo cambia el valor en la vista
                var index = _.findIndex(this.reservas, function(o) { return o.id === idReserva; });
                this.reservas[index].confirmacion = response.data;

               
            }, function (response) {
                this.loading = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.errores = response.data.errors;
            });
        },

        uncheck:function(idReserva){
            // this.loading = true;
            var resource = this.$resource("{{route('admin.ventas.desconfirma')}}");
            resource.save({id:idReserva}).then(function (response) { 
                this.loading = false;
                this.notification('fa fa-check-circle', 'OK!', "Reservacion Desconfirmada.", 'topCenter');
                // this.getCompras(this.dataRoute);
                var index = _.findIndex(this.reservas, function(o) { return o.id === idReserva; });
                this.reservas[index].confirmacion = response.data.reserva.confirmacion;
               
            }, function (response) {
                this.loading = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.errores = response.data.errors;
            });
        },


        flpc: function(id){
            this.modalLPC = true
            this.idSelected = id
        },

        openModalClave: function(id){
            this.modalClave = true
            this.idSelected = id
        },

        asignarClave: function(){
            if (this.reserva.clave) {
                this.loading = true;
                var resource = this.$resource("{{route('admin.ventas.claveHotel')}}");
                var datos = { id: this.idSelected, clave: this.reserva.clave };
                resource.save(datos).then(function (response) { 
                    this.loading = false;
                    this.modalClave = false;
                    this.notification('fa fa-check-circle', 'OK!', "Clave asignada correctamente", 'topCenter');
                    this.reserva.clave = ''
                    // this.getCompras(this.dataRoute);
                    // console.log(response.data)
                    var index = _.findIndex(this.reservas, function(o) { return o.id === response.data['reserva'].id; });
                    this.reservas[index].clave = response.data['reserva'].clave;
                   
                }, function (response) {
                    this.loading = false;
                    this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                    this.errores = response.data.errors;
                });
            }
            else{
                this.notification('fa fa-check-circle', '¿clave?', "Ingrese la Clave", 'topCenter');
            }
        },

        registrarLPC:function(){
            if (this.reserva.limite_pago_cliente) {
                // this.loading = true;
                var resource = this.$resource("{{route('admin.ventas.lpc')}}");
                var datos = { id: this.idSelected, fecha: this.reserva.limite_pago_cliente };
                resource.save(datos).then(function (response) { 
                    this.loading = false;
                    this.modalLPC = false;
                    this.notification('fa fa-check-circle', 'OK!', "Limite pago del cliente confirmado.", 'topCenter');
                    // this.getCompras(this.dataRoute);
                    var index = _.findIndex(this.reservas, function(o) { return o.id === response.data['reserva'].id; });
                    this.reservas[index].limite_pago_cliente = response.data['reserva'].limite_pago_cliente;
                    this.reserva.limite_pago_cliente = ''

                   
                }, function (response) {
                    this.loading = false;
                    this.reserva.limite_pago_cliente = ''
                    this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                    this.errores = response.data.errors;
                });
            }
            else{
                this.notification('fa fa-check-circle', 'OK!', "Seleccione la fecha", 'topCenter');
            }
        },

        fechaLimiteHotel: function(id){            
            this.idSelected = id;
            this.modalfechaHotel = true
            console.log(this.modalfechaHotel);
        },

        registrarLPH:function(){
            if (this.reserva.limite_pago_hotel) {
                this.loading = true;
                var resource = this.$resource("{{route('admin.ventas.lph')}}");
                var datos = { id: this.idSelected, fecha: this.reserva.limite_pago_hotel};
                resource.save(datos).then(function (response) { 
                    this.loading = false;
                    this.modalfechaHotel = false;
                    this.notification('fa fa-check-circle', 'OK!', "Limite pago del cliente confirmado.", 'topCenter');
                    // this.getCompras(this.dataRoute);
                    // proof
                    var index = _.findIndex(this.reservas, function(o) { return o.id === response.data['reserva'].id; });
                    this.reservas[index].limite_pago_hotel = response.data['reserva'].limite_pago_hotel;
                    this.reserva.limite_pago_hotel = ''
                   
                }, function (response) {
                    this.loading = false;
                    this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                    this.errores = response.data.errors;
                });
            }
            else{
                this.notification('fa fa-check-circle', 'OK!', "Seleccione la fecha", 'topCenter');
            }
        },

        pagarHotel:function (id, action){
            // this.loading = true;
            var resource = this.$resource("{{route('admin.ventas.pagarHotel')}}");
            var datos = { id: id, action:action};
            resource.save(datos).then(function (response) { 
                this.loading = false;
                console.log(response.data)
                if (action == 'set') {
                this.notification('fa fa-check-circle', 'OK!', "Pago de hotel confirmado.", 'topCenter');
                }
                else{
                    this.notification('fa fa-check-circle', 'OK!', "Pago de hotel eliminado.", 'topCenter');
                }
                // this.getCompras(this.dataRoute);
                var index = _.findIndex(this.reservas, function(o) { return o.id === response.data['reserva'].id; });
                this.reservas[index].pagado = response.data['reserva'].pagado;
               
            }, function (response) {
                this.loading = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.errores = response.data.errors;
            });
        },

        loadUpdatePanel:function(idReserva){
            var index = _.findIndex(this.reservas, function(o) { return o.id === idReserva; });

            this.reserva = {
                id:             this.reservas[index].id,
                tipo:           this.reservas[index].tipo,
                nombre:         this.reservas[index].nombre,
                telefono:       this.reservas[index].telefono,
                correo:         this.reservas[index].correo,              

                // form.append('fecha_entrada',    this.fechas.startDate);
                // form.append('fecha_salida',     this.fechas.endDate);

                hotel:          this.reservas[index].hotel,
                tercerAdulto:   this.reservas[index].tercerAdulto,
                menores:        this.reservas[index].menores,
                edad1:          this.reservas[index].edad1,
                edad2:          this.reservas[index].edad2,
                precio:         this.reservas[index].precio,
                

            }
            // Asignar las fechas a fechas.starDate y endDate
            this.fechas = {startDate:this.reservas[index].fecha_entrada, endDate:this.reservas[index].fecha_salida}


            this.saveButton = false;
            this.updateButton = true;
            this.panelIndex = false;
            this.panelReserva = true;

        },

        updateData:function(){
            if (this.reserva.tercerAdulto == false) {
                var valor = 0;
            }
            else{var valor =1;}

            this.loading = true;
            var form = new FormData();
            form.append('id',               this.reserva.id);
            form.append('tipo',             this.reserva.tipo);
            form.append('nombre',           this.reserva.nombre);
            form.append('telefono',         this.reserva.telefono);
            form.append('correo',           this.reserva.correo);

            form.append('hotel',            this.reserva.hotel);
            form.append('fecha_entrada',    this.fechas.startDate);
            form.append('fecha_salida',     this.fechas.endDate);
            form.append('tercerAdulto',     valor);
            form.append('menores',          this.reserva.menores);
            form.append('edad1',            this.reserva.edad1);
            form.append('edad2',            this.reserva.edad2);
            form.append('precio',           this.reserva.precio);

            var resource = this.$resource("{{route('admin.ventas.update')}}");
            resource.save(form).then(function (response) {


                this.notification('fa fa-check-circle', 'OK!', "Reserva Actualizada correctamente.", 'topCenter');
                this.regresarReserva();
                this.getCompras(this.dataRoute);
                this.loading = false;
            }, function (response) {                
                // this.loadingSave = false;
                // this.saveInAction = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.erroresReserva = response.data.errors;
                this.loading = false;
                // this.uploadProgress = null;
            });
            
        },

        excelParameters:function(){
            this.loading = true;
            var busqueda = this.$refs.searching.busqueda;
            var tipo = this.tipo;
            var fechaz = this.fechas;
            if (fechaz == "") {
                var start = "";
                var end   = "";
            }

            else{
                var start = fechaz.startDate;
                var end   = fechaz.endDate;
            }

            if (busqueda != "") {
                window.location = "{{URL::to('admin/ventas/excel/parametro')}}/"+busqueda+"/"+tipo;
            }
            else if(start != "" || end != ""){
                window.location = "{{URL::to('admin/ventas/excel/parametroFecha')}}/"+start+"/"+end;
            }

            else if(busqueda !="" || start != "" || end != "")
            {   
                window.location = "{{URL::to('admin/ventas/excel/parametroBusquedaFecha')}}/"+busqueda+"/"+tipo+"/"+start+"/"+end;
            }
            else{

                this.notification('fa fa-exclamation-triangle', 'Error', "Faltan Parametros", 'topCenter');
            }
            
            this.loading = false;
        },

    }
});
</script>