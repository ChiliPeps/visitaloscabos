@extends('cms.master')

{{-- <style>
    .cargando{
        width: 100%;
        height:100%;
        position:absolute;
        background:rgba(255,255,255,0.7);
        left:0;
        z-index: 1;
    }
</style> --}}


@section('content')
    <section class="content-header">
        <h1><i class="fa Example of globe fa-globe"></i> Ventas en Linea</h1>
    </section>

    <section class="content" id="app" v-cloak>
        <div class="box box-primary" v-show="panelIndex">

            <div class="box-header">
                <h3 class="box-title"></h3>
                <div class="box-tools pull-right">
                    <a href="#" class="btn bg-navy" @click="nuevaReserva()">
                    <i class="fa fa-plus-circle"></i> CREAR RESERVA</a>
                </div>

                {{-- <filtersearch :selected="tipo" :options="tipos" @updatefilters="updateFilters"></filtersearch> --}}

                <br>
                {{-- <daterangepicker></daterangepicker>  --}}
                <br>
                
                <datefiltersearch :selected="tipo" :options="tipos" @updatefilters="updateFilters" ref="searching"></datefiltersearch>

                <br>
                <button class="btn btn-xs btn-warning" @click="excelParameters()"
                    style="margin-bottom: 10px;">Exportar Excel</button>
            </div>

            <div class="box-body">

                <!-- VueLoading icon -->
                {{-- <div class="text-center"><i v-show="loading" class="fa fa-spinner fa-spin fa-5x"></i></div> --}}

                <div class="text-center cargando" v-show="loading">
                    <i v-show="loading" class="fa fa-spinner fa-spin fa-5x"></i>
                </div>


                <div class="table-responsive" v-show="!loading">
                <table class="table table-bordered {{-- table-hover --}}">
                    <thead>
                        <tr>
                            <th>Folio</th>
                            <th>Reserva Central</th>
                            <th colspan="1">Nombre</th>
                            <th>Tipo</th>
                            <th>Menores</th>
                            <th>Hotel</th>
                            {{-- <th>Entrada/Salida</th> --}}
                            <th>LP Cliente</th>
                            <th>LP Hotel</th>
                            <th>Precio</th>
                            <th>Pagos</th>
                            <th></th>
                            <th>Clave Hotel</th>
                            <th>Pagado a Hotel</th>


                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="r in reservas" v-bind:class="[r.precio == r.sumaPagos ? 'pagado' : 'unCheck']">
                            <td>@{{r.folio}} </td>
                            <td v-if="r.confirmacion">
                                <button class="btn btn-xs btn-danger" @click="uncheck(r.id)"
                                style="margin-bottom: 10px;">@{{r.confirmacion}}</button> 
                            </td>

                            <td v-else> <button class="btn btn-xs btn-warning" @click="checkToday(r.id)"
                                style="margin-bottom: 10px;">check</button>
                            </td>

                            <td class="firstCol" colspan="1">
                                <div class="flex-test">
                                    <div class="">
                                        <div  style="color: #3c8dbc; font-weight: bold;">
                                            @{{r.nombre}}
                                        </div>

                                    </div>

                                    {{-- <div style="padding-left: 15px; ">
                                        <div style="color: #179821; font-weight: normal;" >@{{r.correo}}</div>
                                        <div>@{{r.telefono}}</div>
                                    </div> --}}
                                </div>
                            </td>
                           <td nowrap="nowrap" style="color:black; font-weight:bold; width:auto; ">@{{r.tipo}}</td>

                           <td class="" >
                                <div class="flex-test">
                                    <div class="">
                                        <div  style="color: #3c8dbc; font-weight: bold;">
                                            @{{r.menores}}
                                        </div>
                                    </div>

                                    <div style="padding-left: 15px; ">
                                        <div v-if="r.edad1" style="color: #179821; font-weight: bold;" >(@{{r.edad1}})</div>
                                        <div v-if="r.edad2" style="color: #179821; font-weight: bold;"> (@{{r.edad2}})</div>
                                    </div>
                                </div>
                            </td>

                           {{-- <td>@{{r.correo}}</td> --}}
                           <td>@{{r.hotel}} <br> <div style="color: #3c8dbc; font-weight: bold;" >@{{r.fecha_entrada}} /--/ @{{r.fecha_salida}} </div></td>

                            <td class = "text-center">
                                @{{r.limite_pago_cliente}} <br>
                                <button class="btn btn-xs btn-info" @click="flpc(r.id)" style="margin-bottom: 10px;">FLPC</button>
                                
                            </td>

                            <td class = "text-center">
                                @{{r.limite_pago_hotel}} <br>
                                <button class="btn btn-xs btn-info" @click="fechaLimiteHotel(r.id)" style="margin-bottom: 10px;">FLPH</button>
                            </td>
                           <td>@{{r.precio}}</td>
                           <td >
                               <button class="btn btn-xs btn-success" @click="getPagos(r.id)" 
                                style="margin-bottom: 10px;" title="Pagos">Pagos</button>
                                {{-- <p v-for ="c in r.detalle">
                                    pago: @{{c.cantidad}}

                                </p> --}}
                                <p v-if="r.c">Pagado</p>
                                {{-- <br>total:
                                @{{r.sumaPagos}} --}}

                               
                           </td>

                           <td nowrap="nowrap">
                                <button class="btn btn-xs btn-warning" @click="loadUpdatePanel(r.id)"
                                        style="" title="Actualizar Pago"><i class="fa fa-refresh"></i></button>

                                <button class="btn btn-xs btn-danger" @click="borrarRerserva(r.id)" 
                                        style="" title="Borrar Reserva"><i class="fa fa-trash"></i></button>
                           </td>
                          <td >
                                @{{r.clave}} <br>
                                <button class="btn btn-xs btn-warning" @click="openModalClave(r.id)"
                                style="margin-bottom: 10px;">Asignar Clave</button>
                          </td>
                          <td v-if="r.pagado == null">
                              <button class="btn btn-xs btn-success" @click="pagarHotel(r.id, 'set')"
                                style="margin-bottom: 10px;">Pago a hotel</button>
                          </td>
                          <td v-else>
                            <div class="text-center">
                                <i class="fa fa-check-square-o"></i>   
                                <br>                     
                                @{{r.pagado}}
                                <button class="btn btn-xs btn-warning" @click="pagarHotel(r.id, 'restore')"><i class="fa fa-refresh"></i></button>
                            </div>                            
                          </td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </div>

            <div class="box-footer clearfix">
                <pagination @setpage="getCompras" :param="pagination"></pagination>
            </div>
            
        </div>

        <div v-show="panelReserva">
            @include('cms.ventas.partials.inputs')
        </div>

        <div v-show="panelPagos">
            @include('cms.ventas.partials.pagos')
        </div>

        {{-- Limite de pago cliente --}}
        <modal v-if="modalLPC" @close="modalLPC = false" {{-- v-show="!hideTagsModal" --}}>
            <div slot="head">Selecciona la fecha</div>
            <div slot="body">
                <div class="form-group">
                    <label>Selecciona la fecha limite de pago Cliente</label>
                    <datepicker v-model="reserva.limite_pago_cliente"></datepicker>
                </div>            

                <div slot="footer">
                    <div class="text-right"> 
                       {{-- <a class="modal-default-button btn bg-navy" @click="closeModalPago()"><i class="fa fa-times-circle"></i> Cerrar</a> --}}
                       <a class="modal-default-button btn bg-green" @click="registrarLPC()"><i class="fa fa-times-circle"></i> Registrar </a>    
                    </div>
                </div>
            </div>
        </modal>

        {{--  --}}
        <modal v-if="modalfechaHotel" @close="modalfechaHotel = false" {{-- v-show="!hideTagsModal" --}}>
            <div slot="head">Selecciona la fecha</div>
            <div slot="body">
                <div class="form-group">
                    <label>Selecciona la fecha limite de pago Hotel</label>
                    <datepicker v-model="reserva.limite_pago_hotel"></datepicker>
                </div>
            

                <div slot="footer">
                    <div class="text-right"> 
                       {{-- <a class="modal-default-button btn bg-navy" @click="closeModalPago()"><i class="fa fa-times-circle"></i> Cerrar</a> --}}
                       <a class="modal-default-button btn bg-green" @click="registrarLPH()"><i class="fa fa-times-circle"></i> Registrar </a>    
                    </div>
                </div>
            </div>
        </modal>

        {{-- Modal Clave hotel --}}

        <modal v-if="modalClave" @close="modalClave = false" {{-- v-show="!hideTagsModal" --}}>
            <div slot="head">Escribe la clave</div>
            <div slot="body">
                <div class="form-group">
                    <label>Escriba la clave proporcionada por el hotel</label>
                    <input type="text" class="form-control" 
                            placeholder="Clave" v-model="reserva.clave" 
                            :class="formError(errores, 'clave', 'inputError')"/>
                </div>            

                <div slot="footer">
                    <div class="text-right"> 
                       {{-- <a class="modal-default-button btn bg-navy" @click="closeModalPago()"><i class="fa fa-times-circle"></i> Cerrar</a> --}}
                       <a class="modal-default-button btn bg-green" @click="asignarClave"> <i class="fa fa-times-circle"></i> Registrar </a>    
                    </div>
                </div>
            </div>
        </modal>

    </section>

@endsection

@push('scripts')
    @include('cms.ventas.partials.scripts')
@endpush