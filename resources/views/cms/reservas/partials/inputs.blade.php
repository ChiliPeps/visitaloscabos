<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title" v-if="saveButton"> Crear Reserva</h3>
        <h3 class="box-title" v-if="updateButton"> Actualizar Reserva</h3>
        <div class="box-tools pull-right">
{{--             @if(Auth::guard('cms')->user()->type != "reportero")
            <a class="btn bg-orange btn-sm" @click="aprobarNoticia" v-if="noticia.aprobada == 0"><i class="fa fa-thumbs-o-up"></i> Aprobar Noticia</a>
            @endif --}}
            <a class="btn bg-navy btn-sm" @click="regresarReserva"><i class="fa fa-chevron-left"></i> Regresar</a>
            <a class="btn btn-success btn-sm" v-show="saveButton" @click="setData()"><i class="fa fa-floppy-o"></i> 
            Guardar</a>
            <a class="btn btn-success btn-sm" v-show="updateButton" @click="updateData()"><i class="fa fa-floppy-o"></i> Actualizar</a>
        </div>
    </div>
</div>

{{-- Form Errros --}}
<formerrors :errorsbag="erroresReserva"></formerrors>

    {{-- <br> --}}
        <div class="row">
            <div class="col-md-6">
        
                <div class="box box-primary">
                    <div class="box-body">
                        
                        
                        {{-- titulo --}}
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" class="form-control form-control-xs" 
                            placeholder="Nombre" v-model="reserva.nombre" 
                            :class="formError(errores, 'nombre', 'inputError')">
                        </div>

                        <div class="form-group">
                            <label>Telefono</label>
                            <input type="text" class="form-control"  v-model="reserva.telefono"  
                            placeholder="612 14 12345" @keypress="isNumber($event)" >
                        </div>

                        <div class="form-group">
                            <label>Correo</label>
                            <input type="text" class="form-control"  v-model="reserva.correo"  
                            placeholder="Correo">
                        </div>

                        <div class="form-group">
                            <label>Habitaciones:</label>
                            {{-- <br> --}}
                            <button class="btn bg-navy btn-sm" @click="habitacionMenos">
                                <i class="fa fa-minus"></i>
                            </button>
                            <input type="number" id="quantity" name="quantity" min="1" max="4" v-model="reserva.habitaciones" readonly> 
                            <button class="btn bg-navy btn-sm" @click="habitacionMas">
                                <i class="fa fa-plus"></i> 
                            </button>
              
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3" v-for="(h, index) in habitaciones" :key="index">
                        <div class="box box-primary text-center">
                            <p>Habitacion @{{index + 1}}</p>
                            <button class="btn btn-danger btn-xs" @click="deleteRooms(index)">X</button>
                            <div class="box-header ">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" class="form-control" 
                                    placeholder="Nombre" v-model="habitaciones[index].nombre" 
                                    :class="formError(errores, 'nombre', 'inputError')">
                                </div>
                            </div>
                            <div class="box-header">
                                <h4>Adultos</h4>
                            </div>

                            <div class="box-body">
                                <div class="form-group" style="display: inline-flex;">
                                    <button class="btn btn-success btn-xs" @click="menosAdultos(index)">
                                        <i class="fa fa-minus"></i>
                                    </button>

                                    <h5 class="mx-5" v-text="habitaciones[index].adultos"></h5>

                                    <button class="btn btn-success btn-xs" @click="habitaciones[index].adultos += 1">
                                        <i class="fa fa-plus"></i> 
                                    </button>
                      
                                </div>
                                {{-- <div class="input-group">


                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-success"
                                        @click="habitaciones[index].adultos -= 1"> - </button>
                                    </div>

                                    <input type="number" disabled class="form-control" v-model="habitaciones[index].adultos" >

                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-success btn-flat" 
                                        @click="habitaciones[index].adultos += 1">+</button>
                                    </span>
                                </div> --}}
                            </div>
                            <div class="box-header text-center">
                                <h4>Menores </h4>
                            </div>
                            <div class="box-body">
                                {{-- <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-info" 
                                        @click="menosMenores(index)"> - </button>
                                    </div>

                                    <input type="number" disabled class="form-control form-control-xs" v-model="habitaciones[index].menores">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-info btn-flat btn-xs" 
                                        @click="habitaciones[index].menores += 1">+</button>
                                    </span>
                                </div> --}}

                                <div class="form-group" style="display: inline-flex;">
                                    <button class="btn btn-info btn-xs" @click="menosMenores(index)">
                                        <i class="fa fa-minus"></i>
                                    </button>

                                    <h5 class="mx-5" v-text="habitaciones[index].menores"></h5>

                                    <button class="btn btn-info btn-xs" @click="masMenores(index)">
                                        <i class="fa fa-plus"></i> 
                                    </button>
                      
                                </div>

                                {{--  --}}

                                <div class="form-group" v-if="habitaciones[index].menores > 0">
                                    <div class="row">

                                        <div class="col-md-6"> 
                                            <label>Edad 1: </label>
                                        </div>

                                        <div class="col-md-6">
                                            <select class="edades" v-model="habitaciones[index].edad1">
                                                <option :value="n" v-for="n in 17" >@{{n}}</option>
                                            </select>
                                        </div>

                                    </div>
                      
                                </div>

                                <div class="form-group" v-if="habitaciones[index].menores > 1">
                                    <div class="row">

                                        <div class="col-md-6"> 
                                            <label>Edad 2: </label>
                                        </div>

                                        <div class="col-md-6">
                                            <select class="edades" v-model="habitaciones[index].edad2">
                                                <option :value="n" v-for="n in 17" >@{{n}}</option>
                                            </select>
                                        </div>

                                    </div>
                      
                                </div>
                                <div class="form-group" v-if="habitaciones[index].menores > 2">
                                    <div class="row">

                                        <div class="col-md-6"> 
                                            <label>Edad 3: </label>
                                        </div>

                                        <div class="col-md-6">
                                            <select class="edades" v-model="habitaciones[index].edad3">
                                                <option :value="n" v-for="n in 17" >@{{n}}</option>
                                            </select>
                                        </div>

                                    </div>
                      
                                </div>

                            </div>      
                        </div>
                    </div>
                </div>
        
            </div>

            {{-- Manuales --}}
            <div class="col-md-6">
                <div class="box box-danger">
                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Hotel/Servicio:</label>
                                    <input type="text" class="form-control" 
                                    placeholder="Hotel" v-model="reserva.hotel">
                            
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tarifa:</label>
                                    <input type="text" class="form-control" 
                                    placeholder="Tarifa" v-model="reserva.tarifa">
                            
                                </div>
                            </div>
                        </div>
                        

                        <div class="form-group">
                            <label>Fechas</label>
                            <daterangepicker v-model="fechas" ref="foo"></daterangepicker>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Precio</label>
                                    <input type="text" class="form-control" v-model="reserva.precio"> 
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" >
                                    <label>Vendedor 
                                    </label>
                                    <select class="form-control" v-model="reserva.id_vendedor" 
                                    :class="formError(errores, 'id_vendedor', 'inputError')">
                                        <option value="" selected disabled>Selecciona al vendedor</option>
                                        <option v-for="v in vendedores" :value="v.id">@{{v.nombre}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" >
                                    <label>Medio de contacto </label>
                                    <select class="form-control" v-model="reserva.medio_contacto" 
                                    :class="formError(errores, 'medio_contacto', 'inputError')">
                                       <option value="" selected disabled>Medio de contacto</option>
                                        <option value="facebook pagina">Facebook página</option>
                                        <option value="facebook grupo">Facebook grupo</option>
                                        <option value="walkin">Walkin</option>
                                        <option value="cambaceo">Cambaceo</option>
                                        <option value="cambaceo oficina">Cambaceo oficina</option>
                                        <option value="diferido">Diferido</option>
                                        <option value="cliente referido">Cliente referido</option>
                                        <option value="cliente frecuente">Cliente Frecuente</option>
                                        <option value="cliente google">Cliente Google</option>
                                        <option value="cliente pagina">Cliente Página</option>
                                        
                                        
                                        
                                        
                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                        

                        <div class="form-group">
                            <label>Observaciones</label><br>
                            <textarea name="" id="" cols="30" rows="10"  v-model="reserva.observaciones"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
</vue-tabs>