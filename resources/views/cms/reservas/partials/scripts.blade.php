<!-- Vue Components & Mixins -->
@include('components.vue.vuePagination')
@include('components.vue.vueHelperFunctions')
@include('components.vue.vueFormErrors')

@include('components.vue.vueNotifications')
@include('components.vue.vueDatepicker')
@include('components.vue.vueDateRangePicker')
@include('components.vue.vueModal')
@include('components.vue.vueFilterSearch')

@include('components.vue.vueDateFilterSearch2')
@include('components.vue.vueDateFilterSearch')
<!-- Vue Components & Mixins -->

<script>
//Laravel's Token
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

var app = new Vue ({
    mounted: function () {
    	this.getCompras(this.dataRoute), this.getVendedores();
    },

    el: "#app",

    mixins: [helperFunctions, notifications],

    data: {
        // inputs 
        panelReserva:false,
        saveButton:true,
        updateButton:false,

        //FilterSearch
        tipo:'nombre',
        tipos:[
            { text: 'Nombre', value:'nombre' },
            { text: 'Nombre en Habitacion', value:'nombreHabitacion' },
            { text: 'Folio', value:'folio' },
            { text: 'Hotel', value:'hotel' },
            { text:'Fecha entrada', value:'fecha_entrada'},
            { text:'Limite de pago/Cliente', value:'limite_pago_cliente'},
            { text:'Limite de pago Hotel', value:'limite_pago_hotel'}
        ],
        // filterSearch2 fechas
        tipo_fecha:'fecha_entrada',
        tipos_fecha:[
            { text: 'Entrada-Salida', value:'fecha_entrada' },
            { text: 'Limite Cliente', value:'limite_pago_cliente' },
            { text: 'Limite Hotel', value:'limite_pago_hotel' }
        ],
        // 
        fechas:'',

        reserva:{id:'', id_vendedor:'', habitaciones:0, nombre:'', telefono:'', correo:'', hotel:'', fecha_entrada:'', fecha_salida:'', precio:'', limite_pago_cliente:'', limite_pago_hotel:'', clave:'', pagado:'', observaciones:'', tarifa:'', medio_contacto:''}, 

        habitacion:{id:'', nombre:'', adultos:0, menores:0, edad1:'', edad2:'', edad3:''},
        habitaciones:[],
        dias:'',

        panelIndex:true,
        panelPagos:false,
        panelCreate:false,

    	loading:false,
    	reservas:null,
    	pagination:null,
    	dataRoute: "{{route('admin.reservas.get')}}",

        modalPago:false,

        pago:{id:'', id_reserva:'', cantidad:'', folio:'', nombreUser:'', forma_pago:'', identificador:''},

        errores:null,
        erroresReserva:null,
  
        pagos:null,
        pagosSuma:'',
        // FilterSearch Pagos
        tipoPago:'identificador',
        tiposPago:[
            { text: 'Identificador', value:'identificador' },
            { text: 'Forma de pago', value:'forma_pago' },
            { text: 'Cantidad', value:'cantidad' },
        ],
        dataRoutePagos:"{{route('admin.pagosReserva.get')}}",


        promoInfo:null,
        datosPrecio:null,
        readonly:false,

        // Modal fechas lp
        modalLPC:false,
        modalfechaHotel:false,
        idSelected:'',
        modalClave:false,

        // botones pago
        saveButtonPago:true,
        updateButtonPago:false,

        pagosIndex:null,
        pagoIndex:0,
        suma:0,

        // vendedores
        vendedores:null,

        selectedReservas:[]
    },
    watch:{

    },

    computed:{
        // calculo de total de pagos y restantes.
        totalPagos: function(){
            var sum = 0;

            if (this.pagos) {
              this.pagos.forEach(function(pago) {
                sum += pago.cantidad;

               });
             return sum;
            }
       },

       restante: function(){
            var sum = 0;
            
            if (this.pagos) {
              this.pagos.forEach(function(pago) {
                sum += pago.cantidad;

               });

                var total = this.reserva.precio - sum;
                return total;             
            }
       },

    },

    methods: {
    	getCompras:function(url){
    		this.loading = true;
            var filter = { busqueda: this.busqueda, tipo: this.tipo, fechas:this.fechas, tipo_fecha:this.tipo_fecha};
            var resource = this.$resource(url);
            resource.get(filter).then(function (response) {
                this.pagination = response.data;
                this.reservas = response.data.data;
                this.loading = false;
            });            
    	},

        updateFilters: function (data) {
            this.busqueda = data.busqueda;
            this.tipo = data.tipo;
            this.fechas = data.fechas;
            this.tipo_fecha = data.tipo_fecha;
            this.getCompras(this.dataRoute);
        },


        getPagos:function(id){
            this.pago.id_reserva = id
            this.panelPagos = true
            this.panelIndex = false
            this.getAllPagos();

            var index = this.findIndexByKeyValue(this.reservas, "id", id);

            this.reserva = {
                id:             this.reservas[index].id,
                nombre:         this.reservas[index].nombre,
                telefono:       this.reservas[index].telefono,
                correo:         this.reservas[index].correo,
                hotel:          this.reservas[index].hotel,             
                fecha_entrada:  this.reservas[index].fecha_entrada,
                fecha_salida:   this.reservas[index].fecha_salida,
                precio:         this.reservas[index].precio,
                habitaciones:   this.reservas[index].habitaciones,
                habitacion_info:this.reservas[index].habitacion_info,

            }
        },

        getAllPagos:function(){
            this.loading = true;
            var resource = this.$resource(this.dataRoutePagos);
            var filter = {busqueda: this.busquedaPago, tipo: this.tipoPago, id: this.pago.id_reserva};
            // var filter = { };
            resource.get(filter).then(function (response) {
                // console.log(response.data.suma);
                this.pagos = response.data.results;
                this.pagosSuma = response.data.suma;
                this.loading = false;
            });
        },

        updateFiltersPagos: function (data) {
            this.busquedaPago = data.busqueda;
            this.tipoPago = data.tipo;
            this.getAllPagos(this.dataRoutePagos);
        },

        regresar:function(){
            this.panelPagos=false
            this.panelIndex=true
            // this.reserva={id:'', habitaciones:0, nombre:'', telefono:'', correo:'', hotel:'', fecha_entrada:'', fecha_salida:'', precio:'', limite_pago_cliente:'', limite_pago_hotel:'', clave:'', pagado:'', observaciones:'', tarifa:''};

        },

        prueba:function(){
            this.modalPago=true
        },

        closeModalPago(){
            this.modalPago = false
            this.pago.cantidad = ''
            this.pago.folio = ''
            this.pago.identificador = ''
            this.pago.forma_pago=''
            this.saveButtonPago=true
            this.updateButtonPago=false
        },

        registrarPago(){
            var form = new FormData();
            form.append('folio',              this.pago.folio);
            form.append('cantidad',           this.pago.cantidad);
            form.append('id_reserva',         this.pago.id_reserva);
            form.append('forma_pago',         this.pago.forma_pago);
            form.append('identificador',      this.pago.identificador);
            //Upload Progress Bar
            // const progress = (e) => {
            //     if (e.lengthComputable) {
            //         this.uploadProgress = Math.floor(e.loaded  / e.total * 100);
            //     }
            // }

            var resource = this.$resource("{{route('admin.pagosReserva.set')}}");
            resource.save(form).then(function (response) {
                this.loadingSave = false;
                this.saveInAction = false;
                this.notification('fa fa-check-circle', 'OK!', "Pago Registrado.", 'topCenter');
                // this.cleanErrors();
                // this.closePanelInputs();
                this.closeModalPago();
                this.getAllPagos();
                this.getCompras(this.dataRoute);
                
                // this.uploadProgress = null;
            }, function (response) {                
                this.loadingSave = false;
                this.saveInAction = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.errores = response.data.errors;
                // this.uploadProgress = null;
            });
        },
        // 

        nuevaReserva:function(){

            this.panelIndex = false
            this.panelReserva = true
            // this.reserva.tipo="reserva";

            this.loading = true;
            var filter = { busqueda: this.busqueda, tipo: this.tipo };
            var resource = this.$resource("{{route('admin.promociones.get')}}");
            resource.get(filter).then(function (response) {
                this.promoInfo = response.data.data;
                // this.reservas = response.data.data;
                this.loading = false;
            });

        },

        tresAdultos:function(){
            if (this.reserva.tercerAdulto) 
            {
                this.reserva.edad2 = 0;
                this.reserva.menores = 0;
            }
        },

        cuantosMenores:function(){
            if (this.reserva.menores == 0) {
                this.reserva.edad1 = 0;
                this.reserva.edad2 = 0;

            }
            else if(this.reserva.menores == 1) {
                this.reserva.edad2 = 0;

            }
        },

        recuperaValor:function(){
            if (this.readonly == false) {
                this.reserva.precio = this.total;
            }
        },

        regresarReserva:function(){
            var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
            var fecha = (new Date(Date.now() - tzoffset)).toISOString().slice(0,-1).slice(0, 10);
            this.fechas.startDate = fecha;
            this.fechas.endDate   = fecha;
            // this.fechas = "";

            this.$refs.foo.reajustarFecha(fecha,fecha);


            this.datosPrecio=null
            this.panelReserva = false
            this.panelIndex = true
            this.dias = 0
            this.reserva={id:'', id_vendedor:'', habitaciones:0, nombre:'', telefono:'', correo:'', hotel:'', fecha_entrada:'', fecha_salida:'', precio:'', limite_pago_cliente:'', limite_pago_hotel:'', clave:'', pagado:'', observaciones:'', tarifa:'', medio_contacto:''};

            this.erroresReserva=null   
            this.saveButton = true;
            this.updateButton = false;      
            this.habitaciones=[]
        },

        setData:function(){
            if (this.habitaciones.length == 0)
            {                 
               this.notification('fa fa-exclamation-triangle', 'Error', "Registra almenos 1 habitacion", 'topCenter');  
               return                             
            }
            
            else{
                var existe;
                this.habitaciones.forEach(function(habitacion, index) {
                    if (habitacion.nombre == "" || habitacion.adultos == 0) {existe=true;}
                });

               if (existe) {
                this.notification('fa fa-exclamation-triangle', 'Error', "Nombre y adulto en habitacion es requerido", 'topCenter');
                return
               }
            }

            this.loading = true;
            var form = new FormData();
            form.append('nombre',           this.reserva.nombre);
            form.append('telefono',         this.reserva.telefono);
            form.append('correo',           this.reserva.correo);

            form.append('hotel',            this.reserva.hotel);

            if (this.fechas != "") {
                form.append('fecha_entrada',    this.fechas.startDate);
                form.append('fecha_salida',     this.fechas.endDate);
            }
            form.append('habitaciones',     this.reserva.habitaciones);
            form.append('precio',           this.reserva.precio);
            form.append('infoHabitaciones', JSON.stringify(this.habitaciones));
            form.append('observaciones',    this.reserva.observaciones);
            form.append('tarifa' ,          this.reserva.tarifa);

            form.append('id_vendedor' ,     this.reserva.id_vendedor);
            // medioDeContacto
            form.append('medio_contacto',   this.reserva.medio_contacto);


            var resource = this.$resource("{{route('admin.reserva.set')}}");
            resource.save(form).then(function (response) {

                this.notification('fa fa-check-circle', 'OK!', "Reserva agregada correctamente.", 'topCenter');
                this.regresarReserva();
                this.getCompras(this.dataRoute);
                this.loading = false;
            }, function (response) {                
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                console.log(response);
                this.erroresReserva = response.data.errors;
                this.loading = false
            });
            
        },

        borrarRerserva:function(idReserva){
            swal({
                title: '{{trans("cms.are_you_sure")}}',
                text: '{{trans("cms.wont_revert_this")}}',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, borrala!',
                cancelButtonText: '{{trans("cms.cancel")}}'
                }).then(function () {
                    var resource = this.$resource("{{route('admin.reserva.delete')}}");
                    resource.delete({id:idReserva}).then(function (response) {
                        this.notification('fa fa-check-circle', 'OK!', "reserva Borrada!", 'topCenter');
                        this.getCompras(this.dataRoute);
                    }, function (response) {
                        this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                        this.checkExtraErrorMessages(response);
                    });
            }.bind(this)).catch(swal.noop);
        },

        borrarPago: function (idPago){
            swal({
                title: '{{trans("cms.are_you_sure")}}',
                text: '{{trans("cms.wont_revert_this")}}',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, borrala!',
                cancelButtonText: '{{trans("cms.cancel")}}'
                }).then(function () {
                    var resource = this.$resource("{{route('admin.pagosReserva.delete')}}");
                    resource.delete({id:idPago}).then(function (response) {
                        this.notification('fa fa-check-circle', 'OK!', "Pago borrado!", 'topCenter');
                        this.getAllPagos();
                    }, function (response) {
                        this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                        this.checkExtraErrorMessages(response);
                    });
            }.bind(this)).catch(swal.noop);
        },

        loadDatosPago: function(idPago){
            this.modalPago=true
            this.saveButtonPago = false
            this.updateButtonPago= true

            var index = _.findIndex(this.pagos, function(o) { return o.id === idPago; });

            this.pago = {
                id:             this.pagos[index].id,
                id_reserva:     this.pagos[index].id_reserva,
                folio:          this.pagos[index].folio,
                cantidad:       this.pagos[index].cantidad,
                forma_pago:     this.pagos[index].forma_pago,
                identificador:  this.pagos[index].identificador,                         
            }
            this.saveButtonPago = false;
            this.updateButtonPago= true;
        },

        updatePago: function () {
            var form = new FormData();
            form.append('id',                 this.pago.id);
            form.append('folio',              this.pago.folio);
            form.append('cantidad',           this.pago.cantidad);
            form.append('id_reserva',         this.pago.id_reserva);
            form.append('forma_pago',         this.pago.forma_pago);
            form.append('identificador',      this.pago.identificador);

            //Upload Progress Bar
            // const progress = (e) => {
            //     if (e.lengthComputable) {
            //         this.uploadProgress = Math.floor(e.loaded  / e.total * 100);
            //     }
            // }

            var resource = this.$resource("{{route('admin.pagosReserva.update')}}");
            resource.save(form).then(function (response) {
                this.loadingSave = false;
                this.saveInAction = false;

                this.notification('fa fa-check-circle', 'OK!', "Pago Registrado.", 'topCenter');
                this.saveButtonPago = true;
                this.updateButtonPago= false;
                this.closeModalPago();
                this.getAllPagos();
                
                // this.uploadProgress = null;
            }, function (response) {                
                this.loadingSave = false;
                this.saveInAction = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.saveButtonPago = true;
                this.updateButtonPago= false;
                this.errores = response.data.errors;
                // this.uploadProgress = null;
            });
        },

        checkToday: function(idReserva){
            // this.loading = true;
            var resource = this.$resource("{{route('admin.reserva.confirma')}}");
            resource.save({id:idReserva}).then(function (response) { 
                this.loading = false;
                this.notification('fa fa-check-circle', 'OK!', "Reservacion Confirmada.", 'topCenter');
                // this.getCompras(this.dataRoute);
                // Sin refrescar las reservas
                var index = _.findIndex(this.reservas, function(o) { return o.id === idReserva; });
                this.reservas[index].confirmacion = response.data;
               
            }, function (response) {
                this.loading = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.errores = response.data.errors;
            });
        },

        uncheck:function(idReserva){
            // this.loading = true;
            var resource = this.$resource("{{route('admin.reserva.desconfirma')}}");
            resource.save({id:idReserva}).then(function (response) { 
                this.loading = false;
                this.notification('fa fa-check-circle', 'OK!', "Reservacion Confirmada.", 'topCenter');
                // this.getCompras(this.dataRoute);
                var index = _.findIndex(this.reservas, function(o) { return o.id === idReserva; });
                this.reservas[index].confirmacion = response.data;
               
            }, function (response) {
                this.loading = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.errores = response.data.errors;
            });
        },


        flpc: function(id){
            this.modalLPC = true
            this.idSelected = id
        },

        openModalClave: function(id){
            this.modalClave = true
            this.idSelected = id
        },

        asignarClave: function(){
            if (this.reserva.clave) {
                // this.loading = true;
                var resource = this.$resource("{{route('admin.reserva.claveHotel')}}");
                var datos = { id: this.idSelected, clave: this.reserva.clave };
                resource.save(datos).then(function (response) { 
                    this.loading = false;
                    this.modalClave = false;
                    this.notification('fa fa-check-circle', 'OK!', "Clave asignada correctamente", 'topCenter');
                    this.reserva.clave = ''
                    // this.getCompras(this.dataRoute);

                    var index = _.findIndex(this.reservas, function(o) { return o.id === response.data['reserva'].id; });
                    this.reservas[index].clave = response.data['reserva'].clave;
                   
                }, function (response) {
                    this.loading = false;
                    this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                    this.errores = response.data.errors;
                });
            }
            else{
                this.notification('fa fa-check-circle', '¿clave?', "Ingrese la Clave", 'topCenter');
            }
        },

        registrarLPC:function(){
            if (this.reserva.limite_pago_cliente) {
                // this.loading = true;
                var resource = this.$resource("{{route('admin.reserva.lpc')}}");
                var datos = { id: this.idSelected, fecha: this.reserva.limite_pago_cliente };
                resource.save(datos).then(function (response) { 
                    this.loading = false;
                    this.modalLPC = false;
                    this.notification('fa fa-check-circle', 'OK!', "Limite pago del cliente confirmado.", 'topCenter');
                    // this.getCompras(this.dataRoute);
                    // 
                    var index = _.findIndex(this.reservas, function(o) { return o.id === response.data['reserva'].id; });
                    this.reservas[index].limite_pago_cliente = response.data['reserva'].limite_pago_cliente;
                    this.reserva.limite_pago_cliente = ''
                   
                }, function (response) {
                    this.loading = false;
                    this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                    this.errores = response.data.errors;
                });
            }
            else{
                this.notification('fa fa-check-circle', 'OK!', "Seleccione la fecha", 'topCenter');
            }
        },

        fechaLimiteHotel: function(id){            
            this.idSelected = id;
            this.modalfechaHotel = true
            // console.log(this.modalfechaHotel);
        },

        registrarLPH:function(){
            if (this.reserva.limite_pago_hotel) {
                // this.loading = true;
                var resource = this.$resource("{{route('admin.reserva.lph')}}");
                var datos = { id: this.idSelected, fecha: this.reserva.limite_pago_hotel};
                resource.save(datos).then(function (response) {                     
                    this.notification('fa fa-check-circle', 'OK!', "Limite pago a hotel confirmado.", 'topCenter');
                    this.modalfechaHotel = false;
                    // this.getCompras(this.dataRoute);
                   // 
                    var index = _.findIndex(this.reservas, function(o) { return o.id === response.data['reserva'].id; });
                    this.reservas[index].limite_pago_hotel = response.data['reserva'].limite_pago_hotel;
                    this.reserva.limite_pago_hotel = ''
                   
                }, function (response) {
                    this.loading = false;
                    this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                    this.errores = response.data.errors;
                });
            }
            else{
                this.notification('fa fa-check-circle', 'OK!', "Seleccione la fecha", 'topCenter');
            }
        },

        pagarHotel:function (id, action){
            // this.loading = true;
            var resource = this.$resource("{{route('admin.reserva.pagarHotel')}}");
            var datos = { id: id, action:action};
            resource.save(datos).then(function (response) { 
                this.loading = false;
                if (action == 'set') {
                this.notification('fa fa-check-circle', 'OK!', "Pago de hotel confirmado.", 'topCenter');
                }
                else{
                    this.notification('fa fa-check-circle', 'OK!', "Pago de hotel eliminado.", 'topCenter');
                }
                // this.getCompras(this.dataRoute);
                var index = _.findIndex(this.reservas, function(o) { return o.id === response.data['reserva'].id; });
                this.reservas[index].hotel_pagado = response.data['reserva'].hotel_pagado;
               
            }, function (response) {
                this.loading = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.errores = response.data.errors;
            });
        },

        loadUpdatePanel:function(idReserva){
            var index = _.findIndex(this.reservas, function(o) { return o.id === idReserva; });

            this.reserva = {
                id:             this.reservas[index].id,
                tipo:           this.reservas[index].tipo,
                nombre:         this.reservas[index].nombre,
                telefono:       this.reservas[index].telefono,
                correo:         this.reservas[index].correo, 
                habitaciones:   this.reservas[index].habitaciones,             
                hotel:          this.reservas[index].hotel,               
                precio:         this.reservas[index].precio,
                habitacion_info:this.reservas[index].habitacion_info,
                observaciones:  this.reservas[index].observaciones,
                tarifa:         this.reservas[index].tarifa,  
                id_vendedor:    this.reservas[index].id_vendedor,

                medio_contacto: this.reservas[index].medio_contacto           

            }
            // Asignar las fechas a fechas.starDate y endDate
            this.fechas = {startDate:this.reservas[index].fecha_entrada, endDate:this.reservas[index].fecha_salida}

            this.getHabitaciones();
            this.saveButton = false;
            this.updateButton = true;
            this.panelIndex = false;
            this.panelReserva = true;

        },

        updateData:function(){
            if (this.habitaciones.length == 0)
            {                 
               this.notification('fa fa-exclamation-triangle', 'Error', "Registra almenos 1 habitacion", 'topCenter');  
               return                             
            }
            
            else{
                var existe;
                this.habitaciones.forEach(function(habitacion, index) {
                    if (habitacion.nombre == "" || habitacion.adultos == 0) {existe=true;}
                });

               if (existe) {
                this.notification('fa fa-exclamation-triangle', 'Error', "Nombre y adulto en habitacion es requerido", 'topCenter');
                return
               }
            }

            this.loading = true;
            var form = new FormData();
            form.append('id',               this.reserva.id);
            form.append('nombre',           this.reserva.nombre);
            form.append('telefono',         this.reserva.telefono);
            form.append('correo',           this.reserva.correo);

            form.append('hotel',            this.reserva.hotel);

            if (this.fechas != "") {
                form.append('fecha_entrada',    this.fechas.startDate);
                form.append('fecha_salida',     this.fechas.endDate);
            }
            // form.append('adultos',          this.reserva.adultos)
            form.append('habitaciones',     this.reserva.habitaciones);
            form.append('precio',           this.reserva.precio);
            form.append('infoHabitaciones', JSON.stringify(this.habitaciones));
            form.append('observaciones',    this.reserva.observaciones);
            form.append('tarifa',           this.reserva.tarifa);
            form.append('id_vendedor' ,     this.reserva.id_vendedor);
            // medioDeContacto
            form.append('medio_contacto',   this.reserva.medio_contacto);

            var resource = this.$resource("{{route('admin.reserva.update')}}");
            resource.save(form).then(function (response) {


                this.notification('fa fa-check-circle', 'OK!', "Reserva Actualizada correctamente.", 'topCenter');
                this.regresarReserva();
                // this.getCompras(this.dataRoute);
                this.loading = false;
            }, function (response) {                
                // this.loadingSave = false;
                // this.saveInAction = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.erroresReserva = response.data.errors;
                this.loading = false;
                // this.uploadProgress = null;
            });
            
        },

        excelParameters:function(){
            this.loading = true;
            var reservas = this.reservas;
            var reservasIds = []


            reservas.forEach((reserva) => {
                reservasIds.push(reserva.id);
            });

            var idsArray = JSON.stringify(reservasIds);

            if (this.selectedReservas == 0) {
                window.location = "{{URL::to('admin/reservas/excel/parametro')}}?ids="+idsArray;
            }
            else{
                var newIds = JSON.stringify(this.selectedReservas); 
                window.location = "{{URL::to('admin/reservas/excel/parametro')}}?ids="+newIds;
            }
            
            this.loading = false;
        },

        pagosExcel:function(){
            this.loading = true;
            var reservas = this.reservas;
            var reservasIds = []


            reservas.forEach((reserva) => {
                reservasIds.push(reserva.id);
            });

            var idsArray = JSON.stringify(reservasIds);

            if (this.selectedReservas == 0) {
                window.location = "{{URL::to('admin/reservas/excel/excelPagos')}}?ids="+idsArray;
            }
            else{
                var newIds = JSON.stringify(this.selectedReservas); 
                window.location = "{{URL::to('admin/reservas/excel/excelPagos')}}?ids="+newIds;
            }
            
            this.loading = false;
        },


        habitacionMenos:function(){
            if (this.habitaciones.length > 0) {
                this.reserva.habitaciones -= 1
                this.habitaciones.splice(this.reserva.habitaciones, 1)
            }
        },

        habitacionMas:function(){
            if (this.habitaciones.length < 4) {
                this.reserva.habitaciones += 1
                this.habitaciones.push({nombre:'', adultos:0, menores:0, edad1:'', edad2:'', edad3:''}) 
            }

        },

        getHabitaciones:function(){
            for (var i = 0; i <= this.reserva.habitaciones -1; i++) {
                this.habitaciones.push({
                    nombre: this.reserva.habitacion_info[i].nombre,
                    adultos:this.reserva.habitacion_info[i].adultos, 
                    menores:this.reserva.habitacion_info[i].menores, 
                    edad1:this.reserva.habitacion_info[i].edad1, 
                    edad2:this.reserva.habitacion_info[i].edad2,
                    edad3:this.reserva.habitacion_info[i].edad3
                })
            }
        },

        menosAdultos:function(index){
            if (this.habitaciones[index].adultos == 0) {return}
            this.habitaciones[index].adultos-= 1
        },

        menosMenores: function(index){
            if (this.habitaciones[index].menores > 0) {
                var num = this.habitaciones[index].menores

                this.habitaciones[index].menores -= 1
                if (num == 1) {
                    this.habitaciones[index].edad1 = ''
                }
                else if(num == 2)
                {
                    this.habitaciones[index].edad2 = ''
                }
                else{
                    this.habitaciones[index].edad3 = ''
                }
            }
        },
        masMenores:function(index){
            if(this.habitaciones[index].menores < 3) {
                this.habitaciones[index].menores += 1
            }
            
        },

        formatPrice(value) {
        let val = (value/1).toFixed(2).replace(',', '.')
            return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        },

        changeColor(idReserva){
            var form = new FormData();
            form.append('id', idReserva);
            form.append('color', this.reserva.color);

            var resource = this.$resource("{{route('admin.reserva.color')}}");
            resource.save(form).then(function (response) {
                this.notification('fa fa-check-circle', 'OK!', "Reserva Actualizada correctamente.", 'topCenter');
                // this.getCompras(this.dataRoute);
                console.log(response.data);
                var index = _.findIndex(this.reservas, function(o) { return o.id === idReserva; });
                this.reservas[index].color = response.data;
                this.reserva.color = '';

            }, function (response) {     
                       
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.erroresReserva = response.data.errors;
                this.loading = false;
            });
        },

        papeleta(idReserva){
            var resource = this.$resource("{{route('admin.reserva.papeleta')}}");
            resource.save({id:idReserva}).then(function (response) {
                this.notification('fa fa-check-circle', 'OK!', "Envio de Papeleta actualizado.", 'topCenter');
                // console.log(response.data);
                var index = _.findIndex(this.reservas, function(o) { return o.id === idReserva; });
                this.reservas[index].papeleta_enviada = response.data.fecha;

            }, function (response) {     
                       
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.erroresReserva = response.data.errors;
                this.loading = false;
            });
        },

        isNumber: function(evt) {
          evt = (evt) ? evt : window.event;
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
            evt.preventDefault();;
          } else {
            return true;
          }
        },

        deleteRooms:function(index){    
            this.reserva.habitaciones--;
            this.habitaciones.splice((index), 1);
        },

        clearSelected:function(){
            this.selectedReservas = [];
        },

        // Vendedores

        getVendedores:function(){
            this.loading = true;
            var resource = this.$resource("{{route('admin.reserva.getVendedores')}}");
            // var filter = { id: this.pago.id_reserva};
            resource.get().then(function (response) {
                this.vendedores = response.data;
                this.loading = false;
            });
        },

        shortDate:function($date){
            if (!$date) {return}
            moment().locale('es');
            return moment($date).format("DD/MM/YY");
        },

        largeDate:function($date){
            if (!$date) {return}
            moment().locale('es');
            return moment($date).format("DD/MM/YY");
        }

    }
});
</script>