@extends('cms.master')

@section('content')
<style>
    th{
      color: white;
      background: #3c8dbc;
    }
    tr:nth-child(even){background-color: #f2f2f2;}

	.firstCol{
        min-width: 180px;
    }

    .check{
    background:#02650559;
    }
    .unCheck{
    /*background:#f39c1263;*/
    }
    .pagado{
    background:#f39c1263;
    }
    .azul{
    background:#128bf363;
    }
    .mx-5{
        margin-left: 5px;
        margin-right: 5px;
    }
</style>
    <section class="content-header">
        <h1><i class="fa fa-calendar-check-o"></i> Reservas.</h1>
    </section>

    <section class="content" id="app" v-cloak>
    	<div class="box box-primary" v-show="panelIndex">

            <div class="box-header">
                <h3 class="box-title"></h3>
                <div class="box-tools pull-right">
                    <a href="#" class="btn bg-navy" @click="nuevaReserva()">
                    <i class="fa fa-plus-circle"></i> CREAR RESERVA</a>
                </div>

                {{-- <filtersearch :selected="tipo" :options="tipos" @updatefilters="updateFilters"></filtersearch> --}}
                {{-- <datefiltersearch :selected="tipo" :options="tipos" @updatefilters="updateFilters" ref="searching"></datefiltersearch> --}}

                <datefiltersearch2 :selected="tipo" :options="tipos" :selected_fecha="tipo_fecha" :options_fecha="tipos_fecha" 
                @updatefilters="updateFilters" ref="searching"></datefiltersearch2>

                <br>
                <button class="btn btn-xs btn-warning" @click="excelParameters()"
                    style="margin-bottom: 10px;">Exportar Excel</button>

                <button class="btn btn-xs btn-info" @click="pagosExcel()"
                    style="margin-bottom: 10px;">Exportar por Habitacion(prueba)</button>

            </div>

            <div class="box-body">

                <!-- VueLoading icon -->
                <div class="text-center"><i v-show="loading" class="fa fa-spinner fa-spin fa-5x"></i></div>

                <div class="table-responsive" v-show="!loading">
                <table class="table table-bordered  table-responsive table-hover">
                    <thead>
                        <tr>
                            <th>
                                <button class="btn btn-danger btn-xs" @click="clearSelected">X</button>
                            </th>
                            <th width="1%">color</th>
                            <th width="55">Clave Agencía</th>
                            <th>Papeleta</th>
                            <th>Reserva <br> Central</th>
                            <th>Nombre</th>
                            <th>Hab.</th>
                            <th>info Hab.</th>
                            <th width="10%">Hotel</th>
                            {{-- <th>Entrada/Salida</th> --}}
                            <th>LP Cliente</th>
                            <th>LP Hotel</th>
                            <th>Precio</th>
                            <th>Pagos</th>                            
                            <th>Clave <br> Hotel</th>
                            <th width="5%">Pagado <br> a Hotel</th>
                            <th width="3%"></th>


                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="r in reservas"{{--  v-bind:class="[r.precio == r.sumaPagos ? 'pagado' : 'unCheck']" --}}
                        v-bind:style="{ background: r.color}">
                        <td>
                            <input type="checkbox" :id="r.id" :value="r.id" v-model="selectedReservas">

                        </td>   
                            <td>
                                <select class="form-control" v-model="reserva.color" @change="changeColor(r.id)" style="width: 15px;">
                                  <option  selected="selected" disabled>Selecciona</option> 
                                  <option value = "null">Blanco</option>
                                  <option value = "rgba(244, 0, 0, 0.7)">Rojo</option>
                                  <option value = "rgba(54, 162, 235, 0.5)">Azul</option>
                                  <option value = "rgba(241, 196, 15, 0.7)">Amarillo</option>
                                  <option value = "rgba(2, 174, 80, 0.5)">Verde</option>
                                  <option value = "rgba(153, 102, 255, 0.5)">Morado</option>
                                  <option value = "rgba(255, 135, 0, 0.7)">Naranja</option>
                                  <option value = "rgba(248, 203, 172, 0.7)">Salmon</option>
                                </select>
                            </td>
                            <td>@{{r.folio}} </td>

                            <td > 
                                <button class="btn btn-xs" 
                                v-bind:class="[r.papeleta_enviada ? 'btn bg-purple' : 'btn bg-black']"
                                @click="papeleta(r.id)">
                                    <span v-if="r.papeleta_enviada"> @{{ shortDate(r.papeleta_enviada)}} </span>
                                    <span v-else>Papeleta</span>
                                </button>
                            </td>
                            <td v-if="r.confirmacion">
                                <button class="btn btn-xs btn-danger" @click="uncheck(r.id)">@{{shortDate(r.confirmacion)}}</button> 
                            </td>

                            <td v-else> <button class="btn btn-xs btn-warning" @click="checkToday(r.id)">check</button>
                            </td>

                        	<td>
	                               <div  style="color: #3c8dbc; font-weight: bold;">
	                                   <p> @{{r.nombre}} </p>
	                               </div>

	                               {{--  <div style="padding-left: 15px; ">
	                                    <div style="color: #179821; font-weight:bold ;" >@{{r.correo}}</div>
	                                    <div>@{{r.telefono}}</div>
	                                </div> --}}
	                            
                        	</td>
                           <td nowrap="nowrap" style="color:black; font-weight:bold; width:auto; ">
                                @{{r.habitaciones}}
                            </td>

                           <td class="" >
	                            <div class="flex-test">
	                                <div class="">
                                        Adu:
                                        <template v-for="m in r.habitacion_info" >
                                           <span>@{{m.adultos}}</span> {{ '  ' }}<slot> </slot>
                                        </template>
                                        <br>
                                        Men:
                                        <template v-for="m in r.habitacion_info" >
                                           <span>@{{m.menores}}</span> {{ '  ' }}<slot> </slot>
                                        </template>
	                                </div>

	                                <div style="padding-left: 15px; ">
	                                    <div v-if="r.edad1" style="color: #179821; font-weight: bold;" >(@{{r.edad1}})</div>
	                                    <div v-if="r.edad2" style="color: #179821; font-weight: bold;"> (@{{r.edad2}})</div>
	                                </div>
	                            </div>
                        	</td>

                           {{-- <td>@{{r.correo}}</td> --}}
                           <td>@{{r.hotel}} <br> <div style="color: #3c8dbc; font-weight: bold;" >@{{ shortDate(r.fecha_entrada)}} -- @{{shortDate(r.fecha_salida)}} </div></td>

                            <td class = "text-center">
                                @{{ shortDate(r.limite_pago_cliente) }} <br>
                                <button class="btn btn-xs btn-info" @click="flpc(r.id)" style="margin-bottom: 10px;">FLPC</button>
                                
                            </td>

                            <td class = "text-center">
                                @{{shortDate(r.limite_pago_hotel)}} <br>
                                <button class="btn btn-xs btn-info" @click="fechaLimiteHotel(r.id)" style="margin-bottom: 10px;">FLPH</button>
                            </td>
                            <td class="text-center">
                           $ @{{r.precio}}</td>
                           <td >
                               <button class="btn btn-xs btn-success" @click="getPagos(r.id)" 
                                style="margin-bottom: 10px;" title="Pagos">Pagos</button>
                                <p v-if="r.c">Pagado</p>                               
                           </td>

                          <td>
                                @{{r.clave}} <br>
                                <button class="btn btn-xs btn-warning" @click="openModalClave(r.id)"
                                style="margin-bottom: 10px;">Clave</button>
                          </td>
                          <td v-if="r.hotel_pagado == null">
                                <div class="text-center">
                                    <button class="btn btn-xs btn-success" @click="pagarHotel(r.id, 'set')"
                                    style="margin-bottom: 10px;">Pagado a hotel</button>
                                </div>
                          </td>

                          <td v-else>
                            <div class="text-center">
                                {{-- <i class="fa fa-check-square-o"></i>    --}}
                                @{{shortDate(r.hotel_pagado)}}
                                <br>
                                <button class="btn btn-xs btn-warning" @click="pagarHotel(r.id, 'restore')"><i class="fa fa-refresh"></i></button>
                            </div>                            
                          </td>

                          <td nowrap="nowrap">
                                <button class="btn btn-xs btn-warning" @click="loadUpdatePanel(r.id)"
                                        style="" title="Actualizar Pago"><i class="fa fa-refresh"></i></button>

                                <button class="btn btn-xs btn-danger" @click="borrarRerserva(r.id)" 
                                        style="" title="Borrar Reserva"><i class="fa fa-trash"></i></button>
                           </td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </div>

            <div class="box-footer clearfix">
                <pagination @setpage="getCompras" :param="pagination"></pagination>
            </div>
            
        </div>

        <div v-show="panelReserva">
            @include('cms.reservas.partials.inputs')
        </div>

        <div v-show="panelPagos">
            @include('cms.reservas.partials.pagos')
        </div>

        {{-- Limite de pago cliente --}}
        <modal v-if="modalLPC" @close="modalLPC = false" {{-- v-show="!hideTagsModal" --}}>
            <div slot="head">Selecciona la fecha</div>
            <div slot="body">
                <div class="form-group">
                    <label>Selecciona la fecha limite de pago Cliente</label>
                    <datepicker v-model="reserva.limite_pago_cliente"></datepicker>
                </div>            

                <div slot="footer">
                    <div class="text-right"> 
                       {{-- <a class="modal-default-button btn bg-navy" @click="closeModalPago()"><i class="fa fa-times-circle"></i> Cerrar</a> --}}
                       <a class="modal-default-button btn bg-green" @click="registrarLPC()"><i class="fa fa-times-circle"></i> Registrar </a>    
                    </div>
                </div>
            </div>
        </modal>

        {{--  --}}
        <modal v-if="modalfechaHotel" @close="modalfechaHotel = false" {{-- v-show="!hideTagsModal" --}}>
            <div slot="head">Selecciona la fecha</div>
            <div slot="body">
                <div class="form-group">
                    <label>Selecciona la fecha limite de pago Hotel</label>
                    <datepicker v-model="reserva.limite_pago_hotel"></datepicker>
                </div>
            

                <div slot="footer">
                    <div class="text-right"> 
                       {{-- <a class="modal-default-button btn bg-navy" @click="closeModalPago()"><i class="fa fa-times-circle"></i> Cerrar</a> --}}
                       <a class="modal-default-button btn bg-green" @click="registrarLPH()"><i class="fa fa-times-circle"></i> Registrar </a>    
                    </div>
                </div>
            </div>
        </modal>

        {{-- Modal Clave hotel --}}

        <modal v-if="modalClave" @close="modalClave = false" {{-- v-show="!hideTagsModal" --}}>
            <div slot="head">Escribe la clave</div>
            <div slot="body">
                <div class="form-group">
                    <label>Escriba la clave proporcionada por el hotel</label>
                    <input type="text" class="form-control" 
                            placeholder="Clave" v-model="reserva.clave" 
                            :class="formError(errores, 'clave', 'inputError')"/>
                </div>            

                <div slot="footer">
                    <div class="text-right"> 
                       {{-- <a class="modal-default-button btn bg-navy" @click="closeModalPago()"><i class="fa fa-times-circle"></i> Cerrar</a> --}}
                       <a class="modal-default-button btn bg-green" @click="asignarClave"> <i class="fa fa-times-circle"></i> Registrar </a>    
                    </div>
                </div>
            </div>
        </modal>

    </section>

@endsection

@push('scripts')
    @include('cms.reservas.partials.scripts')
@endpush

@push('css')
<style>
    td { 
        border: 100px solid black; 
        word-wrap: break-word; 
    }
    .green{
        background-color: green;
    }
    .red{
        background-color: red;
    }
    .orange{
        background-color: orange;
    }

    textarea
    {
      width:100%;
    }
</style>
@endpush