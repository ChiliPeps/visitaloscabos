@extends('cms.master')

@section('content')

    <section class="content-header">
        <h1>Dashboard</h1>
		<h1 style="float: right;">{{ CMSHelpers::getDate() }}</h1>
    </section>

    <section class="content">

        @lang('cms.welcome') {{ Auth::guard('cms')->user()->name }}

        <br><br>
        
        <div class="container-fluid" id="app">

	    	<div class="row">
	    		<div class="col-md-4">
	    			@include('cms.widgets.disk')
	    		</div>
	    		<div class="col-md-4">
                    @include('cms.widgets.memory')
	    		</div>
	    		<div class="col-md-4">
                    @include('cms.widgets.users')
	    		</div>						
	    	</div>

	    	<div class="row">
	    		{{-- Reservas sin checkCentral --}}
	    		<div class="col-lg-2 col-xs-4">

					<div class="small-box bg-yellow">
						<div class="inner">
							<h3>{{$reservasNoCheck}}</h3>
							<p>Reservas sin check Central</p>
						</div>
						<div class="icon">
							<i class="fa fa-exclamation-circle"></i>
						</div>
						<a href="#" class="small-box-footer">
							More info <i class="fa fa-arrow-circle-right"></i>
						</a>
					</div>
				</div>
				{{-- Reservas sin pago a hotel --}}
				<div class="col-lg-2 col-xs-4">

					<div class="small-box bg-red">
						<div class="inner">
							<h3>{{$reservasNoPay}}</h3>
							<p>Reservas sin Pago a hotel</p>
						</div>
						<div class="icon">
							<i class="fa fa-exclamation-triangle"></i>
						</div>
						<a href="#" class="small-box-footer">
							More info <i class="fa fa-arrow-circle-right"></i>
						</a>
					</div>
				</div>
				{{-- Reservas pagadas --}}

				<div class="col-lg-2 col-xs-4">

					<div class="small-box bg-green">
						<div class="inner">
							<h3>{{$hotelPay}}</h3>
							<p>Reservas Pagadas</p>
						</div>
						<div class="icon">
							<i class="fa fa-check-square-o"></i>
						</div>
						<a href="#" class="small-box-footer">
							More info <i class="fa fa-arrow-circle-right"></i>
						</a>
					</div>
				</div>

	    	</div>

	    	<div class="row">
	    		<div class="col-md-6">
	    			<div class="box box-warning">
		    			<div class="box-body">
		    				<label>Selecciona el año</label>
		    				<select class="form-control" v-model="selectedYear" @change="getHotelsByDay()">
                                {{-- <option value="" selected disabled>Selecciona el año</option> --}}
                                <option value="todos" selected>Todos</option>
                                <option v-for="y in years" :value="y.year">@{{y.year}}</option>
                            </select>
							<canvas id="proof"></canvas>
							{{-- <canvas id="chartDos"></canvas> --}}
						</div>
	    			</div>    			
	    		</div>	    			
	    		
	    		<div class="col-md-6">
				
					<div class="box box-warning">
						<div class="box-body">
							<label>Selecciona el año</label>
							<select class="form-control" v-model="selectedYearReservas" @change="getHotelsByReservas()">
                                {{-- <option value="" selected disabled>Selecciona el año</option> --}}
                                <option value="todos" selected >Todos</option>
                                <option v-for="y in years" :value="y.year">@{{y.year}}</option>
                            </select>
							
							<canvas id="reservas"></canvas>
						</div>
					</div>

					
	    		</div>
	    	</div>

	    	<div class="row">
	    		<div class="col-md-8">
	    			@include('cms.widgets.debtors')
	    		</div>

	    		<div class="col-md-4">
	    			<div class="box box-warning">
		    			<div class="box-body">
		    				<label>Selecciona el año</label>
		    				<select class="form-control" v-model="selecteYearContacto" @change="getChartsMedioContacto()">
                        {{-- <option value="" selected disabled>Selecciona el año</option> --}}
	                        <option value="todos" selected >Todos</option>
	                        <option v-for="y in years" :value="y.year">@{{y.year}}</option>
                    </select>

							<canvas id="medioContacto"></canvas>
						</div>
					
	    			</div>

	    		</div>	
	    	</div>

	    	{{-- <div class="row">
	    		<div class="col-md-12">
	    			<div class="box box-warning">
		    			
					
	    			</div>

	    		</div>	
	    	</div> --}}

		</div>

    </section>
@endsection

@push('css')
<style>
    #myChart, {
    	/*width: 200px;*/
    }

    .cargando{
        width: 100%;
        height:100%;
        position:absolute;
        background:rgba(255,255,255,0.7);
        left:0;
        z-index: 1;
    }

</style>
@endpush

@push('scripts')
    @include('cms.widgets.scripts')
@endpush