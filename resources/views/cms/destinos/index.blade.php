@extends('cms.master')

@section('content')
    <section class="content-header">
        <h1><i class="fa fa-edit"></i> Destinos </h1>
    </section>

    <section class="content" id="app" v-cloak>
    	<div class="box box-primary" v-show="panelIndex">

            <div class="box-header">
                <h3 class="box-title"></h3>
                <div class="box-tools pull-right">
                    <a href="#" class="btn bg-navy" @click="openPanelInputs">
                    <i class="fa fa-plus-circle"></i> Nuevo Destino</a>
                </div>

                {{-- <filtersearch :selected="tipo" :options="tipos" @updatefilters="updateFilters"></filtersearch> --}}
            </div>

            <div class="box-body">

                <!-- VueLoading icon -->
                <div class="text-center"><i v-show="loading" class="fa fa-spinner fa-spin fa-5x"></i></div>

                <div class="table-responsive" v-show="!loading">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th colspan="2">Nombre</th>
                            <th>Galeria</th>
                            <th></th>


                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for = "destino in destinos">
                            <td style="width: 85px;"><img :src="checkImage(destino.thumb)" class="img-rounded" width="75" height="50"></td>
                            <td>
                               <div>
                                    <div style="color: #3c8dbc; font-weight: bold;" :title="destino.nombre" v-html="destino.nombre"></div>
                                    <div>@{{ dateString2(destino.created_at) }}</div>
                                    <div></div>
                                </div>
                            </td>
                            <td>
                                <button type="button" @click="panelGaleria(destino.id)" class="btn btn-info">Galeria</button>
                            </td>
                            <td >                                
                                <button type="button" class="btn btn-danger float-right" @click="deleteDestino(destino.id)"
                                style="float: right;"><i class="fa fa-trash"></i></button>

                                <button type="button" class="btn btn-warning float-right" @click="openUpdateInputPanel(destino.id)" 
                                style="float: right;"><i class="fa fa-refresh"></i></button> 
                            </td>


                        </tr>
                    </tbody>
                </table>
                </div>
            </div>

            <div class="box-footer clearfix">
                {{-- <pagination @setpage="getData" :param="pagination"></pagination> --}}
            </div>
            
        </div>

        {{-- Panel de Inputs --}}
        <div v-show="panelInputs">
            @include('cms.destinos.partials.inputs')
        </div>

        <div>
            @include('cms.destinos.partials.galeria')
        </div>

    </section>
@endsection

@push('scripts')
    @include('cms.destinos.partials.scripts')
@endpush