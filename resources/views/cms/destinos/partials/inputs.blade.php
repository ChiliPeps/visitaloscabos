<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title" v-if="saveButton"> Crear Destino</h3>
        <h3 class="box-title" v-if="updateButton"> Actualizar Destino</h3>
        <div class="box-tools pull-right">
{{--             @if(Auth::guard('cms')->user()->type != "reportero")
            <a class="btn bg-orange btn-sm" @click="aprobarNoticia" v-if="noticia.aprobada == 0"><i class="fa fa-thumbs-o-up"></i> Aprobar Noticia</a>
            @endif --}}
            <a class="btn bg-navy btn-sm" @click="closePanelInputs"><i class="fa fa-chevron-left"></i> Regresar</a>
            <a class="btn btn-success btn-sm" v-show="saveButton" @click="setData()"><i class="fa fa-floppy-o"></i> 
            Guardar</a>
            <a class="btn btn-success btn-sm" v-show="updateButton" @click="updateData()"><i class="fa fa-floppy-o"></i> Actualizar</a>
        </div>
    </div>
</div>

{{-- Form Errros --}}
<formerrors :errorsbag="errores"></formerrors>

<vue-tabs>
    {{-- <br> --}}
    <v-tab title="Datos de la Publicación">
        <div class="row">
            <div class="col-md-6">
        
                <div class="box box-primary">
                    <div class="box-body">
        
                        {{-- titulo --}}
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" class="form-control" 
                            placeholder="título" v-model="destino.nombre" 
                            :class="formError(errores, 'nombre', 'inputError')">
                        </div>

                        <div class="form-group">
                            <label>Para cuantas personas</label>
                            <input type="text" class="form-control" 
                            placeholder="¿Cuantas personas?" v-model="destino.personas">
                        </div>

                        <div class="form-group">
                            <label>Precio</label>
                            <input type="text" class="form-control" 
                            placeholder="Precio" v-model="destino.precio" 
                            :class="formError(errores, 'precio', 'inputError')">
                        </div>
        
                        {{-- fecha --}}
                        <div class="form-group">
                            <label>Vigencia</label>
                            <daterangepicker v-model="destino.vigencia"></daterangepicker>
                        </div>

                        <div class="form-group">
                            <label>Descripción</label>
                            <textarea rows="4" class="form-control" v-model="destino.descripcion"></textarea>
                        </div>
        
                    </div>
                </div>
        
            </div>
            <div class="col-md-6">
        
                <div class="box box-danger">
                    <div class="box-body">

                        <div class="form-group">
                            <label>Incluye:</label>
                            <input type="text" class="form-control" 
                            placeholder="Incluye" v-model="destino.incluye" 
                            :class="formError(errores, 'incluye', 'inputError')">
                        </div>

                        <div class="form-group">
                            <label>Tipo:</label>
                            <input type="text" class="form-control" 
                            placeholder="Aventura/Hotel" v-model="destino.tipo" 
                            :class="formError(errores, 'tipo', 'inputError')">
                        </div>

                        <div class="form-group">
                            <label>Duracion:</label>
                            <input type="text" class="form-control" 
                            placeholder="5 dias/ 4 noches" v-model="destino.duracion" 
                            :class="formError(errores, 'tipo', 'inputError')">
                        </div>

                        <div class="form-group">
                            <label>observaciones:</label>
                            <tinymce2 id="tinyMCE2" 
                            v-model="destino.observaciones"></tinymce>
                        </div>
        
                        <div class="form-group">
                            <label for="urlyoutube">Dirección de youtube</label>
                            <input class="form-control" name="urlyoutube" type="text" v-model="destino.youtubeUrl" 
                            @keyup="comprobar_cambio_url">
                        </div>

                        <div class="form-group ">
                    <button class="btn btn-danger btn-sm" @click="comprobarUrl"  v-if="dir_comprobada==false"> 
                        Comprobar Dirección <i class="fa fa-refresh"></i>
                    </button>

                    <button class="btn btn-success btn-sm" style="width: 200px" v-else> 
                        Dirección comprobada
                        <i class="fa fa-check"></i> 
                    </button>
                    <br>
                    <label for="nuevopreviewyt" v-show="dir_comprobada">Imagen Preview</label><br>
                    <img name="nuevopreviewyt" id="nuevopreviewyt" :src="video.img_prev" class="img-responsive">
                    <div v-if="loading==true" class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></div>

                </div>
                            
                    </div>
                </div>
        
            </div>
            
        </div>
    </v-tab>

    <v-tab title="Contenido">
        <div class="box box-primary">
            <div class="box-body">
                
                <div class="form-group">
                    <label>Contenido</label>
                    <tinymce id="tinyMCE" imageuploadurl="{{route('admin.promociones.tinyimages')}}" 
                    v-model="destino.contenido"></tinymce>
                </div>
            </div>
        </div>
    </v-tab>

    <v-tab title="Imagen">
        <div class="box box-primary">
            <div class="box-header"><h2 class="box-title">Imagen de Noticia (960 x 540)</h2></div>
            <div class="box-body">
                {{-- Image Upload --}}
                <imageupload v-model="destino.imagen"></imageupload>
            </div>
        </div>
    </v-tab>

    {{-- <v-tab title="Galeria">
        
    </v-tab> --}}
</vue-tabs>