<!-- Vue Components & Mixins -->
@include('components.vue.vuePagination')
@include('components.vue.vueHelperFunctions')
@include('components.vue.vueFormErrors')
@include('components.vue.vueNotifications')

@include('components.vue.vueImageUpload')
@include('components.vue.vueFilterSearch')
@include('components.vue.vueTinyMce')
@include('components.vue.vueTinyMceSmall')

@include('components.vue.vueDateRangePicker')

@include('components.vue.vueTabs')

{{-- Vue-upload-component --}}
{{-- Documentation -> https://lian-yue.github.io/vue-upload-component/#/en/documents --}}
{!! Html::script('plugins/vue-upload-component/vue-upload-component.js') !!}
<!-- Vue Components & Mixins -->

<script>
//Laravel's Token
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

var app = new Vue ({
    mounted: function () {
        this.csrf = document.querySelector('#token').getAttribute('content'); // CSRF For File Upload Component
        this.getData(this.dataRoute);
        // this.getDataImages(this.dataRouteArchivos);
    },

    el: "#app",

    mixins: [helperFunctions, notifications],

    components:{ FileUpload: VueUploadComponent},

    data: {
    	loading:false,
    	panelInputs:false,
    	panelIndex:true,

    	saveButton:true,
    	updateButton:false,

    	errores:null,

    	destino:{id:'', nombre:'', precio:'', personas:'', incluye:'', vigencia:'', imagen:'', descripcion:'', contenido:'', youtubeUrl:'', fecha_inicio:'', fecha_fin:'', tipo:'', duracion:'', observaciones:''},

        video:{img_prev:'', titulo:''},

        pagination:null,
        destinos:null,

        // Data Route
        dataRoute: "{{route('admin.destinos.get')}}",
        public_url: "{{ URL::to('/') }}/",

        loadingSave:false,
        saveInAction:false,

        dir_comprobada:false,

        //galerias

        dataRouteArchivos: "{{route('admin.destinos.archivosGet')}}",

        panelImages:false,
        panelSubirImages:false,

        
        imagenes:[],
        idDestinoSelect:'',
        csrf: '',
        files: [],

        archivos:null,
         // Check Archivos
        archivosSelected: [],

        tableOn: true,
        selectedAllOn: false,

        paginationImages:'',
        images:''
    },

    methods: {

        getData: function (url) {
            this.loading = true;
            var filter = { busqueda: this.busqueda, tipo: this.tipo };
            var resource = this.$resource(url);
            resource.get(filter).then(function (response) {
                this.pagination = response.data;
                this.destinos = response.data.data;
                this.loading = false;
            });
        },

         getDataImages: function (url) {
            this.loading = true;
            var filter = { busqueda: this.busqueda, tipo: this.tipo, id:this.idDestinoSelect};
            var resource = this.$resource(url);
            resource.get(filter).then(function (response) {
                this.paginationImages = response.data;
                this.images = response.data.data;
                this.loading = false;
            });
        },

        setData:function(){
            var form = new FormData();
            form.append('nombre',           this.destino.nombre);
            form.append('slugurl',          this.slugurl);
            form.append('personas',         this.destino.personas);
            form.append('precio',           this.destino.precio);
            form.append('incluye',          this.destino.incluye);
            // form.append('vigencia',         this.destino.vigencia);
            form.append('fecha_inicio',     this.destino.vigencia.startDate);
            form.append('fecha_fin',        this.destino.vigencia.endDate);
            form.append('imagen',           this.destino.imagen);
            form.append('thumb',            this.destino.thumb);
            form.append('contenido',        this.destino.contenido);
            form.append('descripcion',      this.destino.descripcion);
            form.append('youtubeUrl',       this.destino.youtubeUrl);

            form.append('tipo',             this.destino.tipo);
            form.append('duracion',         this.destino.duracion);
            form.append('observaciones',    this.destino.observaciones);

            form.append('publish',          this.destino.publish);          
            form.append('hits',             this.destino.hits);

            if(this.destino.imagen != '') { form.append('imagen', this.destino.imagen); }


            //Upload Progress Bar
            const progress = (e) => {
                if (e.lengthComputable) {
                    this.uploadProgress = Math.floor(e.loaded  / e.total * 100);
                }
            }

            var resource = this.$resource("{{route('admin.destinos.set')}}", {}, {}, { progress: progress });
            resource.save(form).then(function (response) {
                this.loadingSave = false;
                this.saveInAction = false;
                this.notification('fa fa-check-circle', 'OK!', "Noticia agregada correctamente.", 'topCenter');
                // this.cleanErrors();
                this.closePanelInputs();
                this.getData(this.dataRoute);
                
                // this.uploadProgress = null;
            }, function (response) {                
                this.loadingSave = false;
                this.saveInAction = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.errores = response.data.errors;
                // this.uploadProgress = null;
            });
        },

        deleteDestino: function (idDestino) {
            swal({
                title: '{{trans("cms.are_you_sure")}}',
                text: '{{trans("cms.wont_revert_this")}}',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, borrala!',
                cancelButtonText: '{{trans("cms.cancel")}}'
                }).then(function () {
                    var resource = this.$resource("{{route('admin.destinos.delete')}}");
                    resource.delete({id:idDestino}).then(function (response) {
                        this.notification('fa fa-check-circle', 'OK!', "Destino Borrado!", 'topCenter');
                        this.getData(this.dataRoute);
                    }, function (response) {
                        this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                        this.checkExtraErrorMessages(response);
                    });
            }.bind(this)).catch(swal.noop);
        },

        openUpdateInputPanel: function (idDestino) {
            var index = _.findIndex(this.destinos, function(o) { return o.id === idDestino; });

            this.destino = {
                id:             this.destinos[index].id,
                nombre:         this.destinos[index].nombre,
                personas:       this.destinos[index].personas,
                precio:         this.destinos[index].precio,
                incluye:        this.destinos[index].incluye,
                fecha_inicio:   this.destinos[index].fecha_inicio,
                fecha_fin:      this.destinos[index].fecha_fin,
                contenido:      this.destinos[index].contenido,
                descripcion:    this.destinos[index].descripcion,
                imagen:         this.destinos[index].imagen,
                youtubeUrl:     this.destinos[index].youtubeUrl,
                tipo:           this.destinos[index].tipo,
                duracion:       this.destinos[index].duracion,
                observaciones:  this.destinos[index].observaciones,
                publish:        this.destinos[index].publish,   
                vigencia:       {startDate:this.destinos[index].fecha_inicio, endDate:this.destinos[index].fecha_fin}            
            }

            // this.destino.vigencia.startDate = this.destino.fecha_inicio
            // this.destino.vigencia.endDate = this.destino.fecha_fin

            this.saveButton = false;
            this.updateButton = true;
            this.panelIndex = false;
            this.panelInputs = true;

            // this.comprobarUrl(); // Obtener Thumb de video de youtube
        },


        updateData: function () {
            // if(this.saveInAction == true) { return; }
            // this.isCatVideo(); // Update video_url
            // this.saveInAction = true;

            // this.loadingSave = true;
            // this.errores = null;

            var form = new FormData();
            form.append('id',                this.destino.id);
            form.append('nombre',            this.destino.nombre);
            form.append('personas',          this.destino.personas);
            form.append('slugurl',           this.slugurl);
            form.append('precio',            this.destino.precio);
            form.append('incluye',           this.destino.incluye);
             form.append('fecha_inicio',     this.destino.vigencia.startDate);
            form.append('fecha_fin',        this.destino.vigencia.endDate);
            form.append('contenido',         this.destino.contenido);

            form.append('descripcion',      this.destino.descripcion);
            form.append('youtubeUrl',       this.destino.youtubeUrl);

            form.append('tipo',             this.destino.tipo);
            form.append('duracion',         this.destino.duracion);
            form.append('observaciones',    this.destino.observaciones);

            form.append('publish',           this.destino.publish);
            
            //Is File ?? o String ??
            if(this.destino.imagen != '') {
                if(Object.prototype.toString.call(this.destino.imagen) === '[object File]' ) {
                    form.append('imagen', this.destino.imagen);
                } else { form.append('imagen', 'no valid'); }
            }
            
            // Tags
            // form.append('tags', JSON.stringify(this.noticia.tags));

            //Upload Progress Bar
            const progress = (e) => {
                if (e.lengthComputable) {
                    this.uploadProgress = Math.floor(e.loaded  / e.total * 100);
                }
            }

            var resource = this.$resource("{{route('admin.destinos.update')}}", {}, {}, { progress: progress });
            resource.save(form).then(function (response) { 
                this.loadingSave = false;
                this.saveInAction = false;
                this.notification('fa fa-check-circle', 'OK!', "Noticia actualizada correctamente.", 'topCenter');
                // this.notifyWS(response.data.noticia_id);
                // this.cleanErrors();
                this.closePanelInputs();
                this.getData(this.dataRoute);
                this.uploadProgress = null;
            }, function (response) {
                this.loadingSave = false;
                this.saveInAction = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.errores = response.data.errors;
                this.uploadProgress = null;
            });
        },

        togglePublish: function (idDestino) {
            // Tiene que estar aprobada
            // var indexa = _.findIndex(this.noticias, function(o) { return o.id === idDestino; });
            // if (this.noticias[indexa].aprobada == false) {
            //     this.notification('fa fa-exclamation-triangle', 'Error', "La noticia no está aprobada.", 'topCenter');
            //     return;
            // }

            swal({
                title: '{{trans("cms.are_you_sure")}}',
                text: 'Se cambiará la visibilidad del destino.',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí',
                cancelButtonText: '{{trans("cms.cancel")}}'
                }).then(function () {
                    var resource = this.$resource("{{route('admin.destinos.publish')}}");
                    resource.save({id:idDestino}).then(function (response) { 
                        this.loadingPublish = false;
                        this.saveInAction = false;
                        var index = _.findIndex(this.destinos, function(o) { return o.id === idDestino; });
                        this.destinos[index].publish = response.data['publish'];
                    }, function (response) {
                        this.loadingPublish = false;
                        this.saveInAction = false;
                        this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                        this.checkExtraErrorMessages(response);
                    });
            }.bind(this)).catch(swal.noop);
        },

    	openPanelInputs:function(){
    		this.panelIndex=false
    		this.panelInputs=true
    	},

    	closePanelInputs(){
    		this.panelIndex=true
    		this.panelInputs=false
            this.panelImages=false
            this.saveButton = true
            this.updateButton = false
            this.cleanInputs();
    	},

    	checkImage: function (image_url) {
            if (!/^(f|ht)tps?:\/\//i.test(image_url)) {
                return this.public_url+image_url
            } else { return image_url }
        },

        cleanInputs:function () {
            this.destino = {id:'', nombre:'', personas:'', incluye:'', imagen:'', contenido:'', youtubeUrl:'', observaciones:''}
            this.destino.vigencia = {startDate:moment().format('YYYY-MM-DD'), endDate:moment().format('YYYY-MM-DD')}
            this.video={img_prev:'', titulo:''}
        },

        cleanErrors:function(){
            this.errores=null
        },

        comprobar_cambio_url:function(){
            this.dir_comprobada = false
        },

        comprobarUrl:function(){
        	if (this.destino.youtubeUrl !="") 
        	{
        		this.loading=true;
        		var resource = this.$resource("{{route('admin.destino.comprobarVideo')}}"); 
        		resource.get({url:this.destino.youtubeUrl}).then(function (response) {
        			if (response.data != false) {
                        console.log(response.data);
        				this.video.img_prev = response.body.urlimg;
        				this.video.titulo = response.body.titulo;
        				this.urlComprobada=true;
        				this.notification('fa fa-check-circle', 'OK!', "Video correcto", 'topCenter');
                        this.loading=false
                        this.dir_comprobada=true
        			}
        			else{
        				this.notificationError('Error', "{{trans('cms.server_error')}}" , 'topCenter');
                        this.video.img_prev = "";
                        this.video.titulo = "";
                        this.loading=false
        			}
                	
            	});
        	}
        },

        //GALERIAS
        panelGaleria:function(id){
            this.panelIndex=false
            this.panelImages=true
            this.idDestinoSelect=id
            this.getDataImages(this.dataRouteArchivos);
        },

        inputFile: function (newFile, oldFile) {
            if (newFile && oldFile && !newFile.active && oldFile.active) {
                // Get response data
                if (newFile.response) {
                    this.imagenes.unshift(newFile.response); // Agregar al principio del array
                }
                this.notification('fa fa-exclamation-triangle', 'Correcto', "Archivo(s) subido(s) con exito.", 'topCenter');
            }
        },

                // Filtros/Operaciones antes de seleccionar un archivo a subir
        inputFilter: function (newFile, oldFile, prevent) {           
            // Create Blob for Thumb
            if (newFile && (!oldFile || newFile.file !== oldFile.file)) {

                // File Size Validation
                if (newFile.size > 8388608) {
                    this.notification('fa fa-exclamation-triangle', 'Error', "El archivo no debe ser mayor a 8 Mb.", 'topCenter');
                    return prevent();
                }

                if (!/\.(jpeg|jpe|jpg|gif|png|webp)$/i.test(newFile.name)) {
                    // Nada XD ... no se usar REGEx jaja
                    this.notification('fa fa-exclamation-triangle', 'Error', "Solo se suben imágenes", 'topCenter');
                    return prevent();
                } else {
                    // Create a blob field
                    newFile.blob = '';
                    let URL = window.URL || window.webkitURL;
                    if (URL && URL.createObjectURL) {
                        newFile.blob = URL.createObjectURL(newFile.file);
                    }
                }
            }
        },

        seleccionarTodos: function () {
            var self = this;

            if (this.selectedAllOn) {
                this.archivosSelected = [];
                this.selectedAllOn = false;
            } else {
                this.archivosSelected = [];
                _.forEach(self.archivos, function(value) {
                    self.archivosSelected.push({ archivo_id: value.id });
                });
                this.selectedAllOn = true;
            }
        },

        checkURL: function (url) {
            return(url.match(/\.(jpeg|jpg|gif|png|bmp)$/) != null);
        },

        isChecked: function (archivoId) {
            var index = _.findIndex(this.archivosSelected, function(o) { return o.archivo_id === archivoId; });
            if (index !== -1) { return true; } else { return false; }
        },

        selectArchivo: function (archivoId) {
            if (this.isChecked(archivoId)) {
                var index = _.findIndex(this.archivosSelected, function(o) { return o.archivo_id === archivoId; });
                this.archivosSelected.splice(index, 1);
            } else {
                this.archivosSelected.push({ archivo_id: archivoId });
            }
        },

        openPanelInputsArchivos: function(){
            this.panelSubirImages = true
            this.panelImages = false
        },

        closePanelInputs2: function(){
            this.panelSubirImages = false
            this.panelImages = true
            this.getDataImages(this.dataRouteArchivos);
        },

        regresar: function(){
            this.panelImages= false
            this.panelIndex = true
            
        },

        deleteSelectedArchivos: function () {
            var self = this;
            swal({
                title: '{{trans("cms.are_you_sure")}}',
                text: '{{trans("cms.wont_revert_this")}}',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{trans("cms.yes_delete_it")}}',
                cancelButtonText: '{{trans("cms.cancel")}}'
                }).then(function () {

                    var ids = [];
                    _.forEach(this.archivosSelected, function(value) {
                        ids.push(value.archivo_id);
                    });

                    var resource = this.$resource("{{route('admin.archivos.deletemany')}}");
                    resource.save({ ids: ids }).then(function (response) {
                        this.notification('fa fa-check-circle', 'OK!', 'Archivos Borrados!', 'topCenter');
                        this.archivosSelected = [];
                        this.getDataImages(this.dataRouteArchivos);
                        this.getData(this.dataRoute);
                    }, function (response) {
                        this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                    });
            }.bind(this)).catch(swal.noop);
        },
    }
});
</script>