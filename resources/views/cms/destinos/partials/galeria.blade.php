<div class="box box-primary" v-show = "panelImages">

    <div class="box-header">
        <h3 class="box-title"></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-warning" @click ="regresar()" >Regresar</button>
            <a href="#" class="btn bg-navy" @click="openPanelInputsArchivos">
                <i class="fa fa-plus-circle"></i> Nuevos Archivos
            </a>
        </div>
        
        <div class="flexislots">
            {{-- Seleccionar / Deseleccionar Todos --}}
            <div style="margin-right: 10px;">
                <button type="button" class="btn btn-sm btn-default" @click="seleccionarTodos()">
                    <i class="fa fa-check-square-o" v-if="!selectedAllOn"> Seleccionar Todos</i>
                    <i class="fa fa-square" v-else> Deseleccionar Todos</i>
                </button>
            </div>

            {{-- Borrar Varios --}}
            <div v-if="archivosSelected.length > 0">
                <button type="button" class="btn btn-sm btn-danger" @click="deleteSelectedArchivos()">
                <i class="fa fa-trash"> Borrar Seleccionados</i></button>
            </div>
        </div>
            
       {{--  <filtersearch :selected="tipo" :options="tipos" @updatefilters="updateFilters"></filtersearch> --}}
    </div>

    <div class="box-body">

        <div class="text-center"><i v-show="loading" class="fa fa-spinner fa-spin fa-5x"></i></div>
        
        <div class="table-responsive" v-show="!loading" v-if="tableOn">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th style="width: 45px;"></th>
                    <th>Thumb</th>
                    <th>Nombre</th>
                    <th>Subido</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="archivo in images">
                    <td>
                        <button v-if="!isChecked(archivo.id)" type="button" class="btn btn-sm btn-default" @click="selectArchivo(archivo.id)">
                        <i class="fa fa-square-o"> </i></button>
                        <button v-else type="button" class="btn btn-sm btn-default" @click="selectArchivo(archivo.id)">
                        <i class="fa fa-check-square-o"> </i></button>
                    </td>
                    <td>
                        <img v-if="checkURL(public_url+archivo.ruta)" :src="public_url+archivo.ruta" class="img-responsive" width="40" height="auto">
                        <i v-else class="fa fa-file" style="font-size: 40px;"></i>
                    </td>
                    <td>@{{ archivo.nombre }}</td>
                    <td>@{{ dateString2(archivo.created_at) }}</td>
                    <td>
                        {{-- <button type="button" class="btn btn-sm btn-success" @click="downloadArchivo(archivo.id)">
                        <i class="fa fa-cloud-download"> Descargar</i></button>
                        <button type="button" class="btn btn-sm btn-danger" @click="deleteArchivo(archivo.id)">
                        <i class="fa fa-trash"> Borrar</i></button> --}}
                    </td>
                </tr>
            </tbody>
        </table>
        </div>
    </div>
</div>


<div v-show="panelSubirImages">
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"> Subir Archivos</h3>
        <div class="box-tools pull-right">
            <a class="btn bg-navy btn-sm" @click="closePanelInputs2"><i class="fa fa-chevron-left"></i>Regresar</a>
        </div>
    </div>
</div>

<div class="box box-primary">
    <div class="box-body">

        <div class="row">
            <div class="col-xs-12 col-md-6">
                {{-- FileUpload Component --}}
                <file-upload
                    :headers="{'X-CSRF-TOKEN': csrf, 'X-Requested-With': 'XMLHttpRequest'}"
                    ref="upload"
                    v-model="files"
                    post-action="{{route('admin.archivos.aloneupload')}}"
                    @input-file="inputFile"
                    @input-filter="inputFilter"
                    :data="{idDestinoSelect: idDestinoSelect}"
                    :multiple="true"
                    :maximum="10"
                    :drop="true">
                <a class="btn btn-info"><i class="fa fa-plus-circle"></i> Agregar Archivos</a>
                </file-upload>
                {{-- FileUpload Component --}}
            </div>
            <div class="col-xs-12 col-md-6" style="text-align: right;">
                <a v-show="!$refs.upload || !$refs.upload.active" @click.prevent="$refs.upload.active = true" class="btn btn-success">
                <i class="fa fa-cloud-upload"></i> Subir Archivos</a>
                <a v-show="$refs.upload && $refs.upload.active" @click.prevent="$refs.upload.active = false" class="btn btn-danger">
                <i class="fa fa-stop-circle-o"></i> Parar Subida de Archivos</a>
            </div>
        </div>


        <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Thumb</th>
                    <th>Nombre</th>
                    <th>Tamaño</th>
                    <th>Velocidad</th>
                    <th>Estatus</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>

                <tr v-if="!files.length">
                    <td colspan="7">
                        {{-- <div class="text-center p-5"><h4>Suelta archivos en cualquier lugar para subir<br>ó</h4>
                            <label for="file" class="btn btn-lg btn-primary">Seleccionar Archivos</label>
                        </div> --}}
                    </td>
                </tr>

                <tr v-for="(file, index) in files">
                    <td>@{{index}}</td>
                    <td>
                        <img v-if="file.blob" :src="file.blob" class="img-responsive" width="40" height="auto">
                        <span v-else><i class="fa fa-file" style="font-size: 40px;"></i></span>
                    </td>
                    <td>
                        <div></div>@{{file.name}}
                        <div class="progress" v-if="file.active || file.progress !== '0.00'">
                            <div class="progress-bar progress-bar-primary progress-bar-striped" 
                            :class="{'bg-danger': file.error, 'progress-bar-animated': file.active}" 
                            role="progressbar" :style="{ width: file.progress + '%' }">@{{file.progress}}%</div>
                        </div>
                    </td>
                    <td>@{{formatBytes(file.size, 2)}}</td>
                    <td>@{{formatBytes(file.speed, 2)}}</td>
                    <td>
                        <span v-if="file.error" class="label label-danger">@{{file.error}}</span>
                        <span v-else-if="file.success" class="label label-success">OK</span>
                        <span v-else-if="file.active" class="label label-info">Activo</span>
                        <span v-else class="label label-warning">Esperando...</span>
                    </td>
                    <td>
                        <button type="button" class="btn btn-xs btn-danger" @click.prevent="$refs.upload.remove(file)">
                        <i class="fa fa-trash"></i></button>
                    </td>
                </tr>
            </tbody>
        </table>
        </div>

    </div>
</div>


<div>
    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
        <span class="label label-warning"> El tamaño Máximo de Archivo es 8 Mb</span>
        <span class="label label-danger"> Máximo de 10 Archivos por subida</span>
      {{--   <span class="label label-info" v-show="$refs.upload && $refs.upload.features.drag">Support drag and drop upload</span>
        <span class="label label-info" v-show="$refs.upload && $refs.upload.features.directory">Support folder upload</span>
        <span  class="label label-info"v-show="$refs.upload && $refs.upload.features.html5">Support for HTML5</span> --}}
    </p>
</div>

</div>