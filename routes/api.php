<?php

use Illuminate\Http\Request;

use App\Models\CMS\CMSVenta;
use App\Models\CMS\CMSPago;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'v1'], function () {

	Route::get('r/prueba', function () {
        // return new ReservationResource(Reservation::where('hash_link', $hashId)->firstOrFail());
		$results = CMSVenta::all();
		return response()->json($results);

	});

	Route::get('venta/{id}', function () {
        // return new ReservationResource(Reservation::where('hash_link', $hashId)->firstOrFail());
        return CMSVenta::where('id', 1)->get();
        // return CMSVenta::all();
    });


	Route::get('pago/{id?}', function ($id = null) {

        $results = CMSPago::with('venta')->where('id_venta', $id)->orderBy('created_at', 'desc')->get();
        return response()->json($results);
    });
    

    
    // Route::group(['middleware' => ['ajax']], function () {
        Route::get('prueba/prueba',           ['uses' => 'PromocionesController@getPromociones']);
    // });

});