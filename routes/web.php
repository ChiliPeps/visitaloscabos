<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Index
Route::get('/',             ['as' => 'inicio',      'uses' => 'SiteController@inicio']);
Route::get('hoteles',       ['as' => 'hoteles',     'uses' => 'SiteController@hoteles']);
Route::get('promociones',   ['as' => 'promociones', 'uses' => 'SiteController@promociones']);
Route::get('contacto',      ['as' => 'contacto',    'uses' => 'SiteController@contacto']);
Route::get('galeria',       ['as' => 'galeria',     'uses' => 'SiteController@galeria']);


// Route::get('destino/cabo',  ['as' => 'destino',     'uses' => 'SiteController@destino']);

//Promociones
Route::get('promocionesGet',         ['as' => 'get.Promociones',        'uses' => 'PromocionesController@getPromociones']);

// Blog
Route::get('blog',         ['as' => 'blog',        'uses' => 'SiteController@blog']);

Route::get('blog/{id}',		['as' => 'publicacion',     'uses' => 'PublicacionesController@getPublicacion']);

//Destinos
Route::get('destino/{id}',  ['as' => 'destinos',  'uses' => 'DestinosController@getDestino']);

Route::get('promocion/{id}',['as' => 'promo',  'uses' => 'PromocionesController@getPromo']);

// nuevas routes
Route::get('promociones/{nombre}',['as' => 'promocionNombre',  'uses' => 'PromocionesController@getPromodos']);
Route::get('promociones/{nombre}/reserva/{id}',['as' => 'promocionReserva',  'uses' => 'PromocionesController@getPromotres']);



Route::get('promocionid',   ['as' => 'promoXid',  'uses' => 'PromocionesController@getPromoId']);
// contacto correo  
Route::post('/contacto_send', ['as' => 'contacto.send',   'uses' => 'ContactoController@enviarCorreo']);
Route::post('/reserva_send',  ['as' => 'reserva.send',    'uses' => 'ContactoController@enviarReserva']);
Route::post('/recordatorioPago_send',['as' => 'recordatorio.send', 'uses'=>'ContactoController@recordatorioPago']);


Route::get('/test', ['as' => 'test', 'uses' => 'TestController@test']);

// Rutas Stripe
Route::get('pago',             ['as' => 'pago',      'uses' => 'StripeController@prueba']);
Route::post('saveDatos',       ['as' => 'saveDatos', 'uses' => 'StripeController@saveDatos']);

Route::get('success',          ['as' => 'success',   'uses' => 'StripeController@success']);

// proof send mail queque
Route::get('email-test', ['as' => 'success',   'uses' => 'ContactoController@recordatorioPagoMultiple']);
// Route::get('email-test', function(){
  
// 	$details['email'] = 'your_email@gmail.com';
  
//     dispatch(new App\Jobs\DebtorsMail($details));
// });