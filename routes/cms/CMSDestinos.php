<?php

// Destinos Module
Route::get('destinos',   ['as' => 'admin.destinos.index', 'uses' => 'Cms\DestinosController@index']);

Route::group(['middleware' => ['ajax']], function () {


	Route::get('video/comprobar',  ['as' => 'admin.destino.comprobarVideo',    'uses' => 'Cms\DestinosController@comprobarUrl']);


	Route::get('destinos/get', ['as' => 'admin.destinos.get',      'uses' => 'Cms\DestinosController@getDestinos']);

	Route::post('destinos/set',          ['as' => 'admin.destinos.set',      'uses' => 'Cms\DestinosController@setDestino']);

    Route::post('destinos/update',       ['as' => 'admin.destinos.update',   'uses' => 'Cms\DestinosController@updateDestino']);

    Route::delete('destinos/delete',     ['as' => 'admin.destinos.delete',   'uses' => 'Cms\DestinosController@deleteDestino']);    

    Route::post('destinos/publish',      ['as' => 'admin.destinos.publish',   'uses' => 'Cms\DestinosController@publish']);

    Route::post('archivos/aloneupload',  ['as' => 'admin.archivos.aloneupload', 'uses' => 'Cms\DestinosController@aloneUploadFile']);

    Route::get('archivos/get',  ['as' => 'admin.destinos.archivosGet',   'uses' => 'Cms\DestinosController@getArchivos']);

    Route::post('archivos/deletemany',   ['as' => 'admin.archivos.deletemany',  'uses' => 'Cms\DestinosController@deleteManyArchivos']);
});