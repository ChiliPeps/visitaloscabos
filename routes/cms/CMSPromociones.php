<?php

// Promociones Module
Route::get('promociones',   ['as' => 'admin.promociones.index', 'uses' => 'Cms\PromocionesController@index']);

//Ajax Routes
Route::group(['middleware' => ['ajax']], function () {
    Route::get('promociones/get',           ['as' => 'admin.promociones.get',      'uses' => 'Cms\PromocionesController@getPromociones']);

    Route::post('promociones/set',          ['as' => 'admin.promociones.set',      'uses' => 'Cms\PromocionesController@crearPromocion']);
    
    Route::post('promociones/update',       ['as' => 'admin.promociones.update',   'uses' => 'Cms\PromocionesController@updatePromocion']);

    Route::delete('promociones/delete',     ['as' => 'admin.promociones.delete',   'uses' => 'Cms\PromocionesController@deletePromocion']);

    // Route::get('autores/getAll',           ['as' => 'admin.autores.getAll',      'uses' => 'Cms\PromocionesController@getAll']);

    // // Para Módulo de Noticias
    // Route::get('autores/getAll',        ['as' => 'admin.autores.getAll',      'uses' => 'Cms\PromocionesController@getAll']);


	//subir imagenes 
    Route::post('promociones/tinyimages',  ['as' => 'admin.promociones.tinyimages', 'uses' => 'Cms\PromocionesController@tinymceImages']);
});