<?php

//Ajax Routes
Route::group(['middleware' => ['ajax']], function () {
    Route::get('debtors/get',  ['as' => 'admin.debtors.get', 'uses' => 'Cms\AdminController@getDebtors']);
    Route::delete('debtors/delete',  ['as' => 'admin.debtors.delete', 'uses' => 'Cms\AdminController@deleteDebtors']);

    Route::get('charts/get',  ['as' => 'admin.charts.get', 'uses' => 'Cms\AdminController@charts']);

    Route::get('charts/hotelsByDay',  ['as' => 'admin.charts.get', 'uses' => 'Cms\AdminController@getHotelsByDay']);
    Route::get('charts/getYears',  ['as' => 'admin.charts.getYears', 'uses' => 'Cms\AdminController@getYears']);

    // Get chart most Reservas
    Route::get('charts/hotelsByReservas',  ['as' => 'admin.charts.getReservas', 'uses' => 'Cms\AdminController@getHotelsByReservas']);

    // getMedioContacto
    Route::get('charts/getMedioContacto',  ['as' => 'admin.charts.getMedioContacto', 'uses' => 'Cms\AdminController@getMedioContacto']);    
});