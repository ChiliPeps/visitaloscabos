<?php

// Reservas Module
Route::get('reservas',   ['as' => 'admin.reservas.index', 'uses' => 'Cms\ReservasController@index']);

//Ajax Routes
Route::group(['middleware' => ['ajax']], function () {
    Route::get('reservas/get',           ['as' => 'admin.reservas.get',      'uses' => 'Cms\ReservasController@getReservas']);

    Route::post('reserva/set',          ['as' => 'admin.reserva.set',      'uses' => 'Cms\ReservasController@setReserva']);
    
    Route::post('reserva/update',       ['as' => 'admin.reserva.update',   'uses' => 'Cms\ReservasController@updateReserva']);

    Route::delete('reserva/delete',     ['as' => 'admin.reserva.delete',   'uses' => 'Cms\ReservasController@deleteReserva']);


    // Route::get('autores/getAll',           ['as' => 'admin.autores.getAll',      'uses' => 'Cms\PromocionesController@getAll']);

    // // Para Módulo de Noticias
    // Route::get('autores/getAll',        ['as' => 'admin.autores.getAll',      'uses' => 'Cms\PromocionesController@getAll']);


	//subir imagenes 
    // Route::post('promociones/tinyimages',  ['as' => 'admin.promociones.tinyimages', 'uses' => 'Cms\ReservasController@tinymceImages']);

    // Confirmar
    Route::post('confirmar',            ['as' => 'admin.reserva.confirma',       'uses' => 'Cms\ReservasController@confirma']);
    Route::post('desconfirmar',         ['as' => 'admin.reserva.desconfirma',    'uses' => 'Cms\ReservasController@desconfirma']);

    Route::post('limitePagoCliente',    ['as' => 'admin.reserva.lpc',      'uses' => 'Cms\ReservasController@lpc']);
    Route::post('limitePagoHotel',      ['as' => 'admin.reserva.lph',      'uses' => 'Cms\ReservasController@lph']);

    Route::post('claveHotel',           ['as' => 'admin.reserva.claveHotel', 'uses' => 'Cms\ReservasController@asignarClave']);

    Route::post('hotelPagado',          ['as' => 'admin.reserva.pagarHotel', 'uses' => 'Cms\ReservasController@hotelPagado']);

    Route::post('color',                ['as' => 'admin.reserva.color', 'uses' => 'Cms\ReservasController@color']);

    Route::post('papeleta',             ['as' => 'admin.reserva.papeleta', 'uses' => 'Cms\ReservasController@papeleta']);

    // Vendedores
    Route::get('getVendedores', ['as' => 'admin.reserva.getVendedores', 'uses'=> 'Cms\ReservasController@getVendedores']);

    

});

// rutas para excel
Route::get('reservas/excel/parametro',       ['as' => 'admin.reservas.excelProof',      'uses' => 'Cms\ReservasController@excelProof']);

Route::get('reservas/excel/excelPagos',       ['as' => 'admin.reservas.excelPagos',      'uses' => 'Cms\ReservasController@excelPagos']);

// Route::get('reservas/excel/parametro/{busqueda}/{tipo}',       ['as' => 'admin.reservas.excelParamters',      'uses' => 'Cms\ReservasController@excelParameters']);

// Route::get('reservas/excel/parametroFecha/{fecha1}/{fecha2}',       ['as' => 'admin.reservas.excelParamters',      'uses' => 'Cms\ReservasController@excelParametersFecha']);

// Route::get('reservas/excel/parametroBusquedaFecha/{busqueda}/{tipo}/{fecha1}/{fecha2}',       ['as' => 'admin.reservas.excelParamters',      'uses' => 'Cms\ReservasController@excelParametersBusquedaFecha']);
