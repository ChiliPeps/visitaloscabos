<?php

// Reservas Module
// Route::get('pagos',   ['as' => 'admin.reservas.index', 'uses' => 'Cms\PagosController@index']);

//Ajax Routes
Route::group(['middleware' => ['ajax']], function () {
	
    Route::get('pagos/get',           ['as' => 'admin.pagos.get',      'uses' => 'Cms\PagosController@getPagos']);

    Route::post('pagos/set',          ['as' => 'admin.pagos.set',   'uses' => 'Cms\PagosController@registrarPago']);
    
    Route::post('pagos/update',       ['as' => 'admin.pagos.update',   'uses' => 'Cms\PagosController@updatePago']);

    Route::delete('pagos/delete',     ['as' => 'admin.pagos.delete',   'uses' => 'Cms\PagosController@deletePago']);

    // Route::get('autores/getAll',           ['as' => 'admin.autores.getAll',      'uses' => 'Cms\PagosController@getAll']);

    // // Para Módulo de Noticias
    Route::get('pagos/suma',        ['as' => 'admin.pagos.sumaPagos',      'uses' => 'Cms\PagosController@getSuma']);


	//subir imagenes 
    // Route::post('promociones/tinyimages',  ['as' => 'admin.promociones.tinyimages', 'uses' => 'Cms\PagosController@tinymceImages']);
});