<?php

// Ventas Module
Route::get('ventas',   ['as' => 'admin.ventas.index', 'uses' => 'Cms\VentasController@index']);

//Ajax Routes
Route::group(['middleware' => ['ajax']], function () {
    Route::get('ventas/get',           ['as' => 'admin.ventas.get',      'uses' => 'Cms\VentasController@getReservas']);

    Route::post('ventas/set',          ['as' => 'admin.ventas.set',      'uses' => 'Cms\VentasController@setReserva']);
    
    Route::post('ventas/update',       ['as' => 'admin.ventas.update',   'uses' => 'Cms\VentasController@updateReserva']);

    Route::delete('ventas/delete',     ['as' => 'admin.ventas.delete',   'uses' => 'Cms\VentasController@deleteReserva']);

    // Confirmar
    Route::post('ventas/confirmar',          ['as' => 'admin.ventas.confirma',       'uses' => 'Cms\VentasController@confirma']);
    Route::post('ventas/desconfirmar',       ['as' => 'admin.ventas.desconfirma',    'uses' => 'Cms\VentasController@desconfirma']);
    // limite pago cliente
    Route::post('ventas/limitePagoCliente',  ['as' => 'admin.ventas.lpc',      'uses' => 'Cms\VentasController@lpc']);
    Route::post('ventas/limitePagoHotel',    ['as' => 'admin.ventas.lph',      'uses' => 'Cms\VentasController@lph']);

    Route::post('ventas/claveHotel',         ['as' => 'admin.ventas.claveHotel', 'uses' => 'Cms\VentasController@asignarClave']);

    Route::post('ventas/hotelPagado',         ['as' => 'admin.ventas.pagarHotel', 'uses' => 'Cms\VentasController@hotelPagado']);
    // 
    Route::get('ventas/getUpdated',           ['as' => 'admin.ventas.getUpdated',      'uses' => 'Cms\VentasController@getUpdated']);
    

});

// rutas para excel

Route::get('ventas/excel/parametro/{busqueda}/{tipo}',       ['as' => 'admin.ventas.excelParamters',      'uses' => 'Cms\VentasController@excelParameters']);

Route::get('ventas/excel/parametroFecha/{fecha1}/{fecha2}',       ['as' => 'admin.ventas.excelParamters',      'uses' => 'Cms\VentasController@excelParametersFecha']);

Route::get('ventas/excel/parametroBusquedaFecha/{busqueda}/{tipo}/{fecha1}/{fecha2}',       ['as' => 'admin.ventas.excelParamters',      'uses' => 'Cms\VentasController@excelParametersBusquedaFecha']);
