<?php

// Reservas Module
// Route::get('pagos',   ['as' => 'admin.reservas.index', 'uses' => 'Cms\PagosController@index']);

//Ajax Routes
Route::group(['middleware' => ['ajax']], function () {
	
    Route::get('pagosReserva/get',           ['as' => 'admin.pagosReserva.get',      'uses' => 'Cms\PagosReservasController@getPagos']);

    Route::post('pagosReserva/set',          ['as' => 'admin.pagosReserva.set',   'uses' => 'Cms\PagosReservasController@registrarPago']);
    
    Route::post('pagosReserva/update',       ['as' => 'admin.pagosReserva.update',   'uses' => 'Cms\PagosReservasController@updatePago']);

    Route::delete('pagosReserva/delete',     ['as' => 'admin.pagosReserva.delete',   'uses' => 'Cms\PagosReservasController@deletePago']);

    // Route::get('autores/getAll',           ['as' => 'admin.autores.getAll',      'uses' => 'Cms\PagosController@getAll']);

    // // Para Módulo de Noticias
    Route::get('pagosReserva/suma',        ['as' => 'admin.pagosReserva.sumaPagos',      'uses' => 'Cms\PagosReservasController@getSuma']);


	//subir imagenes 
    // Route::post('promociones/tinyimages',  ['as' => 'admin.promociones.tinyimages', 'uses' => 'Cms\PagosController@tinymceImages']);
});