<?php

// Vendedores Module
Route::get('vendedores',   ['as' => 'admin.vendedores.index', 'uses' => 'Cms\VendedoresController@index']);

// Ajax Routes
Route::group(['middleware' => ['ajax']], function () {
    Route::get('vendedores/get',           ['as' => 'admin.vendedores.get',      'uses' => 'Cms\VendedoresController@getVendedores']);

    Route::post('vendedores/set',          ['as' => 'admin.vendedores.set',      'uses' => 'Cms\VendedoresController@setVendedor']);
    
    Route::post('vendedores/update',       ['as' => 'admin.vendedores.update',   'uses' => 'Cms\VendedoresController@updateVendedor']);

    Route::post('vendedores/active',       ['as' => 'admin.vendedores.active',   'uses' => 'Cms\VendedoresController@active']);

    Route::delete('vendedores/delete',     ['as' => 'admin.vendedores.delete',   'uses' => 'Cms\VendedoresController@delete']);
});